import numpy as np
import pylab as pl
import os
import shutil
import time
import gzip

from tempfile import mkstemp as _mkstemp
from shutil import move as _move
from os import remove as _remove
from os import close as _close


def DataType(filepath):
    filename = filepath.split('/')[-1][:-4]
    datatype = filename[-3:]
    return datatype

def GetLocalSettings():
    a = GeneralString()
    a.Read('../LOCALSETTINGS.txt')
    return a.data

def UpdateRunDir():
    #this should be run from the run dir!
    rundir = os.getcwd()+'/'
    a = GeneralString()
    a.Read('../LOCALSETTINGS.txt')
    dd = a.data
    dd['rundir'] = rundir
    a.OverWrite(dd)

class Data :
    """
    Read('../path/to/filename_typ.dat')

    Produces
    self.data - dictionary containing all data in file.
    self.datainfo - diciontary containing
    'type', 'noofdata','sampindex'

    NewFile('suffix',infolist=None)
    Creates new file

    Write(sample)
    sample is dictionary
    """
    
    def __init__(self,filezip = False) :
        self.data     = {}
        self.datainfo = {}
        self.filezip = filezip
        

    def Clear(self) :
        self.data     = {}
        self.datainfo = {}

    def Read(self, filepath):
        self.filepath = filepath
        self.filename = filepath.split('/')[-1][:-7]
        datatype      = self.filename[-6:]
        
        self.datainfo['type'] = datatype
        
        if self.filename.startswith('20'):
            daydate   = int(self.filename[:8])
        else:
            daydate   = int(self.filename[:-7][-8:])
        print 'File Recorded on '+str(daydate)
        
        #read data
        if self.datainfo['type'] == 'INI':
            print 'Initialisation Data Format'
            a = DataFormatIni()
            a.Read(filepath)
            self.data = a.data
        #elif self.datainfo['type'] == 'scan':
        #    print 'Data Scan'
        #    a = DataScan()
        #    a.Read(filepath)
        #    self.data = a.data
        #    self.setupdata = a.setupdata
        elif (daydate > 20120000) :
            print 'Data Format'
            a = DataFormat3()
            a.Read(filepath)
            self.data = a.data
            self.setupdata = a.setupdata
        else:
            print 'Unknown Data Format'

        #prepare some extra variables
        if self.data.has_key('refc1amp') == True:
            self.data['sampindex']      = np.arange(len(self.data['refc1amp']))
            self.datainfo['noofdata']   = len(self.data['refc1amp'])
        #else:
         #   l = self.data.keys()[0]
          #  if l == 'info':
          #      l = self.data.keys()[1]
          #      self.data['sampindex']      = np.arange(len(self.data[l]))
          #      self.datainfo['noofdata']   = len(self.data[l])

        if self.datainfo['type'] == 'INI':
            del self.data['info']
    
    
    def CloseFile(self) :
        
        self.f.close()
        
class DataFormat :

    # data of slot2 & slot3
    def __init__(self) :
        
        self.wfmsl2ch1 = []
        self.wfmsl2ch0 = []
        self.wfmsl3ch1 = []
        self.wfmsl3ch0 = []
        self.BPM01X = []
        self.BPM02X = []
        self.BPM03X = []
        self.BPM04X = []
        self.BPM05X = []
        self.BPM01Y = []
        self.BPM02Y = []
        self.BPM03Y = []
        self.BPM04Y = []
        self.BPM05Y = []
        self.npulse = []
        self.posix = []
        self.posiy = []
        self.SetcbpmPosi = []
        self.axis = []
        self.scannpulse = []
        self.scannstep = []
        self.scanrange = []
        self.scanrlposi = []
    
    def Readlogs(self, fd) :

              
        
        print 'CBPM.log read',fd
        
        self.iread =0
        
        for line in fd:
            line = line.strip('>')
            line = line.strip()            
            t = line.split(" ")
            nv = len(t)
            d = []


            if t[0] == 'slot2ch1>':
                for i in range(1,nv) :
                    self.wfmsl2ch1.append(float(t[i]))
                
            elif t[0] == 'slot2ch0>':
                for i in range(1,nv) :
                    self.wfmsl2ch0.append(float(t[i]))                              
            elif t[0] == 'slot3ch0>':
                for i in range(1,nv) :
                    self.wfmsl3ch0.append(float(t[i]))                              
            elif t[0] == 'slot3ch1>':
                for i in range(1,nv) :
                    self.wfmsl3ch1.append(float(t[i]))                              
            elif t[0] == 'npulse>':
                for i in range(1,nv) :
                    self.npulse.append(float(t[i]))
                    
            elif t[0] == 'readPosiH>':
                for i in range(1,nv) :
                    self.posix.append(float(t[i]))          
            elif t[0] == 'readPosiV>':
                for i in range(1,nv) :
                    self.posiy.append(float(t[i]))
            elif t[0] == 'scanrelativeposi>':
                for i in range(1,nv) :
                    self.scanrlposi.append(float(t[i]))
            elif t[0] == 'SetcbpmPosi>':
                for i in range(1,nv) :
                    self.SetcbpmPosi.append(float(t[i]))
                    
            elif t[0] == 'Ini_Axis>':
                for i in range(1,nv) :
                    self.axis.append((t[i]))
                #self.axis = np.array(self.axis)
            elif t[0] == 'BPM01X>':
                for i in range(1,nv) :
                    self.BPM01X.append(float(t[i]))
            elif t[0] == 'BPM02X>':
                for i in range(1,nv) :
                    self.BPM02X.append(float(t[i]))
            elif t[0] == 'BPM03X>':
                for i in range(1,nv) :
                    self.BPM03X.append(float(t[i]))
            elif t[0] == 'BPM04X>':
                for i in range(1,nv) :
                    self.BPM04X.append(float(t[i]))
            elif t[0] == 'BPM05X>':
                for i in range(1,nv) :
                    self.BPM05X.append(float(t[i]))
            elif t[0] == 'BPM01Y>':
                for i in range(1,nv) :
                    self.BPM01Y.append(float(t[i]))
            elif t[0] == 'BPM02Y>':
                for i in range(1,nv) :
                    self.BPM02Y.append(float(t[i]))
            elif t[0] == 'BPM03Y>':
                for i in range(1,nv) :
                    self.BPM03Y.append(float(t[i]))
            elif t[0] == 'BPM04Y>':
                for i in range(1,nv) :
                    self.BPM04Y.append(float(t[i]))
            elif t[0] == 'BPM05Y>':
                for i in range(1,nv) :
                    self.BPM05Y.append(float(t[i]))
            elif t[0] == 'scannpulse>':
                for i in range(1,nv) :
                    self.scannpulse.append(float(t[i]))
            elif t[0] == 'scanrange>':
                for i in range(1,nv) :
                    self.scanrange.append(float(t[i]))
            elif t[0] == 'scannstep>':
                for i in range(1,nv) :
                    self.scannstep.append(float(t[i]))
                  
        #print self.wfmsl2ch1
                
        print 'Bpm. LoggedData.total pulses>',self.iread

    def ReadlogsForJsCall(self, fd) :
    
        print 'CBPM.log for JSCall read',fd
        
        self.iread =0
    
        for line in fd:
            line = line.strip('>')
            line = line.strip()
            t = line.split(" ")
            nv = len(t)
            d = []
            
            if t[0] == 'slot2ch1>':
                for i in range(1,nv) :
                    self.wfmsl2ch1.append(float(t[i]))
            
            elif t[0] == 'slot2ch0>':
                for i in range(1,nv) :
                    self.wfmsl2ch0.append(float(t[i]))
            elif t[0] == 'slot3ch0>':
                for i in range(1,nv) :
                    self.wfmsl3ch0.append(float(t[i]))
            elif t[0] == 'slot3ch1>':
                for i in range(1,nv) :
                    self.wfmsl3ch1.append(float(t[i]))
            
            
            elif t[0] == 'npulse>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.npulse.append(d)
                print self.npulse
        
            elif t[0] == 'readPosiH>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.posix.append(d)
            elif t[0] == 'readPosiV>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.posiy.append(d)
        
            elif t[0] == 'Ini_Axis>':
                for i in range(1,nv) :
                    self.axis.append((t[i]))
                print self.axis
            elif t[0] == 'BPM01X>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.BPM01X.append(d)
            elif t[0] == 'BPM02X>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.BPM02X.append(d)
            elif t[0] == 'BPM03X>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.BPM03X.append(d)
            elif t[0] == 'BPM04X>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.BPM04X.append(d)
            elif t[0] == 'BPM05X>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.BPM05X.append(d)
            elif t[0] == 'BPM01Y>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.BPM01Y.append(d)
            elif t[0] == 'BPM02Y>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.BPM02Y.append(d)
            elif t[0] == 'BPM03Y>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.BPM03Y.append(d)
            elif t[0] == 'BPM04Y>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.BPM04Y.append(d)
            elif t[0] == 'BPM05Y>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.BPM05Y.append(d)
            elif t[0] == 'scannpulse>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.scannpulse.append(d)
            elif t[0] == 'scanrange>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.scanrange.append(d)
            elif t[0] == 'scannstep>':
                for i in range(1,nv) :
                    d.append(float(t[i]))
                self.scannstep.append(d)
    
        #print self.wfmsl2ch1
    
        print 'Bpm. LoggedData.total JsCall pulses>',self.iread


    def initialize(self):
        self.wfmsl2ch1 = []
        self.wfmsl2ch0 = []
        self.wfmsl3ch1 = []
        self.wfmsl3ch0 = []
        self.BPM01X = []
        self.BPM02X = []
        self.BPM03X = []
        self.BPM04X = []
        self.BPM05X = []
        self.BPM01Y = []
        self.BPM02Y = []
        self.BPM03Y = []
        self.BPM04Y = []
        self.BPM05Y = []
        self.npulse = []
        self.posix = []
        self.posiy = []
        self.SetcbpmPosi = []
        self.axis = []
        self.scannpulse = []
        self.scannstep = []
        self.scanrange = []
        self.scanrlposi = []

    def array(self):
        self.BPM01X = pl.array(self.BPM01X)
        self.BPM02X = pl.array(self.BPM02X)
        self.BPM03X = pl.array(self.BPM03X)
        self.BPM04X = pl.array(self.BPM04X)
        self.BPM05X = pl.array(self.BPM05X)
        self.BPM01Y = pl.array(self.BPM01Y)
        self.BPM02Y = pl.array(self.BPM02Y)
        self.BPM03Y = pl.array(self.BPM03Y)
        self.BPM04Y = pl.array(self.BPM04Y)
        self.BPM05Y = pl.array(self.BPM05Y)
        self.posix = pl.array(self.posix)
        self.posiy = pl.array(self.posiy)



