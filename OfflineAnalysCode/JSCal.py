from   math import *
import Data
import OfflineAnalyse as ana

import gzip
import os
import pickle
import numpy          as np
import pylab          as pl
import scipy          as sp
import scipy.signal   as si
import scipy.optimize as op
import scipy.stats    as st
import math           as m
import shutil
from ROOT import TCanvas, TGraph, TGraphErrors, TLegend,TPad,TF1,gApplication,gStyle
saves = {}


class CalibrationJitterSubtract :

    def __init__(self,fd,jbps=[]) :
        print 'Calculations:MoverCalibration:__init__>',fd


        self.filename = fd
        self.jbps = jbps

    def subtractJitter(self,center, vcenter,xmin,xmax,ymin,ymax) :
        print "MoverCalibrationJitterSubtracted> Removing jitter..."
        
    
        orb = NewOrbit(self.filename,'scanlgsl2')
        orb.svd_all(center,vcenter)
        orb.CalJisPlot(self.filename,xmin,xmax,ymin,ymax)


class NewOrbit :
    def __init__(self,fd,op) :
        
        self.filepath = fd
        
        if fd.split('_')[-1][-2:] == 'gz':
            print 'reading zip file...'
            self.f = gzip.open(self.filepath,'rb')
            self.filename = fd[-28:]
        else :
            self.f = open(self.filepath,'r')
            self.filename = fd

        self.d = Data.DataFormat()
        self.d.ReadlogsForJsCall(self.f)
        self.axis = self.d.axis[0]
        self.d.array()
        #self.CalAnalysis_abs(op)
        self.CalAnalysis_all(op)
        
    
    
    def svd(self, center, vcenter, jbpms=[], rbpms=[], controlPlot=False) :
        
        scannpulse = pl.array(self.d.scannpulse)
        scannstep = pl.array(self.d.scannstep)
        
        npulse = int(scannpulse[0,:])
        nstep = int(scannstep[0,:])
        
        print npulse, nstep
           
        self.ibpm  = int(npulse*(nstep+1))
        self.jbpms = jbpms
        self.rbpms = rbpms
           
        # use all bpms upstream if none specified
        if len(self.jbpms)<1 :
            self.jbpms = range(0,self.ibpm+1)
           
        # check the list of bpms for removal
        if 529 not in self.rbpms :
            self.rbpms.append(529)
           
        # check if current bpm needs to be removed
        if self.ibpm in self.jbpms and self.ibpm not in self.rbpms :
            self.rbpms.append((self.ibpm))
           
        # remove rbpms
        for btr in self.rbpms :
            if btr in self.jbpms :
                self.jbpms.remove(btr)

#print self.ibpm, self.jbpms, self.rbpms

        ## Fill data of position BPMs
        if self.axis == 'Hori':
            
            self.iposi = self.d.posix[self.jbpms,:]
            self.mbpm1x = self.d.BPM01X[self.jbpms,:]
            self.mbpm2x = self.d.BPM02X[self.jbpms,:]
            self.mbpm3x = self.d.BPM03X[self.jbpms,:]
            self.mbpm4x = self.d.BPM04X[self.jbpms,:]
            self.mbpm5x = self.d.BPM05X[self.jbpms,:]
        
            # check for NaN and Inf values and set them to 0
            for abpm in self.jbpms :
                self.mbpm1x[abpm,pl.find(pl.isnan(self.mbpm1x[abpm,:])==True)]=0
                self.mbpm2x[abpm,pl.find(pl.isnan(self.mbpm2x[abpm,:])==True)]=0
                self.mbpm3x[abpm,pl.find(pl.isnan(self.mbpm3x[abpm,:])==True)]=0
                self.mbpm4x[abpm,pl.find(pl.isnan(self.mbpm4x[abpm,:])==True)]=0
                self.mbpm5x[abpm,pl.find(pl.isnan(self.mbpm5x[abpm,:])==True)]=0
        
        elif self.axis == 'Vert':
            
            self.iposi = self.d.posiy[self.jbpms,:]
            self.mbpm1y = self.d.BPM01Y[self.jbpms,:]
            self.mbpm2y = self.d.BPM02Y[self.jbpms,:]
            self.mbpm3y = self.d.BPM03Y[self.jbpms,:]
            self.mbpm4y = self.d.BPM04Y[self.jbpms,:]
            self.mbpm5y = self.d.BPM05Y[self.jbpms,:]
                
            # check for NaN and Inf values and set them to 0
            for abpm in self.jbpms :
                self.mbpm1y[abpm,pl.find(pl.isnan(self.mbpm1y[abpm,:])==True)]=0
                self.mbpm2y[abpm,pl.find(pl.isnan(self.mbpm2y[abpm,:])==True)]=0
                self.mbpm3y[abpm,pl.find(pl.isnan(self.mbpm3y[abpm,:])==True)]=0
                self.mbpm4y[abpm,pl.find(pl.isnan(self.mbpm4y[abpm,:])==True)]=0
                self.mbpm5y[abpm,pl.find(pl.isnan(self.mbpm5y[abpm,:])==True)]=0
        
        self.ivol = self.Vm
        fake = pl.hstack((self.iposi,self.ivol))
        print fake

        # To invert voltage data which are smaller than a symmetric vertical axis
        i = 0
        vol = []
        for line in fake:
            if fake[i][0] < center:
                dis = fabs(fake[i][1]-vcenter)
                fake[i][1] = fake[i][1]-2*dis
            vol.append(fake[i][1])
            i += 1
        self.vol = pl.array(vol)

        if self.axis == 'Hori':
            self.m = pl.hstack((self.mbpm1x,self.mbpm2x,self.mbpm5x,self.iposi))
        elif self.axis == 'Vert':
            self.m = pl.hstack((self.mbpm1y,self.mbpm2y,self.mbpm5y,self.iposi))


        (u,s,vh) = pl.svd(self.m,False)

        self.volcoeff = pl.dot(vh.transpose(),pl.dot(pl.diag(1/s),pl.dot(u.transpose(),self.vol)))
        print self.volcoeff
            
        self.volpred = pl.dot(self.m,self.volcoeff)
        disc = self.vol - self.volpred

    def svd_all(self, center, vcenter, jbpms=[], rbpms=[], controlPlot=False) :
    
        scannpulse = pl.array(self.d.scannpulse)
        scannstep = pl.array(self.d.scannstep)
    
        npulse = int(scannpulse[0,:])
        nstep = int(scannstep[0,:])
    
        print npulse, nstep
    
        self.ibpm  = int(npulse*(nstep+1))
        self.jbpms = jbpms
        self.rbpms = rbpms


        # use all bpms upstream if none specified
        if len(self.jbpms)<1 :
            self.jbpms = range(0,self.ibpm+1)
    
        # check the list of bpms for removal
        if 529 not in self.rbpms :
            self.rbpms.append(529)
    
        # check if current bpm needs to be removed
        if self.ibpm in self.jbpms and self.ibpm not in self.rbpms :
            self.rbpms.append((self.ibpm))
    
        # remove rbpms
        for btr in self.rbpms :
            if btr in self.jbpms :
                self.jbpms.remove(btr)
    
    #print self.ibpm, self.jbpms, self.rbpms
    
    
    
    
        if self.axis == 'Hori':
            
            self.iposi = self.XMAX[self.jbpms,:]
            self.mbpm1x = self.d.BPM01X[self.jbpms,:]
            self.mbpm2x = self.d.BPM02X[self.jbpms,:]
            self.mbpm3x = self.d.BPM03X[self.jbpms,:]
            self.mbpm4x = self.d.BPM04X[self.jbpms,:]
            self.mbpm5x = self.d.BPM05X[self.jbpms,:]
    
        elif self.axis == 'Vert':
           
            self.iposi = self.YMAX[self.jbpms,:]
            self.mbpm1y = self.d.BPM01Y[self.jbpms,:]
            self.mbpm2y = self.d.BPM02Y[self.jbpms,:]
            self.mbpm3y = self.d.BPM03Y[self.jbpms,:]
            self.mbpm4y = self.d.BPM04Y[self.jbpms,:]
            self.mbpm5y = self.d.BPM05Y[self.jbpms,:]
        self.ivol = self.Vm
        fake = pl.hstack((self.iposi,self.ivol))
    
    
        i = 0
        vol = []
        for line in fake:
            if fake[i][0] < center:
                dis = fabs(fake[i][1]-vcenter)
                fake[i][1] = fake[i][1]-2*dis
            vol.append(fake[i][1])
            i += 1
        self.vol = pl.array(vol)
    
        if self.axis == 'Hori':
            self.m = pl.hstack((self.mbpm1x,self.mbpm2x,self.mbpm5x,self.iposi))
        elif self.axis == 'Vert':
            self.m = pl.hstack((self.mbpm1y,self.mbpm2y,self.mbpm5y,self.iposi))
    
        (u,s,vh) = pl.svd(self.m,False)
    
        self.volcoeff = pl.dot(vh.transpose(),pl.dot(pl.diag(1/s),pl.dot(u.transpose(),self.vol)))
        print self.volcoeff
    
        self.volpred = pl.dot(self.m,self.volcoeff)
        disc = self.vol - self.volpred

################# Plot #######################
    def CalJisPlot(self,fd,xmin,xmax,ymin,ymax):
        global saves
        
        
        axis = self.d.axis[0]
        
        
        cv = TCanvas("cv","",10,40,1100,1000)
        cv.SetGrid()
        cv.SetFillColor( 0 )
        saves['cv']=cv
                
        pad1 = TPad( 'pad1', 'This is pad1', 0.05, 0.02, 0.98, 0.98, 0 )
        pad1.Draw()
        saves[ 'pad1' ] = pad1
                
        pad1.cd()
                
        pad1.SetFillColor( 42 )
        pad1.SetBorderMode( 0 )
        pad1.SetBorderSize( 1 )
        pad1.SetFrameBorderMode( 0 )
        pad1.SetGrid()
        pad1.SetFillColor( 0 )
                
        gStyle.SetTitleOffset(100,"Y")
        gStyle.SetTitleFillColor(0)
        gStyle.SetTitleBorderSize(0)
                
                
                
        gr    = TGraph(len(self.iposi),self.iposi,self.vol)
        grpre = TGraph(len(self.iposi),self.iposi,self.volpred)
        
                
        gr.SetTitle(axis+ ':' + self.filename)
        gr.GetXaxis().SetTitle( 'CBPM position [mm]' )
        gr.GetYaxis().SetTitle( 'V_{p} [V]' )
        gr.GetXaxis().SetTitleSize(0.05)
        gr.GetYaxis().SetTitleSize(0.05)
        gr.GetXaxis().SetTitleOffset(0.9)
        gr.GetYaxis().SetTitleOffset(0.9)
                
        gr.SetMarkerColor(2)
        grpre.SetMarkerColor(4)
                
        gr.SetMarkerStyle(4)
        grpre.SetMarkerStyle(5)
                
        gr.SetMaximum(ymax)
        gr.SetMinimum(ymin)
                
        grpre.SetMaximum(ymax)
        grpre.SetMinimum(ymin)
                
        gr.Draw("AP")
        grpre.Draw("same P")
        
        f = TF1('f','pol1')
        fpre = TF1('fpre','pol1')
        
        f.SetParName(0,"b")
        f.SetParName(1,"a")
                    
        fpre.SetParName(0,"B")
        fpre.SetParName(1,"A")
                
        f.SetLineColor(2)
        fpre.SetLineColor(4)
        f.SetLineStyle(1)
        fpre.SetLineStyle(2)
        f.SetLineWidth(1)
        fpre.SetLineWidth(1)
                
        # To present a fitting parameters on a window
        gStyle.SetOptFit (111)
        gStyle.SetStatX ( 0.49 )
        gStyle.SetStatY ( 0.89 )
        gStyle.SetFillColor(2)
        gStyle.SetStatBorderSize (1)
                
        gr.Fit(f,"R","",xmin,xmax)
                
        # To present a fitting parameters on a window
        gStyle.SetOptFit (111)
        gStyle.SetStatX ( 0.49 )
        gStyle.SetStatY ( 0.69 )
        gStyle.SetStatBorderSize (1)
                
        grpre.Fit(fpre,"R+","",xmin,xmax)
                
                
        saves['gr']=gr
        saves['grpre']=grpre
                
        l = TLegend(0.62,0.75,0.89,0.89)
        l.SetFillColor(0)
        l.AddEntry(gr,'Data',"p")
        l.AddEntry(f,'y=ax+b',"l")
        l.AddEntry(grpre,'Data reduced Jitter effect',"p")
        l.AddEntry(fpre,'y=Ax+B',"l")
        l.Draw()
        saves['l'] = l
        
        cv.SaveAs(axis+'_'+fd+'_CalJit.eps')
        cv.Modified()
        cv.Update()

# file = 'JSCal_Parameters' +'.txt'
#       f = open(file,'a')

#        f.write('filename > '+ self.filename + ', coefficient = ' + str(self.volcoeff) + '\n')
#        f.close()


############## Append data ###################
# stage positions are real value.
    def CalAnalysis_abs(self,op) :
    
        print 'Ca.Analysis. Log readingfile...' , self.filepath, self.filename
        
    
        posix        = []
        posiy        = []
        d1         = []
        d2         = []
        time       = []
        samplerate = 5e8
        dt         = 1/samplerate
        self.Vmaxch0   = []
        self.Vmaxch1   = []
        self.d.scannpulse = pl.array(self.d.scannpulse)
        self.d.scannstep = pl.array(self.d.scannstep)
    
        npulse = int(self.d.scannpulse[0,:])
        nstep = int(self.d.scannstep[0,:])
    
        print 'Analysis.Cl.Plot pulse>' , npulse,nstep
    
        if op == 'scanlgsl2' :
            for i in self.d.wfmsl2ch0:
                d1.append(i)
            for i in self.d.wfmsl2ch1:
                d2.append(i)
        elif op == 'scanlgsl3' :
            for i in self.d.wfmsl3ch0:
                d1.append(i)
            for i in self.d.wfmsl3ch1:
                d2.append(i)
        else :
            print 'option is not found'
    
        self.ch0 = pl.array(d1)
        self.ch1 = pl.array(d2)
        Vm = []
        Z = 50.
        for j in range(0,nstep+1):
            #for j in range(0,10):
        
            print 'step>', j+1
            
            for i in range(j*(int(npulse)),(j+1)*(int(npulse))) :
                V0 =self.ch0[i*3000:(i+1)*3000]
                V1 =self.ch1[i*3000:(i+1)*3000]
                # peak value of each pulse
                Vp0 = max(V0)
                Vp1 = max(V1)
                
                if self.axis == 'Hori':
                    p0 = 20.64
                    p1 = 0.004654
                    power = (Vp1-p1)/p0*pow(10,1.64/20)*1e-3
                elif self.axis == 'Vert':
                    p0 = 20.82
                    p1 = -0.02141
                    power = (Vp0-p1)/p0*pow(10,1.64/20)*1e-3
            
                vpeak = pl.sqrt(2*power*Z)
            
                Vm.append([float(vpeak)])
            
        self.Vm = np.array(Vm)
        self.f.close()

    ## Exceptional (in case there is only the first stage position, )
    def CalAnalysis_all(self,op) :
    
        print 'Ca.Analysis. Log readingfile...' , self.filepath, self.filename
    
    
        tpx        = []
        tpy        = []
        d1         = []
        d2         = []
        time       = []
        samplerate = 5e8
        dt         = 1/samplerate
        self.Vmaxch0   = []
        self.Vmaxch1   = []
        
        self.d.scannpulse = pl.array(self.d.scannpulse)
        self.d.scannstep = pl.array(self.d.scannstep)
        
        npulse = int(self.d.scannpulse[0,:])
        nstep = int(self.d.scannstep[0,:])
    
        print 'Analysis.Cl.Plot pulse>' , npulse,nstep
        
        ## Append CBPM positions Hori
        for i in self.d.posix:
            tpx.append(i)
        ## Append CBPM positions Vert
        for i in self.d.posiy:
            tpy.append(i)
        
        self.px = pl.array(tpx)
        self.py = pl.array(tpy)
    
        if op == 'scanlgsl2' :
            for i in self.d.wfmsl2ch0:
                d1.append(i)
            for i in self.d.wfmsl2ch1:
                d2.append(i)
        elif op == 'scanlgsl3' :
            for i in self.d.wfmsl3ch0:
                d1.append(i)
            for i in self.d.wfmsl3ch1:
                d2.append(i)
        else :
            print 'option is not found'
    
        self.ch0 = pl.array(d1)
        self.ch1 = pl.array(d2)
        Vm = []
        XMAX = []
        YMAX = []
        Z = 50.
        for j in range(0,nstep+1):
            #for j in range(0,10):
        
            print 'step>', j+1
        
            for i in range(j*(int(npulse)),(j+1)*(int(npulse))) :
                XMAX.append(self.px[j])
                YMAX.append(self.py[j])
                V0 =self.ch0[i*3000:(i+1)*3000]
                V1 =self.ch1[i*3000:(i+1)*3000]
                # peak value of each pulse
                Vp0 = max(V0)
                Vp1 = max(V1)
            
                if self.axis == 'Hori':
                    p0 = 20.64
                    p1 = 0.004654
                    power = (Vp1-p1)/p0*pow(10,1.64/20)*1e-3
                elif self.axis == 'Vert':
                    p0 = 20.82
                    p1 = -0.02141
                    power = (Vp0-p1)/p0*pow(10,1.64/20)*1e-3
            
                vpeak = pl.sqrt(2*power*Z)
            
                Vm.append([float(vpeak)])
    
    
        self.Vm = np.array(Vm)
        self.XMAX = pl.array(XMAX)
        self.YMAX = pl.array(YMAX)
        print self.YMAX, self.Vm
    
        print len(self.Vm)
        self.f.close()



