#! /usr/bin/python


import numpy as np
import time
import gzip

import OfflineAnalyse as ana

def main():
    an = ana.BPMAnalysis('Vert') # Hori/Vert : Setting option of ch0
    filepath = '20141115_1810_scansl2.dat'
    option   = 'scanlgsl2' # scanlgsl2 or scanlgsl3
  
    an.IntegralAnalysis(filepath,option)
    an.IntVolPlot('ch1',-1,1,-5.3e-8,-1.9e-8) # pol2 option:(ch0/ch1), xmin, xmax, ymin, ymax
    
if __name__ == "__main__":
    main()
    rep = ''
    while not rep in ['q','Q']:
        rep = raw_input('enter "q" to quit: ')
        if 1 < len(rep):
            rep = rep[0]

