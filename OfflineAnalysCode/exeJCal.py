#! /usr/bin/python

## To analyse beam jitter with position BPM data at VELA
#  
#
#

import numpy as np
import time
import gzip

import OfflineAnalyse as ana

def main():
    an = ana.BPMAnalysis('Vert') # Hori/Vert : Setting option of ch0
    filepath = 'ListVert'
    an.CalJitter(filepath)
    an.JCal_Plot('BPM01X',1,3.5)
  

if __name__ == "__main__":
    main()
    rep = ''
    while not rep in ['q','Q']:
        rep = raw_input('enter "q" to quit: ')
        if 1 < len(rep):
            rep = rep[0]

