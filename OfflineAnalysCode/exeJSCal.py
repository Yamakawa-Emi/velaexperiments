#! /usr/bin/python


###
## To subtruct beam jitter effect from the data taken by CBPM (rhul)


import numpy as np
import time
import gzip

import Data
import JSCal as js

def main():
    a = js.CalibrationJitterSubtract('20141208_1104_scansl2.dat.gz')
    xmin = -1.6
    xmax = 1.6
    ymin = -0.05
    ymax = 0.1
    a.subtractJitter(0.1088, 0.02309,xmin,xmax,ymin,ymax)




if __name__ == "__main__":
    main()
    rep = ''
    while not rep in ['q','Q']:
        rep = raw_input('enter "q" to quit: ')
        if 1 < len(rep):
            rep = rep[0]

