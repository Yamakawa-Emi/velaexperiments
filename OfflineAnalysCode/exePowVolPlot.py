#! /usr/bin/python


import numpy as np
import time
import gzip

import OfflineAnalyse as ana

def main():
    an = ana.BPMAnalysis('Vert') # Hori/Vert : Setting option of ch0
    filepath = '20141208_1352_scansl2.dat.gz'
    option   =  'scanlgsl2' # scanlgsl2 or scanlgsl3
    fixpara = 0.08473
    an.CalAnalysis_all(filepath,option)
    an.AtPortPlot_abs(filepath,fixpara,-1.6,1.6,0,1e-4,0.0,0.1)
    
if __name__ == "__main__":
    main()
    rep = ''
    while not rep in ['q','Q']:
        rep = raw_input('enter "q" to quit: ')
        if 1 < len(rep):
            rep = rep[0]

