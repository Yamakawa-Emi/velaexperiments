#!../../bin/linux-x86/bpmProc

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/bpmProc.dbd" 
bpmProc_registerRecordDeviceDriver pdbbase
dbLoadRecords("db/acquire.db","name=RP1")
dbLoadRecords("db/bpmPolProc.vdb","name=cbpm1x,diodename=CDIODE,waveformName=RP1:cha,nbunch=10,tok=0,nowf=1024")
dbLoadRecords("db/bpmPolProc.vdb","name=cbpm1y,diodename=CDIODE,waveformName=RP1:chb,nbunch=10,tok=0,nowf=1024")
dbLoadRecords("db/bpmPolProcIQpos.vdb","name=cbpm1x,rname=REFC1,diodename=CDIODE,nowf=1024,nbunch=10,nbpms=2")
dbLoadRecords("db/bpmPolProcIQpos.vdb","name=cbpm1y,rname=REFC1,diodename=CDIODE,nowf=1024,nbunch=10,nbpms=2")
dbLoadRecords("db/bpmDiodeProc.db","name=CDIODE,waveformName=RP1:chb,ns=1024")

cd ${TOP}/iocBoot/${IOC}
iocInit()
## Start any sequence programs
#seq sncxxx,"user=ipsbpm"
