#!../../bin/linux-x86/bpmProc

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/bpmProc.dbd" 
bpmProc_registerRecordDeviceDriver pdbbase
dbLoadRecords("db/acquire.db","name=RP2")
dbLoadRecords("db/bpmPolProc.vdb","name=cbpm2x,diodename=CDIODE,waveformName=RP2:cha,nbunch=10,tok=0,nowf=1024")
dbLoadRecords("db/bpmPolProc.vdb","name=cbpm2y,diodename=CDIODE,waveformName=RP2:chb,nbunch=10,tok=0,nowf=1024")
dbLoadRecords("db/bpmPolProcIQpos.vdb","name=cbpm2x,rname=REFC1,diodename=CDIODE,nowf=1024,nbunch=10,nbpms=2")
dbLoadRecords("db/bpmPolProcIQpos.vdb","name=cbpm2y,rname=REFC1,diodename=CDIODE,nowf=1024,nbunch=10,nbpms=2")


cd ${TOP}/iocBoot/${IOC}
iocInit()
## Start any sequence programs
#seq sncxxx,"user=ipsbpm"
