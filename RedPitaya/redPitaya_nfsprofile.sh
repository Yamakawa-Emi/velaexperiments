#!/bin/bash

export EPICS=/mnt/base-RP
export EPICS_CA_MAX_ARRAY_BYTES=100000000
export PATH=$PATH:$EPICS/bin/linux-arm
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/base-RP/lib/linux-arm
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/RedPitaya/ioc/lib/linux-arm
export PATH=$PATH:/mnt/extensions-3.14.12.2/bin

EDMPVOBJECTS=$EPICS/extensions/src/edm/setup
EDMOBJECTS=$EPICS/extensions/src/edm/setup
EDMHELPFILES=$EPICS/extensions/src/edm/helpFiles
EDMFILES=$EPICS/extensions/src/edm/setup
EDMDATAFILES=/home/epics/ioc/run/CBPM
export EDMPVOBJECTS EDMOBJECTS EDMHELPFILES EDMFILES EDMDATAFILES


export EPICS_CA_AUTO_ADDR_LIST=NO
export EPICS_CA_ADDR_LIST="localhost"
export EPICS_CAS_INTF_ADDR_LIST="localhost"
