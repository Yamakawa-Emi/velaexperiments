#!/bin/sh 
# start extlw ioc

case "$1" in
    start)
    echo "Starting $0"

    if [ -f /tmp/ioc.lock ]; then
	echo "ioc alread running"
	exit 0;
    fi

    cd /home/epics/ioc/iocBoot/iocextlw/
    screen -d -m -S ioc -h 1000 ./st.cmd
    touch /tmp/ioc.lock
    echo $USER >> /tmp/ioc.lock
    chmod ug+wr /tmp/ioc.lock
    ;;

    stop)
    echo "Stopping $0"
    /usr/bin/killall -9 st.cmd
    rm -rf /tmp/ioc.lock
    ;;

    restart)
    echo "Restarting $0"
    sh $0 stop
    sh $0 start
    ;;
esac