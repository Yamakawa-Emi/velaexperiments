#!../../bin/linux-x86/extlw

## You may have to change extlwApp to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/extlw.dbd"
extlw_registerRecordDeviceDriver pdbbase

## Load record instances


dbLoadRecords("db/cbpmLabVIEW.db","name=c2")
dbLoadRecords("db/plot.db","name=plot")
dbLoadRecords("db/scan.db","name=scan")
dbLoadRecords("db/cbpmWaveform.db","name=pxi")
#dbLoadRecords("db/cbpmPlot.db","name=plot")

## Set this to see messages from mySub
#var mySubDebug 1

## Run this to trace the stages of iocInit
#traceIocInit

cd ${TOP}/iocBoot/${IOC}
iocInit

## Start any sequence programs
#seq sncExample, "user=sboogertHost"
