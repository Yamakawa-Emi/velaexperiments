#include <stdio.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <aSubRecord.h>
#include <math.h>
#include <string.h>
#include <fftw3.h>
#include <float.h>
#include <cadef.h>

fftw_complex *fftw_in;
fftw_complex *fftw_out;
fftw_plan    fftw_p;

/* Initialise functions */

static long FFTInit(aSubRecord *prec) {
  
  printf("FFTInit> \n");

  fftw_in  = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*1024);
  fftw_out = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*1024);
  fftw_p = fftw_plan_dft_1d(1024,fftw_in, fftw_out, FFTW_FORWARD, FFTW_ESTIMATE);
} 

static long cbpmPosGrabInit(aSubRecord *prec) {
    printf("cbpmPosGrabInit> \n");
}

static long SumInit(aSubRecord *prec) {
  printf("SumInit> \n");
}

static long FltCoeffInit(aSubRecord *prec) {
  printf("FltCoeffInit> \n");
}

static long PedestalInit(aSubRecord *prec) {
  printf("PedestalInit> \n");
}

static long SubPedestalInit(aSubRecord *prec) {
  printf("SubPedestalInit> \n");
}

static long reverseSearchInit(aSubRecord *prec) {
  printf("reverseSerchInit> \n");
}

static long SubConstWfInit(aSubRecord *prec) {
  printf("subConstInit> \n");
}

static long AddWfInit(aSubRecord *prec) {
  printf("addWfInit> \n");
}

static long MultWfInit(aSubRecord *prec) {
  printf("multWfInit> \n");
}

static long MultConstWfInit(aSubRecord *prec) {
  printf("multConstInit> \n");
}

static long MaxSignalInit(aSubRecord *prec) {
  printf("maxSignalInit> \n");
}

static long SampleGetAmpInit(aSubRecord *prec) {
  printf("sampleGetAmpInit> \n");
}

static long SampleGetPhaInit(aSubRecord *prec) {
  printf("sampleGetPhaseInit> \n");
}


static long FilterInit(aSubRecord *prec) {
  printf("FIRFilterInit> \n");
}

static long subArrayInit(aSubRecord *prec) {
  printf("subArrayInit> \n");
}

static long shiftWfInit(aSubRecord *prec) {
  printf("shiftWfInit> \n");
}

static long complexDCInit(aSubRecord *prec) {
  printf("complexDCInit> \n");
}

static long complexLOInit(aSubRecord *prec) {
  printf("complexLOInit> \n");
}

static long calcomplexLOInit(aSubRecord *prec) {
  printf("cal complexLOInit> \n");
}


static long ampandphaseInit(aSubRecord *prec) {
  printf("AmplitudeandPhase Init> \n");
}

static long IQPosTiltInit(aSubRecord *prec) {
  printf("IQpos> \n");
}

static long DerivInit(aSubRecord *prec) {
  printf("Deriv Init> \n");
}

static long calSampleGetPhaInit(aSubRecord *prec) {
  printf("cal Sample Get Pha> \n");
}

static long calSampleGetAmpInit(aSubRecord *prec) {
  printf("cal Sample Get Amp Init> \n");
}

static long calIQPosTiltInit(aSubRecord *prec) {
  printf("cal IQ pos tilt Init> \n");
}

static long calcomplexDCInit(aSubRecord *prec) {
  printf("cal complex DC  Init> \n");
}

static long PeaksInit(aSubRecord *prec) {
  printf("Peaks Init> \n");
}

static long sampInit(aSubRecord *prec) {
  printf("samps Init> \n");
}


// //###########################
// // Subtract pedestal
// //###########################

static long SubPedestalProc(aSubRecord *prec) {
  
  long i;
  double sum = 0;
    
  //output and input links
  double *wfin  = (double*)prec->a;
  long   *start = (long*)prec->b;

  double *wfout = (double*)prec->vala;
  double *ped   = (double*)prec->valb;
  
  // calculate pedestal
  for(i=0;i<int(*start);i++){
    sum += wfin[i];
  }
  *ped = sum/double(*start);
  
  // subtract pedestal 
  for (i=0; i<int(prec->nova); i++) {
    wfout[i] = wfin[i] - *ped;
  }
  
  //  printf("pedestal>%lf\n",*ped);

  return 0; // process output links 
}


/*

static long reverseSearchProc(aSubRecord *prec) {
  
  long i,j;
  long l = 0;//real bunch number
  long last,init;
  long iBitMax;
  long iBitMin;

  //output and input links
  double *wfin  = (double*)prec->a;
  long   *tok   = (long*)prec->b;  
  double *t0    = (double*)prec->c;
  long *nb    = (long*)prec->d;
  double *lastunsat   = (double*)prec->vala;
  
  //revearse search unsaturated wf   
  for (i=0;i<int(prec->nova);i++){
    lastunsat[i]=0;
  }

  if (*tok == 0){
    iBitMax = 16383;
    iBitMin = 0;
  }

  else if (*tok = 1){
    iBitMax = 32765;
    iBitMin = -32767;
  }



  for(j=0;j<int(prec->nod);j++){
    if (int(t0[j]) == 0) break;
    l++;
  }

  last = int((prec->noa)/2.);
  // reverse search for last unsaturated pulse 
  for(j=int(l)-1;j>-1;j--) {
    if (j == 0) init = 0;
    else init = int((t0[j]+t0[j-1])/2.);

    for(i=int(last);i>int(init);i--) {
      if((wfin[i]) == (iBitMax) ||
	 (wfin[i]) == (iBitMin)) {
	lastunsat[j] = i+1;
	break;
      }

      if (j!=0) last = int((t0[j]+t0[j-1])/2.);
    }
    //printf("lastunsat i> %lf %lu\n",lastunsat[j],j);
  }
  
return 0; // process output links/
}
*/

static long reverseSearchProc(aSubRecord *prec) {
  
  long i,j;
  long l = 0; // real bunch number
  long last,init;
  long iBitMax;
  long iBitMin;

  //output and input links
  double *wfin  = (double*)prec->a;
  long   *tok   = (long*)prec->b;  
  double *t0    = (double*)prec->c;
  long   *nb    = (long*)prec->d;
  long   *delt  = (long*)prec->e;

  double *lastunsat   = (double*)prec->vala;

  //revearse search unsaturated wf   
  for (i=0;i<int(prec->nova);i++){
    lastunsat[i]=0;
  }

  if (*tok == 0){
    iBitMax = 16383;
    iBitMin = 0;
  }

  else if (*tok = 1){
    iBitMax = 32765;
    iBitMin = -32767;
  }

  for(j=0;j<int(*nb);j++){
    if (int(t0[j]) == 0) break;
    l++;
  }

  last = int((prec->noa)/2.);
  // reverse search for last unsaturated pulse 
  for(j=int(l)-1;j>-1;j--) {
    if (j == 0) init = 0;
    else init = t0[j] - *delt;

    for(i=int(last);i>int(init);i--) {
      if((wfin[i]) == (iBitMax) ||
	 (wfin[i]) == (iBitMin)) {
	lastunsat[j] = i+1;
	break;
      }

      if (j!=0) last = init;
    }
    //    printf("t0>%lf, init>%lu last>%lu lastunsat>%lf\n",t0[j],init, last,lastunsat[j]);
  }
  
  return 0; // process output links/
}


// //###########################
// // Calculate pedestal value
// //###########################

static long PedestalProc(aSubRecord *prec) {
  
  long i;
  double sum = 0;
    
  //output and input links
  double *wfin  = (double*)prec->a;
  long   *start = (long*)prec->b;
  double *lastunsat = (double*)prec->c;

  double *wfout = (double*)prec->vala;
  double *ped   = (double*)prec->valb;
  
  // calculate pedestal
  for(i=0;i<int(*start);i++){
    sum += wfin[i];
  }
  *ped = sum/double(*start);
  
  // subtract pedestal 
  for (i=0; i<int(prec->nova); i++) {
    wfout[i] = wfin[i] - *ped;

    if(i<int(lastunsat[0]) || i<int(*start) ) {
      wfout[i] = 0.;
    }
  }

  return 0; // process output links 
}

/*
static long PedestalProc(aSubRecord *prec) {
  
  long i;
  long c = 0;
  
  
  //get (array) data from input link
  double *waveform_pd = (double*)prec->a;
  long   *signalstart = (long*)prec->b;
  
  double sumup = 0;
  
  //   //sumup Wf data
     for(i=0;i<int(*signalstart);i++){
       sumup += waveform_pd[i];
       c++;
     }
     
     //   //output link
     double *pedestal = (double *)prec->vala;
     *pedestal = sumup/double(c);
     
     // printf("pedestal>%f\n",*pedestal);
     return 0; 
}
*/
// //###############################
// // Sum up all points in waveform
// //##############################

static long SumProc(aSubRecord *prec) {
  
  long i;
  
  //get (array) data from input link
  double *waveform_sum    = (double*)prec->a;
  long   *startNum     = (long*)prec->b;
  long   *stopNum     = (long*)prec->c;
  
  
  double sumup = 0;
  
  //sumup Wf data
  for(i=(*startNum);i<(*stopNum);i++){
    sumup += waveform_sum[i];
  }
  
  //output link
  double *sum = (double *)prec->vala;
     *sum = sumup;
     
     return 0; /* process output links */
}

// //### adding constant to waveform #####
// // waveform + constants -> new waveform
// //#####################################

static long SubConstWfProc(aSubRecord *prec) {
  
  long i;
  
  //get (array) data from input link
  double *waveform_add    = (double*)prec->a;
  double *constant_add    = (double*)prec->b;
  
  //output link
  double *vala_wf;
  vala_wf = (double*)prec->vala;
  
//   // adding constant to Wf
  for (i=0; i<int(prec->nova); i++) {
    vala_wf[i] = waveform_add[i] - *constant_add;
  }
  
  //   // 
  return 0; /* process output links */
}

// //### adding waveforms #################
// // waveform1 + waveform2 -> new waveform
// //######################################

static long AddWfProc(aSubRecord *prec) {
  
  long i;
  
  //get (array) data from input link
  double *waveform1_add    = (double*)prec->a;
  double *waveform2_add    = (double*)prec->b;
  
  
  //output link
  double *vala_wfadd;
  vala_wfadd = (double *)prec->vala;
  
  // Wf1 + Wf2
  for (i=0; i<int(prec->nova); i++) {
    vala_wfadd[i] = waveform1_add[i] + waveform2_add[i];
  }
  
  return 0; /* process output links */
}

// //### waveform plus constant #########
// // Constant * waveform -> new waveform
// //####################################

static long MultConstWfProc(aSubRecord *prec) {
  
  long i;
  
  //get (array) data from input link
  double *waveform_ml    = (double*)prec->a;
  double *constant_ml    = (double*)prec->b;
  
  //output link
  double *vala_ml;
  vala_ml = (double *)prec->vala;
  
     // adding constant to Wf
  for (i=0; i<int(prec->nova); i++) {
    vala_ml[i] = waveform_ml[i] * (*constant_ml);
  }
  
  return 0; /* process output links */
}

// //## Multiply waveforms #########################
// // waveform1[i] * waveform2[i] -> new waveform[i]
// //###############################################

static long MultWfProc(aSubRecord *prec) {
  
  long i;
  
  //get (array) data from input link
  double *waveform1_mlwf    = (double*)prec->a;
  double *waveform2_mlwf    = (double*)prec->b;
  
  //output link
  double *vala_mlwf;
  vala_mlwf = (double *)prec->vala;
  
  // Wf1 + Wf2
  for (i=0; i<int(prec->nova); i++) {
    vala_mlwf[i] = waveform1_mlwf[i] * waveform2_mlwf[i];
  }
  
  return 0; /* process output links */
}

// //################################################
// //### Find peak value of signal in waveform ######
// //################################################

static long MaxSignalProc(aSubRecord *prec) {
  
  long i;
  
  //get (array) data from input link
  double *waveform_max   = (double*)prec->a;
  
  //output link
  double *maxSampleNum = 0;
  double *maxValue = 0;   
  maxSampleNum = (double *)prec->vala;
  maxValue     = (double *)prec->valb;
  
  // find Maximum value of Wf
  for (i=0; i<int(prec->noa); i++) {
    if (waveform_max[i] > *maxValue) {
      *maxValue = waveform_max[i];
      *maxSampleNum = i;
    }
  }
#ifdef __DEBUG__
  printf("maxValue maxSample# %f %f \n", *maxValue, *maxSampleNum);
#endif
  return 0; /* process output links */
}

// //#############
// //### FFT #####
// //#############

static long FFTProc(aSubRecord *prec) {
  
  long i;
  
  //get (array) data from input link
  double *waveform_fft   = (double*)prec->a;
  
  /* FFT copy waveform data into FFT */
  for(i=0;i<int(prec->noa);i++) {
    fftw_in[i][0] = waveform_fft[i];
    fftw_in[i][1] = 0;
  }

  //   /* FFT perform transform */
  fftw_execute(fftw_p); 
  
 
  
  double *fftfreq;
  double *fftmagWf, *fftphaWf;
  double maxValue = 0;
  double maxNum = 0;
  
  fftmagWf = (double*)prec->vala;
  fftphaWf = (double*)prec->valb;
  fftfreq  = (double*)prec->valc;
  
  for(i=0;i<int(prec->noa/2);i++) {
    fftmagWf[i] = pow(fftw_out[i][0],2)+pow(fftw_out[i][1],2);
    fftphaWf[i] = atan2(fftw_out[i][1],fftw_out[i][0]);
    
    if (fftmagWf[i] > maxValue){
      maxValue = fftmagWf[i];
      maxNum = i;
    }
  }  
  
  *fftfreq = double(maxNum)/(double)(prec->noa);
  
  return 0;
}




// //###############
// //## sub Array ##
// //###############
static long subArrayProc(aSubRecord *prec) {
  
  double *Signal_sA      = (double*)prec->a;
  long   *GetStartNum    = (long*)prec->b;
  long   *GetSignalWind  = (long*)prec->c;
  double *TakenSignals   = (double*)prec->vala;
  
  long i;
  long j=0;
  
  for(i=0;i<int(prec->noa);i++){
    
    if ( i >= (*GetStartNum) && i <= (*GetStartNum + *GetSignalWind) ){
      TakenSignals[j] = Signal_sA[i];
      j++;
    }
  }
  
  return 0; /* process output links */
}

// //####################
// //## Shift waveform ##
// //####################

static long shiftWfProc(aSubRecord *prec) {
  
  double *Signal_sWf          = (double*)prec->a;
  long   *shift           = (long*)prec->b;
  double *shiftedSignal   = (double*)prec->vala;
  
  long i;
  for(i=0;i<int(prec->noa);i++){
    if (i < (*shift))shiftedSignal[i] = 0;
    else if (i >= (*shift))shiftedSignal[i] = Signal_sWf[i-(*shift)];
  }
  
  return 0; /* process output links */
}

// //########################
// //## digital complex LO ##
// //########################

static long complexLOProc(aSubRecord *prec) {
  
  double *ddcfreq    = (double*)prec->a;
  double *ddclOReal    = (double*)prec->vala;
  double *ddclOImag    = (double*)prec->valb;
  
  long i;
  double TWOPI = 6.28318530717958623;
  
  for (i=0; i<int(prec->nova); i++){
    ddclOReal[i] = cos(TWOPI * (*ddcfreq) * double(i) );
    ddclOImag[i] = sin(TWOPI * (*ddcfreq) * double(i) );
  }

  

  return 0; /* process output links */
}

static long calcomplexLOProc(aSubRecord *prec) {
  
  double *ddcfreq    = (double*)prec->a;
  double *calddcfreq    = (double*)prec->b;
  double *ddclOReal    = (double*)prec->vala;
  double *ddclOImag    = (double*)prec->valb;
  double *calddclOReal    = (double*)prec->valc;
  double *calddclOImag    = (double*)prec->vald;

  
  long i;
  double TWOPI = 6.28318530717958623;
  
  for (i=0; i<int(prec->nova); i++){
    ddclOReal[i] = cos(TWOPI * (*ddcfreq) * double(i) );
    ddclOImag[i] = sin(TWOPI * (*ddcfreq) * double(i) );
    calddclOReal[i] = cos(TWOPI * (*calddcfreq) * double(i) );
    calddclOImag[i] = sin(TWOPI * (*calddcfreq) * double(i) );
  }
  
  return 0; /* process output links */
}


// //###################
// //## LO * waveform ##
// //###################

static long complexDCProc(aSubRecord *prec) {
  
  double *data = (double*)prec->a;
  double *ddcloReal   = (double*)prec->b;
  double *ddcloImag   = (double*)prec->c;
  double *ddcdatamixReal   = (double*)prec->vala;
  double *ddcdatamixImag   = (double*)prec->valb;
  long i;
  
  for(i=0;i<int(prec->noa);i++){
    ddcdatamixReal[i] = data[i] * ddcloReal[i];
    ddcdatamixImag[i] = data[i] * ddcloImag[i];
  }
  
  return 0; /* process output links */
}





// //###################
// //## LO * waveform ##
// //###################

static long calcomplexDCProc(aSubRecord *prec) {
  
  double *data = (double*)prec->a;
  double *ddcloReal   = (double*)prec->b;
  double *ddcloImag   = (double*)prec->c;
  double *calddcloReal   = (double*)prec->d;
  double *calddcloImag   = (double*)prec->e;
  long *calsignalstart = (long*)prec->f;
  double *ddcdatamixReal   = (double*)prec->vala;
  double *ddcdatamixImag   = (double*)prec->valb;
  long i;
  
  for(i=0;i<int(prec->noa);i++){
    if(i<*calsignalstart){
      ddcdatamixReal[i] = data[i] * ddcloReal[i];
      ddcdatamixImag[i] = data[i] * ddcloImag[i];
    }
    else {
      ddcdatamixReal[i] = data[i] * calddcloReal[i];
      ddcdatamixImag[i] = data[i] * calddcloImag[i];
    }
  }
  
  return 0; /* process output links */
}

// //##################
// //## FIR (Filter) ##
// //##################

static long FilterProc(aSubRecord *prec) {
  
  double *coeff   = (double*)prec->a;
  long   *DDCFILTERSIZE  = (long*)prec->b;    
  double *ddcdatamixReal = (double*)prec->c;
  double *ddcdatamixImag = (double*)prec->d;
  
  double *ddcfilteredReal = (double*)prec->vala;
  double *ddcfilteredImag = (double*)prec->valb;
  
  long i,j;
  
  for(i=0;i<int(prec->noa);i++) {
    ddcfilteredReal[i] = 0.0;
    ddcfilteredImag[i] = 0.0;
    for(j=-(*DDCFILTERSIZE);j<=(*DDCFILTERSIZE);j++) {
      if(i+j > 0 && i+j < int(prec->noa) ) {
	ddcfilteredReal[i] += ddcdatamixReal[i+j]*coeff[j+(*DDCFILTERSIZE)];
	ddcfilteredImag[i] += ddcdatamixImag[i+j]*coeff[j+(*DDCFILTERSIZE)];
      }
    }
  }
  
  return 0; 
}


// //########################
// //## FIR Coeff (Filter) ##
// //########################

static long FltCoeffProc(aSubRecord *prec) {
  
  double *ddcfilterparams     = (double*)prec->a;
  long *DDCFILTERSIZE        = (long*)prec->b;
  long *ddcfiltertype         = (long*)prec->c;
  
  double *h = (double*)prec->vala;
  
  double SQRTTWOPI = 2.50662827463100024;
  double TWOPI     = 6.28318530717958623;
  
  long i;
  
  // generate filter coefficients
  // Frankie Gaussian
  if ((*ddcfiltertype == 1)){
    for(i=0;i<int(prec->nova);i++) {
      h[i] = SQRTTWOPI*(*ddcfilterparams)/1.0*exp(-pow((((double)i-(*DDCFILTERSIZE))*TWOPI*(*ddcfilterparams)),2)/(2.*pow(1.0,2)));
    }
  }
  //Gaussian
  else if ((*ddcfiltertype == 2)){
    for(i = 0;i<int(prec->nova);i++) {
      h[i] = exp(-(double)i*(double)i/2./(*ddcfilterparams)/(*ddcfilterparams))/SQRTTWOPI/(*ddcfilterparams);
    }
  }
  //     //half Frankie gaussian
  else if ((*ddcfiltertype == 3)){
    for(i = 0;i<int((prec->nova)/2);i++) {
      h[i] = SQRTTWOPI*(*ddcfilterparams)/1.0*exp(-pow(((double)i-(*DDCFILTERSIZE))*TWOPI*(*ddcfilterparams),2)/(2*pow(1.0,2)));
    }
  }
  return 0;
}


// //#########################
// //## Amplitude and Phase ##
// //######################### 

static long ampandphaseProc(aSubRecord *prec) {
  
  
  double *ddcfilteredReal   = (double*)prec->a;
  double *ddcfilteredImag   = (double*)prec->b;
  
  double *ddcamp     = (double*)prec->vala;
  double *ddcpha     = (double*)prec->valb;
  
  long i;
  
  for(i=0;i<int(prec->noa);i++){
    //ddcamp[i] = sqrt(ddcfilteredReal[i]*ddcfilteredReal[i] + ddcfilteredImag[i]*ddcfilteredImag[i]);
    ddcamp[i] = sqrt(pow(ddcfilteredReal[i],2) + pow(ddcfilteredImag[i],2));
    ddcpha[i] = atan2(ddcfilteredImag[i],ddcfilteredReal[i]);
  }
  
  return 0; 
}

// //######################
// // Sample amp signal ###
// //######################
static long SampleGetAmpProc(aSubRecord *prec) {
  
  double *ddcamp          = (double*)prec->a;
  double *t0              = (double*)prec->b;
  long   *nob             = (long*)prec->c;
  double *ddcisamp        = (double*)prec->d;
  long   *interpolation   = (long*)prec->e;
  double *ddcdecay        = (double*)prec->f;
  double *lastunsat       = (double*)prec->g;
  double *ddcfilterparams = (double*)prec->h;
  long   *delt            = (long*)prec->i;

  double *amp = (double*)prec->vala;
  
  long i; 
  double oldT;
  double ts[int(prec->nova)];
  double oldS;
  
  double nsp[int(prec->nova)];
  double msp[int(prec->nova)];
 
  double ext[int(prec->nova)];

  double timediff;
  double dt;
  dt = 1/(2.0*(*ddcfilterparams));

  for (i=0; i<int(prec->nova);i++){
    amp[i] = 0;
  }

  for(i=0;i<int(*nob);i++){
    nsp[i] = t0[i] + (*ddcisamp);
    msp[i] = int(lastunsat[i])+1/(2.0*(*ddcfilterparams));

    if (nsp[i]<msp[i]){
      ts[i]=msp[i];
      if (int(t0[i+1]) != 0){
	timediff = (t0[i+1]-int(*delt))-msp[i];
	//	printf("more than one bunch:t0[i+1]>%lf, timediff>%lf, dt>%lf\n",t0[i+1],timediff,dt);
	if (timediff < dt){
	  ts[i] = (t0[i+1]-int(*delt)+lastunsat[i])/2.0;
	  //	  printf("timediff<dt:ts>%lf\n",ts[i]);
	}
      }
      ext[i] = 1;
    }
    else{
      ts[i] = nsp[i];
      if(int(*interpolation)==1){
	ext[i] = 2;
      }
      else if (int(*interpolation)==0){
	ext[i] = 0;
      }
    }
  }

  //  printf("lastunsat>%lf t0>%lf nsp>%lf msp>%lf ts>%lfinterpolation>%lu  ext>%lf\n",lastunsat[0],t0[0],nsp[0],msp[0],ts[0],*interpolation,ext[0]);

  for(i=0;i<int(*nob);i++){
    amp[i] = ddcamp[int(ts[i])];
    
    if (int(ext[i]) == 1){
      amp[i] = amp[i] * exp((nsp[i] - int(ts[i]))/(*ddcdecay));
      //  printf("ts>%lf amp>%lf\n",ts[i],amp[i]);
    }
    else if (int(ext[i]) == 2){
      amp[i] = amp[i] * exp((ts[i] - int(ts[i]))/(*ddcdecay));
    }   
  }
   
  return 0;
}

// //########################
// // Sample phase signal ###
// //########################

static long SampleGetPhaProc(aSubRecord *prec) {
  double *ddcpha          = (double*)prec->a;
  double *t0              = (double*)prec->b; // diode t0
  long   *nob             = (long*)prec->c;
  double *ddcisamp        = (double*)prec->d;
  long   *interpolation   = (long*)prec->e;
  double *ddcdecay        = (double*)prec->f;
  double *lastunsat       = (double*)prec->g;
  double *ddcfilterparams = (double*)prec->h;
  long   *delt            = (long*)prec->i;

  double *pha = (double*)prec->vala;
  
  long i; 
  double oldT;
  double newT;
  double ts[int(prec->nova)];
  double oldS;
  double newS;

  double nsp[int(prec->nova)];
  double msp[int(prec->nova)];
 
  double ext[int(prec->nova)];

  double timediff;
  double dt;

  dt = 1/(2.0*(*ddcfilterparams));

  for(i=0;i<int(prec->nova);i++){
    pha[i] = 0;
  }

  for(i=0;i<int(*nob);i++){
    nsp[i] = t0[i] + (*ddcisamp);
    msp[i] = int(lastunsat[i])+1/(2.0*(*ddcfilterparams));

    if (nsp[i]<msp[i]){
      ts[i]=msp[i];
      if (t0[i+1] != 0){
	timediff = (t0[i+1]-int(*delt)) - msp[i];
	if (timediff < dt){
	  ts[i] = (t0[i+1]-int(*delt)+lastunsat[i])/2.0;
	}
      }
    }
    else{
      ts[i] = nsp[i];
      if(int(*interpolation)==1){
	ext[i] = 2;
      } 
      else if (int(*interpolation)==0){
	ext[i] = 0;
      }
    }

  }
  //  printf("lastunsat>%lf nsp>%lf msp>%lf ts>%lf extra>%lu  ext>%lf\n",lastunsat[0],nsp[0],msp[0],ts[0],*extra,ext[0]);

  for(i=0;i<int(*nob);i++){ 
    pha[i] = ddcpha[int(ts[i])];
    if (int(ext[i]) == 2){
      modf( ts[i], &oldT );
      newT = oldT + 1.;
      oldS = ddcpha[int(oldT)];
      newS = ddcpha[int(newT)];
      pha[i] = oldS + (oldS - newS)/(oldT - newT)*(ts[i]-oldT);
    }
  }
  
  return 0;
}


/*
static long SampleGetAmpProc(aSubRecord *prec) {
  
  double *ddcamp   = (double*)prec->a;
  double *t0       = (double*)prec->b;
  double *ddcisamp = (double*)prec->c;
  long   *extra    = (long*)prec->d;
  double *ddcdecay = (double*)prec->e;
  double *lastunsat= (double*)prec->f;
  double *ddcfilterparams = (double*)prec->g;

  double *amp = (double*)prec->vala;
  
  long i; 
  double oldT;
  double ts[int(prec->nova)];
  double oldS;
  
  double nsp[int(prec->nova)];
  double msp[int(prec->nova)];
 
  double ext[int(prec->nova)];

  for(i=0;i<int(prec->nova);i++){
    //printf("t0>%lf lastun>%lf\n",t0[i],lastunsat[i]);
    nsp[i] = t0[i] + (float)*ddcisamp;
    msp[i] = int(lastunsat[i]) + 1/(2.0*(*ddcfilterparams));

    if (nsp[i]<msp[i]){
      ts[i]=msp[i];
      ext[i] = 1;
    }
    else{
      ts[i] = nsp[i];
      if(int(*extra)==1){
	ext[i] = 2;
      }
      else if (int(*extra)==0){
	ext[i] = 0;
      }
    }
  }

  //printf("lastunsat>%lf nsp>%lf msp>%lf ts>%lf extra>%lu  ext>%lf\n",lastunsat[0],nsp[0],msp[0],ts[0],*extra,ext[0]);

  for(i=0;i<int(prec->nova);i++){
    amp[i] = ddcamp[int(ts[i])];
    //printf("amp>%lf\n",amp[i]);
    if (int(ext[i]) == 1){
      amp[i] = amp[i] * exp((nsp[i] - (int)ts[i])/(*ddcdecay));
      //printf("ts>%lf nsp>%lf ddcdecay>%lf amp>%lf\n",ts[i],nsp[i],*ddcdecay,amp[i]);
    }
    
    else if (int(ext[i]) == 2){
      modf( ts[i], &oldT );
      oldS = ddcamp[int(oldT)];
      amp[i] = oldS*exp((-(oldT)+ts[i])/(*ddcdecay));
      }    
  }
 
  return 0;
}
*/

// //######################
// // Sample amp signal ###
// //######################

static long calSampleGetAmpProc(aSubRecord *prec) {
  
  double *ddcamp   = (double*)prec->a;
  double *t0  = (double*)prec->b;
  double *ddcisamp = (double*)prec->c;
  long *nbunch     = (long*)prec->d;
  double *ddcdecay = (double*)prec->e;
  double *calddcisamp = (double*)prec->f;
  
  double *amp = (double*)prec->vala;
  double *calcamp = (double*)prec->valb;
  
  long i; 
  double oldT;
  double ts[int(*nbunch)+1];
  double oldS;
  
  for(i=0;i<int(*nbunch);i++){
    ts[i] = t0[i] + *ddcisamp;
    modf( ts[i], &oldT );
    oldS = ddcamp[int(oldT)];
   
    amp[i] = oldS*exp(((oldT)-ts[i])/(*ddcdecay));
  }
  *calcamp = ddcamp[int(*calddcisamp)];
  
  
  return 0; /* process output links */
}



// //########################
// // Sample phase signal ###
// //########################

/*

static long SampleGetPhaProc(aSubRecord *prec) {
  double *ddcpha   = (double*)prec->a;
  double *t0       = (double*)prec->b; // diode t0
  double *ddcisamp = (double*)prec->c;
  long   *extra    = (long*)prec->d;
  double *ddcdecay = (double*)prec->e;
  double *lastunsat= (double*)prec->f;
  double *ddcfilterparams = (double*)prec->g;

  double *pha = (double*)prec->vala;
  
  long i; 
  double oldT;
  double newT;
  double ts[int(prec->nova)];
  double oldS;
  double newS;

  double nsp[int(prec->nova)];
  double msp[int(prec->nova)];
 
  double ext[int(prec->nova)];

  for(i=0;i<int(prec->nova);i++){
    nsp[i] = t0[i] +float(*ddcisamp);
    msp[i] = int(lastunsat[i])+1/(2.0*(*ddcfilterparams));

    if (nsp[i]<msp[i]){
      ts[i]=msp[i];
      ext[i] = 1;
    }
    else{
      ts[i] = nsp[i];
      if(int(*extra)==1){
	ext[i] = 2;
      } 
      else if (int(*extra)==0){
	ext[i] = 0;
      }
    }

  }
  //  printf("lastunsat>%lf nsp>%lf msp>%lf ts>%lf extra>%lu  ext>%lf\n",lastunsat[0],nsp[0],msp[0],ts[0],*extra,ext[0]);

  for(i=0;i<int(prec->nova);i++){ 
    pha[i] = ddcpha[int(ts[i])];
    
    if (int(ext[i]) == 1){      
      modf( ts[i], &oldT );
      newT = oldT + 1.;
      oldS = ddcpha[int(oldT)];
      newS = ddcpha[int(newT)];
      pha[i] = oldS + (oldS - newS)/(oldT - newT)*(nsp[i]-oldT);
    }
    
    if (int(ext[i]) == 2){
      modf( ts[i], &oldT );
      newT = oldT + 1.;
      oldS = ddcpha[int(oldT)];
      newS = ddcpha[int(newT)];
      pha[i] = oldS + (oldS - newS)/(oldT - newT)*(ts[i]-oldT);
    }
  }
  
  return 0;
}
*/

static long calSampleGetPhaProc(aSubRecord *prec) {
    
  double *ddcpha   = (double*)prec->a;
  double *t0       = (double*)prec->b; // diode t0
  double *ddcisamp = (double*)prec->c;
  long *nbunch     = (long*)prec->d;
  double *ddcdecay = (double*)prec->e;
  double *calddcisamp = (double*)prec->f;
  
  double *pha = (double*)prec->vala;
  double *calcpha = (double*)prec->valb;
   
  long i; 
  double oldT;
  double newT;
  double ts[int(*nbunch)+1];
  double oldS = 0;
  double newS = 0;
  
  for(i=0;i<int(*nbunch);i++){
    ts[i] = t0[i] + *ddcisamp;
    modf( ts[i], &oldT );
    newT = oldT + 1.;
    oldS = ddcpha[int(oldT)];
    newS = ddcpha[int(newT)];
    
    pha[i] = oldS + (oldS - newS)/(oldT - newT)*(ts[i]-oldT);
  }
  *calcpha = ddcpha[int(*calddcisamp)];
   
  return 0; /* process output links */
}



// //###########################
// //## Grab all positions    ##
// //###########################

static long cbpmPosGrabProc(aSubRecord *prec) {

  double *bpm1x   = (double*)prec->a;
  double *bpm1y   = (double*)prec->b;
  double *bpm2x   = (double*)prec->c;
  double *bpm2y   = (double*)prec->d;
  double *bpm3x   = (double*)prec->e;
  double *bpm3y   = (double*)prec->f;
  long *nbunch   = (long*)prec->g;
  long *nbpm   = (long*)prec->h;
  double *wfin = (double*)prec->i;
  double *x    = (double*)prec->vala;
  double *y    = (double*)prec->valb;

 

  long i;
  for(i=0;i<int(*nbunch);i++){
    
    x[int(*nbpm)*i] = bpm1x[i];
    x[1+int(*nbpm)*i] = bpm2x[i];
    x[2+int(*nbpm)*i] = bpm3x[i];
    
    y[int(*nbpm)*i] = bpm1y[i];
    y[1+int(*nbpm)*i] = bpm2y[i];
    y[2+int(*nbpm)*i] = bpm3y[i];
  }

  //printf("cbpm processing done> %f\n");
  
  return 0; /* process output links */
}


 //#############
// //## I and Q ##
// //#############

static long IQPosTiltProc(aSubRecord *prec) {
 
  double *ampSig       = (double*)prec->a;
  double *phaSig       = (double*)prec->b;
  double *ampRef       = (double*)prec->c;
  double *phaRef       = (double*)prec->d;
  double *posscale     = (double*)prec->e;
  double *iqrot        = (double*)prec->f;
  double *tiltscale    = (double*)prec->g;
  double *bbaoffset    = (double*)prec->h;
  long   *nbpms        = (long*)prec->i;
  double *ddcfreqdip   = (double*)prec->j;
  double *ddcfreqref   = (double*)prec->k;
  double *isdiode      = (double*)prec->l;
  double *calt0        = (double*)prec->m;
  double *posscaleflip = (double*)prec->n;
  double *posflip      = (double*)prec->o;
  long   *nob          = (long*)prec->p;
  
  double *I        = (double*)prec->vala;
  double *q        = (double*)prec->valb;
  double *pos      = (double*)prec->valc;
  double *tilt     = (double*)prec->vald;
   
  long i;
  double PI = 3.14159255358979312;
  
  //  ca_pend_io(0.2);
  // frequency split correction 
  double dp;

  for(i=0;i<int(prec->novc);i++){
    pos[i] = 0;
    tilt[i] = 0;
  }
  
  for(i=0;i<int(*nob);i++){
    dp = 2*PI*((*ddcfreqdip) - (*ddcfreqref))*(isdiode[i] - (*calt0));
    I[i] = ampSig[i]/ampRef[i] * cos(phaSig[i] - phaRef[i] - dp);
    q[i] = ampSig[i]/ampRef[i] * sin(phaSig[i] - phaRef[i] - dp);
    pos[i]  = (*posscale)*(ampSig[i])/(ampRef[i]) * cos((phaSig[i] - phaRef[i]) - dp - (*iqrot));
    tilt[i] = (*tiltscale)*(ampSig[i])/(ampRef[i]) * sin((phaSig[i] - phaRef[i]) - dp - (*iqrot));
    
    // Position scale flip 
    pos[i] = (*posscaleflip)*(pos[i]);

    // subtract bba 
    pos[i] -= *bbaoffset;
    
    // Position flip
    pos[i] = (*posflip)*(pos[i]);

    //printf("amp %f pha %f ampr %f phar %f\n",ampSig[i],phaSig[i],ampRef[i],phaRef[i]);
    //printf("dp : %f pos: %f  \n",dp,pos[i]);
  }
  
  return 0; 
}

 //#############
// //## I and Q ##
// //#############

static long calIQPosTiltProc(aSubRecord *prec) {
 
  double *ampSig    = (double*)prec->a;
  double *phaSig    = (double*)prec->b;
  double *ampRef    = (double*)prec->c;
  double *phaRef    = (double*)prec->d;
  double *calampSig    = (double*)prec->e;
  double *calphaSig    = (double*)prec->f;
  double *calampRef    = (double*)prec->g;
  double *calphaRef    = (double*)prec->h;

  double *posscale  = (double*)prec->i;
  double *iqrot     = (double*)prec->j;
  double *tiltscale = (double*)prec->k;
  double *bbaOffset = (double*)prec->l;
  long   *nbpms     = (long*)prec->m;

  double   *ddcfreq    = (double*)prec->n;
  double   *ddcfreqRef = (double*)prec->o;
  double   *t0         = (double*)prec->p;
  double   *calt0      = (double*)prec->q;
  
  double *i        = (double*)prec->vala;
  double *q        = (double*)prec->valb;
  double *cali     = (double*)prec->valc;
  double *calq     = (double*)prec->vald;
  double *pos      = (double*)prec->vale;
  double *tilt     = (double*)prec->valf;
   
  long nb;
  double dp;
  double PI = 3.14159255358979312;
  


  /* frequency split correction */
  dp = 2*PI*((*ddcfreq) - (*ddcfreqRef))*((*t0) - (*calt0));
  
  for(nb=0;nb<int(prec->noa);nb++){
    
    i[nb] = ampSig[nb]/ampRef[nb] * cos(phaSig[nb] - phaRef[nb]-dp);
    q[nb] = ampSig[nb]/ampRef[nb] * sin(phaSig[nb] - phaRef[nb]-dp);

  }

  /* Calibration tone I and Q */	
  *cali = *calampSig/(*calampRef)*cos((*calphaSig)-(*calphaRef));
  *calq = *calampSig/(*calampRef)*sin((*calphaSig)-(*calphaRef));
 
  for(nb=0;nb<int(prec->noa);nb++){
    pos[nb]  = -*bbaOffset + *posscale * ampSig[nb]/ampRef[nb]*cos((phaSig[nb]-phaRef[nb])-dp-*iqrot);
    tilt[nb] = *tiltscale * ampSig[nb]/ampRef[nb]*sin((phaSig[nb]-phaRef[nb])-dp-*iqrot);
    
    printf("amp %f pha %f ampr %f phar %f\n",ampSig[nb],phaSig[nb],ampRef[nb],phaRef[nb]);
    printf("pos: %f  \n",pos[nb]);
  }
  
  return 0; 
}

// //#############
// //## I and Q ##
// //#############

static long sampProc(aSubRecord *prec) {

  double *val    = (double*)prec->a;
  long *isamp    = (long*)prec->b;
   
  double *outval        = (double*)prec->vala;
   
  *outval = val[int(*isamp)];
   
  return 0; 
}


//#################
//## Diode proc. ##
//#################

static long DerivProc(aSubRecord *prec) {
  
  long i,j;
  
  //get wf data from input link
  double *wfin  = (double*)prec->a;
  //output link
  double *wfout = (double*)prec->vala;
  double wffilt[prec->noa];

  /*
  // filter wf
  long fwidth = 20;
  double sig = 3.;
  double h[fwidth];
  for(j=0;j<fwidth;j++) {
    h[j] = exp(-(double)j*(double)j/2./sig/sig)/sig;
  }

  for(i=0;i<int(prec->noa);i++) {
    wffilt[i] = 0.0;
    for(j=-int(fwidth/2);j<int(fwidth/2);j++) {
      if(i+j >= 0 && i+j < int(prec->noa) ) {
        wffilt[i] += wfin[i+j]*h[j];
      }
    }
  }
  */

  //diff Wf data
  for(i=1;i<prec->nova;i++){
    wfout[i] = wfin[i]-wfin[i-1];
    //wfout[i] = wffilt[i]; //-wffilt[i-1];
    //wfout[i] = fabs(wfin[i]-wfin[i-1]);
  }
  wfout[0] = 0.;
   
  return 0; /* process output links */
}

static double ParabolaVertex(double *vxy3) {
   
  double x1 = vxy3[0];
  double x2 = vxy3[1];
  double x3 = vxy3[2];
  double y1 = vxy3[3];
  double y2 = vxy3[4];
  double y3 = vxy3[5];
  
  double vx;
  
  double denom = (x1 - x2) * (x1 - x3) * (x2 - x3); 
  double A     = (x3 * (y2 - y1) + x2 * (y1 - y3) + x1 * (y3 - y2)) / denom;
  double B     = (x3 * x3 * (y1 - y2) + x2 * x2 * (y3 - y1) + x1 * x1 * (y2 - y3)) / denom;
  double C     = (x2 * x3 * (x2 - x3) * y1 + x3 * x1 * (x3 - x1) * y2 + x1 * x2 * (x1 - x2) * y3) / denom;

  //printf("A B C %f %f %f\n", A, B, C);
    
  vx = -B / (2 * A);
  //printf("vx= %f\n", vx);
  //vy = C - B * B / (4 * A);
  
  return vx;
}

static long PeaksProc(aSubRecord *prec) { 

  long countmn = 0;
  long countmx = 0;
  long i, mnpos, mxpos;
  double delta = 0.1; //sensitivity setting
  double mn = 0;
  double mx = 0;
  double curr;
  int lookformax = 1; 

  long nmins;
  long nmaxs;
  long mins[prec->noa];
  long maxs[prec->noa];

  //get wf data from input link
  double *v    = (double*)prec->a;

  //output link
  long   *nob = (long*)prec->vala;
  double *t0  = (double*)prec->valb;

  double maxall = 0.;
  for(i=0;i<prec->noa;i++) {
    if (v[i] > maxall) {
      maxall = v[i];
    } 
  }

  for(i=0;i<prec->noa;i++) {
    curr = v[i];
    //printf("curr= %f\n", curr);
    if (curr > mx) {
      mx = curr;
      mxpos = i;
    }
    if (curr < mn) {
      mn = curr;
      mnpos = i;
    }
    
    if (lookformax) {
      if ((curr < mx-delta*fabs(mx))&&(curr > delta*maxall)) {
        maxs[countmx] = i;
        countmx++; 
        mn = curr;
        mnpos = i;
        lookformax = 0;
      } 
    } else {
      if (curr > mn+delta*fabs(mn)) {
        mins[countmn] = i;
        countmn++; 
        mx = curr;
        mxpos = i;
        lookformax = 1; 
      }
    }


    //printf("i=%ld, curr=%f, countmx=%ld, countmn=%ld\n", i,curr,countmx,countmn);
    
  }

  nmins = countmn;
  nmaxs = countmx;
 
  double vxy[6];
  long vcount = 0;
  long currmax;
#ifdef __DEBUG__
  printf("Detected %ld peaks\n", nmaxs);
#endif
  for (i=0;i<nmaxs;i++) {
    currmax = maxs[i]-1; // -1 is a hot fix - understand!
    //printf("currmax= %ld\n", currmax);
    if (currmax > 0) {
      //printf("currmax= %ld\n", currmax);
      vxy[0] = currmax-1; 
      vxy[1] = currmax; 
      vxy[2] = currmax+1; 
      vxy[3] = v[currmax-1]; 
      vxy[4] = v[currmax]; 
      vxy[5] = v[currmax+1]; 
      t0[vcount] = ParabolaVertex(vxy);
#ifdef __DEBUG__
      printf("der= %f, i0= %ld, t0= %f\n",v[currmax],currmax,t0[vcount]);
#endif
      vcount++; 
    } 

  *nob = vcount;

  }

  return 0; /* process output links */
}




//########################
//## register functions ##
//########################
epicsRegisterFunction(SubPedestalInit);
epicsRegisterFunction(SubPedestalProc);
epicsRegisterFunction(reverseSearchInit);
epicsRegisterFunction(reverseSearchProc);
epicsRegisterFunction(PedestalInit);
epicsRegisterFunction(PedestalProc);
epicsRegisterFunction(SumInit);
epicsRegisterFunction(SumProc);
epicsRegisterFunction(SubConstWfInit);
epicsRegisterFunction(SubConstWfProc);
epicsRegisterFunction(AddWfInit);
epicsRegisterFunction(AddWfProc);
epicsRegisterFunction(MultConstWfInit);
epicsRegisterFunction(MultConstWfProc);
epicsRegisterFunction(MultWfInit);
epicsRegisterFunction(MultWfProc);
epicsRegisterFunction(MaxSignalInit);
epicsRegisterFunction(MaxSignalProc);
epicsRegisterFunction(FFTInit);
epicsRegisterFunction(FFTProc);
epicsRegisterFunction(FltCoeffInit);
epicsRegisterFunction(FltCoeffProc);
epicsRegisterFunction(FilterInit);
epicsRegisterFunction(FilterProc);
epicsRegisterFunction(SampleGetAmpInit);
epicsRegisterFunction(SampleGetAmpProc);
epicsRegisterFunction(SampleGetPhaInit);
epicsRegisterFunction(SampleGetPhaProc);
epicsRegisterFunction(subArrayInit);
epicsRegisterFunction(subArrayProc);
epicsRegisterFunction(shiftWfInit);
epicsRegisterFunction(shiftWfProc);
epicsRegisterFunction(complexLOInit);
epicsRegisterFunction(complexLOProc);
epicsRegisterFunction(complexDCInit);
epicsRegisterFunction(complexDCProc);
epicsRegisterFunction(ampandphaseInit);
epicsRegisterFunction(ampandphaseProc);
epicsRegisterFunction(IQPosTiltInit);
epicsRegisterFunction(IQPosTiltProc);
epicsRegisterFunction(DerivInit);
epicsRegisterFunction(DerivProc);
epicsRegisterFunction(PeaksInit);
epicsRegisterFunction(PeaksProc);
epicsRegisterFunction(cbpmPosGrabProc);
epicsRegisterFunction(cbpmPosGrabInit);
epicsRegisterFunction(sampProc);
epicsRegisterFunction(sampInit);
epicsRegisterFunction(calIQPosTiltProc);
epicsRegisterFunction(calIQPosTiltInit);
epicsRegisterFunction(calSampleGetPhaProc);
epicsRegisterFunction(calSampleGetPhaInit);
epicsRegisterFunction(calSampleGetAmpProc);
epicsRegisterFunction(calSampleGetAmpInit);
epicsRegisterFunction(calcomplexDCProc);
epicsRegisterFunction(calcomplexDCInit);
epicsRegisterFunction(calcomplexLOProc);
epicsRegisterFunction(calcomplexLOInit);


