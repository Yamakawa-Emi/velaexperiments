#!../../bin/linux-x86/bpm

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/bpm.dbd" 
bpm_registerRecordDeviceDriver pdbbase
dbLoadRecords("db/cbpmLabVIEW.db","name=c2")
dbLoadRecords("db/plot.db","name=plot")
dbLoadRecords("db/scan.db","name=scan")
dbLoadRecords("db/system.vdb","cbpm=cbpm,nb=2")

cd ${TOP}/iocBoot/${IOC}
iocInit()
## Start any sequence programs
#seq sncxxx,"user=ipsbpm"
