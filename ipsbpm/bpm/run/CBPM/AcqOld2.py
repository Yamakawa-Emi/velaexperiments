#! /usr/bin/env python

import cothread
import cothread.catools as ca
import Data
import OrderedDict
import numpy as np
import time
import optparse
import gzip

import ControlMovers
from OrderedDict import OrderedDict as od

class Acq :
    
    def __init__(self,filezip,suffix,infolist=['NA']) :
       
        self.d          = Data.Data()
        self.triggervar = 'RP2y:ddcfilteredImag' #'c2:Trig'
        self.QuitEvent  = cothread.Event(auto_reset = False)
        self.nacquired  = 0
        self.clnacquired  = 0
        self.cl2Dnacquired  = 0
        self.infolist   = infolist
        self.suffix     = suffix
      
        ca.caput('c2:progress',0.0)
        ca.caput('scan:progress',0.0)
        ca.caput('c2:cpulse',0.0)
        ca.caput('scan:cpulse',0.0)
        
        self.d.NewFile(filezip,suffix,infolist)
        self.filename = self.d.filename
        self.filepath = self.d.filepath
        print 'New Data File '+self.filepath

        VELAmagnets   = ['EBT-INJ-MAG-BSOL-01',
                         'EBT-INJ-MAG-SOL-01',
                         'EBT-INJ-MAG-VCOR-01',
                         'EBT-INJ-MAG-HCOR-01',
                         'EBT-INJ-MAG-VCOR-02',
                         'EBT-INJ-MAG-HCOR-02',
                         'EBT-INJ-MAG-QUAD-01',
                         'EBT-INJ-MAG-QUAD-02',
                         'EBT-INJ-MAG-QUAD-03',
                         'EBT-INJ-MAG-QUAD-04',
                         'EBT-INJ-MAG-VCOR-03',
                         'EBT-INJ-MAG-HCOR-03',
                         'EBT-INJ-MAG-VCOR-04',
                         'EBT-INJ-MAG-HCOR-04',
                         'EBT-INJ-MAG-DIP-01',
                         'EBT-INJ-MAG-VCOR-05',
                         'EBT-INJ-MAG-HCOR-05',
                         'EBT-INJ-MAG-QUAD-05',
                         'EBT-INJ-MAG-QUAD-06',
                         'EBT-INJ-MAG-VCOR-06',
                         'EBT-INJ-MAG-HCOR-06',
                         'EBT-INJ-MAG-QUAD-07',
                         'EBT-INJ-MAG-QUAD-08',
                         'EBT-INJ-MAG-VCOR-07',
                         'EBT-INJ-MAG-HCOR-07',
                         'EBT-INJ-MAG-QUAD-09',
                         'EBT-INJ-MAG-VCOR-08',
                         'EBT-INJ-MAG-HCOR-08',
                         'EBT-INJ-MAG-QUAD-10',
                         'EBT-INJ-MAG-QUAD-11',
                         'EBT-INJ-MAG-VCOR-09',
                         'EBT-INJ-MAG-HCOR-09',
                         'EBT-INJ-MAG-DIP-02',
                         'EBT-INJ-MAG-QUAD-12',
                         'EBT-INJ-MAG-DIP-03',
                         'EBT-INJ-MAG-QUAD-13',
                         'EBT-INJ-MAG-QUAD-10',
                         'EBT-INJ-MAG-QUAD-11']
        magpvs            = [x+':RI' for x in VELAmagnets]

        VELAvacuums        = ['EBT-INJ-VAC-IMG-01',
                             'EBT-INJ-VAC-PIRG-01',
                             'EBT-INJ-VAC-IONP-01',
                             'EBT-INJ-VAC-IONP-02',
                             'EBT-INJ-VAC-IONP-12',
                             'EBT-INJ-VAC-IONP-13',
                             'EBT-INJ-VAC-IONP-14',
                             'EBT-INJ-VAC-IMG-07',
                             'EBT-INJ-VAC-PIRG-07',
                             'EBT-INJ-VAC-IMG-08',
                             'EBT-INJ-VAC-PIRG-08',
                             'EBT-INJ-VAC-IMG-09',
                             'EBT-INJ-VAC-PIRG-09',
                             'EBT-INJ-VAC-IMG-02',
                             'EBT-INJ-VAC-PIRG-02',
                             'EBT-INJ-VAC-IONP-03',
                             'EBT-INJ-VAC-IONP-04',
                             'EBT-INJ-VAC-IONP-05',
                             'EBT-INJ-VAC-IONP-06',
                             'EBT-INJ-VAC-IMG-03',
                             'EBT-INJ-VAC-PIRG-03',
                             'EBT-INJ-VAC-IONP-07',
                             'EBT-INJ-VAC-IMG-10',
                             'EBT-INJ-VAC-PIRG-10',
                             'EBT-INJ-VAC-IONP-15',
                             'EBT-INJ-VAC-IMG-11',
                             'EBT-INJ-VAC-PIRG-11',
                             'EBT-INJ-VAC-IONP-08',
                             'EBT-INJ-VAC-IMG-04',
                             'EBT-INJ-VAC-PIRG-04',
                             'EBT-INJ-VAC-IONP-09',
                             'EBT-INJ-VAC-IONP-10',
                             'EBT-INJ-VAC-IMG-05',
                             'EBT-INJ-VAC-PIRG-05',
                             'EBT-INJ-VAC-IMG-06',
                             'EBT-INJ-VAC-PIRG-06',
                             'EBT-INJ-VAC-IONP-11']
                             
        vacpvs            = [x + ':P' for x in VELAvacuums]
       
        self.vackeys      = dict(zip(VELAvacuums,vacpvs))
        self.magkeys      = dict(zip(VELAmagnets,magpvs))
        self.cbpmSetupkeys = {
            'Ini_SetPosi'       :'c2:Posi',
            'Ini_SetSpeed'      :'c2:Speed',
            'Ini_readPosiH'    :'c2:OutHoriPosi',
            'Ini_readPosiV'    :'c2:OutVerPosi',
            'Ini_ADC0'          :'c2:ADC0',
            'Ini_ADC1'          :'c2:ADC1',
            'Ini_Axis'          :'c2:Axis',
            'npulse'        :'c2:npulse',
            'SimpleMove'    :'c2:SimpleMove'
            }
        
        self.cbpmScankeys = {
            'Ini_SetPosi'       :'c2:Posi',
            'Ini_SetSpeed'      :'c2:Speed',
            'Ini_readPosiH'   :'c2:OutHoriPosi',
            'Ini_readPosiV'   :'c2:OutVerPosi',
            'Ini_ADC0'          :'c2:ADC0',
            'Ini_ADC1'          :'c2:ADC1',
            'Ini_Axis'          :'c2:Axis',
            'SimpleMove'    :'c2:SimpleMove',
            'scannpulse'   :'scan:npulse',
            'scanprogress' :'scan:progress',
            'scanrange'    :'scan:range',
            'scannstep'    :'scan:nstep'
            }

        self.cbpm2DScankeys = {
            'Ini_SetPosi'       :'c2:Posi',
            'Ini_SetSpeed'      :'c2:Speed',
            'Ini_readPosiH'     :'c2:OutHoriPosi',
            'Ini_readPosiV'     :'c2:OutVerPosi',
            'Ini_ADC0'          :'c2:ADC0',
            'Ini_ADC1'          :'c2:ADC1',
            'Ini_Axis'          :'c2:Axis',
            'SimpleMove'        :'c2:SimpleMove',
            'Ini_2Dscannpulse'  :'scan:2Dnpulse',
            'Ini_2Dscanrange_1st' :'scan:2Drange1stMove',
            'Ini_2Dscanrange_2nd' :'scan:2Drange2ndMove',
            'Ini_2Dscannstep_1st' :'scan:2Dnstep1stMove',
            'Ini_2Dscannstep_2nd' :'scan:2Dnstep2ndMove'
            }

        
        self.cbpmPositionkeys = {
            'SetcbpmPosi'  :'c2:Posi'
            #'readPosiH'    :'c2:OutHoriPosi',
            #'readPosiV'    :'c2:OutVerPosi'
            }

        self.bpmPositionkeys = {
             'BPM01X'     :'VELABPM:1X',
             'BPM02X'     :'VELABPM:2X',
             'BPM03X'     :'VELABPM:3X',
             'BPM04X'     :'VELABPM:4X',
             'BPM05X'     :'VELABPM:5X',
             'BPM01Y'     :'VELABPM:1Y',
             'BPM02Y'     :'VELABPM:2Y',
             'BPM03Y'     :'VELABPM:3Y',
             'BPM04Y'     :'VELABPM:4Y',
             'BPM05Y'     :'VELABPM:5Y'
             }

        self.bpmVOLPositionkeys = {
             'BPM01VOL'     :'VELABPMVOL:1',
             'BPM02VOL'     :'VELABPMVOL:2',
             'BPM03VOL'     :'VELABPMVOL:3',
             'BPM04VOL'     :'VELABPMVOL:4',
             'BPM05VOL'     :'VELABPMVOL:5'
             }

        
        self.pulseInfokeys = {
            'readPosiH'    :'c2:OutHoriPosi',
            'readPosiV'    :'c2:OutVerPosi',
            'logcpulse'       :'c2:cpulse',
            'scanrelativeposi':'scan:rlposi',
            'scancpulse'      :'scan:cpulse',
            'scancstep'       :'scan:cstep'
            }

    
        self.pulse2DscanInfokeys = {
            'scan2Dcpulse'           :'scan:2Dcpulse',
            'scan2Dcstep1st'         :'scan:2Dcstep1st',
            'scan2Dcstep2nd'         :'scan:2Dcstep2nd',
            'scan2DrelativePosi1st'  :'scan:2Drlposi_1st',
            'scan2DrelativePosi2nd'  :'scan:2Drlposi_2nd'
             }
 
        if self.suffix == 'INI':
            self.datakeys = od({
                'c2:VELAsetup'      :'c2:VELAsetup',
                'c2:Posi'           :'c2:Posi',
                'c2:Axis'           :'c2:Axis',
                'c2:status'         :'c2:status',
                'c2:SimpleMove'     :'c2:SimpleMove',
                'scan:npulse'       :'c2:scan:npulse',
                'scan:progress'     :'scan:progress',
                'scan:range'        :'scan:range',
                'scan:nstep'        :'scan:nstep'
                })

        elif (self.suffix == 'logsl2') or (self.suffix == 'scansl2') or (self.suffix == 'scan2Dsl2') :
            self.datakeys = od({
                'slot2ch0' :'c2:wfmsl2ch0',
                'slot2ch1' :'c2:wfmsl2ch1'
                })

        elif (self.suffix == 'logRP') or (self.suffix == 'scanRP') :
            self.datakeys = od({
                'cha:wf'              : 'cha:wf',
                'chb:wf'              : 'chb:wf',
                'EBT-INJ-SCOPE-01:P1' : 'EBT-INJ-SCOPE-01:P1',
		'RP2x:pedestal'       : 'RP2x:pedestal',
		'RP2x:freq'           : 'RP2x:freq',
		'RP2x:ddcdatamixReal' : 'RP2x:ddcdatamixReal',
		'RP2x:ddcdatamixImag' : 'RP2x:ddcdatamixImag',
                'RP2x:ddcfilteredReal': 'RP2x:ddcfilteredReal',
                'RP2x:ddcfilteredImag': 'RP2x:ddcfilteredImag',
		'RP2y:pedestal'       : 'RP2y:pedestal',
                'RP2y:freq'           : 'RP2y:freq',
                'RP2y:ddcdatamixReal' : 'RP2y:ddcdatamixReal',
                'RP2y:ddcdatamixImag' : 'RP2y:ddcdatamixImag',
                'RP2y:ddcfilteredReal': 'RP2y:ddcfilteredReal',
                'RP2y:ddcfilteredImag': 'RP2y:ddcfilteredImag'
                })
            
     
        elif (self.suffix == 'logsl3') or (self.suffix == 'scansl3') or (self.suffix == 'scan2Dsl3'):
            self.datakeys = od({
                'slot3ch0' :'c2:wfmsl3ch0',
                'slot3ch1' :'c2:wfmsl3ch1'
                })

        else:
            self.datakeys = od({
                'BPM01X'     :'VELABPM:1X',
                'BPM02X'     :'VELABPM:2X',
                'BPM03X'     :'VELABPM:3X',
                'BPM04X'     :'VELABPM:4X',
                'BPM05X'     :'VELABPM:5X',
                'BPM01Y'     :'VELABPM:1Y',
                'BPM02Y'     :'VELABPM:2Y',
                'BPM03Y'     :'VELABPM:3Y',
                'BPM04Y'     :'VELABPM:4Y',
                'BPM05Y'     :'VELABPM:5Y',
                'slot2ch0'  :'c2:wfmsl2ch0',
                'slot2ch1'  :'c2:wfmsl2ch1',
                'slot3ch0'  :'c2:wfmsl3ch0',
                'slot3ch1'  :'c2:wfmsl3ch1',
                'cha:wf'    :'cha:wf',
                'chb:wf'    :'chb:wf'
                })

     
        self.BPMcollection() # !!!! changed 20150211 !!!!
        self.BPMVOLcollection()  
        
        self.ps = []
        self.ks = []
        for key,pv in self.datakeys.iteritems():
            self.ps.append(pv)
            self.ks.append(key)
           

    def Reset(self) :
        self.QuitEvent.Signal()
        #ca.caput('c2:Posi',0.0)
        if hasattr(self,'s'):
            self.s.close()

    
    def Callback(self,value) :
        print 'Acq:Acq:callback> ',self.npulse

        self.npulse = self.npulse + 1
        self.nacquired = self.nacquired + 1
        ca.caput('c2:cpulse',self.npulse)
        
        self.BPMcollection()
        self.BPMVOLcollection()
        self.AcquireBPMpositions()
        self.AcquireBPMVOLpositions()
        self.AcquirePulseInfos()
       
        d = (ca.caget(self.ps))
        
        sample = dict(zip(self.ks,d))
       
        self.d.WriteData(sample)
        
        perc = (self.npulse/self.npulsemax)*100
        ca.caput('c2:progress',perc)

        
        if self.npulse==self.npulsemax:
            self.Reset()


    def clCallback(self,value) :
    
        print 'Acq:Acq:cal callback> ',self.clnpulse

        self.clnpulse = self.clnpulse + 1
        self.clnacquired = self.clnacquired + 1
        ca.caput('scan:cpulse',self.clnpulse)
        
        self.BPMcollection()    #  !!!! 20150211 changed !!!!
        self.BPMVOLcollection()    
        self.AcquireBPMpositions()
        self.AcquireBPMVOLpositions()
        self.AcquirePulseInfos()
        
               
        d = (ca.caget(self.ps))
        
        sample = dict(zip(self.ks,d))
       
        self.d.WriteData(sample)
        
        perc = (self.clnpulse/self.clnpulsemax)*100
        ca.caput('scan:progress',perc)

        
        if self.clnpulse==self.clnpulsemax:
            self.Reset()

    def cl2DCallback(self,value) :
    
        print 'Acq:Acq:cal 2D callback> ',self.cl2Dnpulse

        self.cl2Dnpulse = self.cl2Dnpulse + 1
        self.cl2Dnacquired = self.cl2Dnacquired + 1
        ca.caput('scan:2Dcpulse',self.cl2Dnpulse)
        
        self.BPMcollection()
        self.BPMVOLcollection()
        self.AcquireBPMpositions()
        self.AcquireBPMVOLpositions()
        self.Acquire2DPulseInfos()
        
               
        d = (ca.caget(self.ps))
        
        sample = dict(zip(self.ks,d))
       
        self.d.WriteData(sample)
        
        #  perc = (self.clnpulse/self.clnpulsemax)*100
        # ca.caput('scan:2Dprogress',perc)

        
        if self.cl2Dnpulse==self.cl2Dnpulsemax:
            self.Reset()

    def cl2DAcquireN(self,cl2Dnpulsemax) :
    
        
        print 'calibration 2D Scan Acquiring ',cl2Dnpulsemax,' pulses'
      
        self.cl2Dnpulse = 0
        self.cl2Dnpulsemax = cl2Dnpulsemax
        self.QuitEvent.Reset()
        
        self.AcquireCBPMpositions()
        
        self.s = ca.camonitor(self.triggervar,self.cl2DCallback)
        
        self.QuitEvent.Wait()
           
        print self.cl2Dnpulse,' Pulses Acquired'

   

    def clAcquireN(self,clnpulsemax) :
        """Acquire a number of pulses
        
        Usage AcquireN(integer)
        
        Data samples consisting of 'datakeys' in init method
        are saved upon a trigger from self.triggervar, also 
        defined in init.
        
        File remains open
        
        """
        
        print 'calibration Scan Acquiring ',clnpulsemax,' pulses'
        ca.caput('scan:progress',0)
        self.clnpulse = 0
        self.clnpulsemax = clnpulsemax
        self.QuitEvent.Reset()
        self.AcquireCBPMpositions()
        
        self.s = ca.camonitor(self.triggervar,self.clCallback)
        
        self.QuitEvent.Wait()
           
        print self.clnpulse,' Pulses Acquired'
        
    
    def AcquireN(self,npulsemax) :
        """Acquire a number of pulses
        
        Usage AcquireN(integer)
        
        Data samples consisting of 'datakeys' in init method
        are saved upon a trigger from self.triggervar, also 
        defined in init.
        
        File remains open
        
        """
        
        print 'Acquiring ',npulsemax,' pulses'
        ca.caput('c2:progress',0)
        self.npulse = 0
        self.npulsemax = npulsemax
        self.QuitEvent.Reset()
        self.AcquireCBPMpositions()
        
        self.s = ca.camonitor(self.triggervar,self.Callback)
        
        self.QuitEvent.Wait()
           
        print self.npulse,' Pulses Acquired'

  
    def AcquireTemp(self):
        sample={}
        for key, pv in self.datakeys.iteritems():
            sample[key] = ca.caget(pv)
        self.d.WriteData(sample)

    def AcquireVELAsetups(self):
        print 'lwAcq>Acquiring VELA Magnet & Vacuum Values'

      
        a = ca.caget('EBT-INJ-VAC-PIRG-01:P') #get a generic float type
        a.ok=False #set .ok to false huzzah
        while a.ok != True:
            a = ca.caget('EBT-INJ-MAG-BSOL-01:RI',timeout=1,throw=False)
            time.sleep(0.5)
            print 'Magnet Acquisition: ',a.ok

        print 'Proceeding...'
        
        sample={}
        a = ca.caget('EBT-INJ-VAC-PIRG-01:P') #get a generic float type
        a.ok=False #set .ok to false huzzah
        res = [a]
        
        while res[0].ok == False:
            res = ca.caget(self.magkeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.magkeys.keys(),res))
        sample.update(res_dict)
        res = [a]
        
        while res[0].ok == False:
            res = ca.caget(self.vackeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.vackeys.keys(),res))
        sample.update(res_dict)
        res = [a]
      
        self.d.WriteData(sample)


    def AcquireCBPMsetups(self):
        print 'Acq>Acquiring CBPM Initial Values'
    
    
        a = ca.caget('EBT-INJ-DIA-BPMC-02:X') #get a generic float type
        a.ok=False #set .ok to false huzzah
        while a.ok != True:
            a = ca.caget('c2:Posi',timeout=1,throw=False)
            time.sleep(0.5)
            print 'CBPM initial setup Acquisition: ',a.ok
    
        print 'Proceeding...'

        sample={}
        a = ca.caget('EBT-INJ-DIA-BPMC-02:X') #get a generic float type
        a.ok=False #set .ok to false huzzah
        res = [a]
    
        while res[0].ok == False:
            res = ca.caget(self.cbpmSetupkeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.cbpmSetupkeys.keys(),res))
        sample.update(res_dict)
        res = [a]

        self.d.WriteData(sample)
            
    def AcquireScansetups(self):
        print 'Acq>Acquiring Scan Initial Values'
                
                
        #a = ca.caget('VELABPM:1X') #get a generic float type !!!! 20150211 cahnged !!!!
        a = ca.caget('c2:Posi') #get a generic float type 
        a.ok=False #set .ok to false huzzah
        while a.ok != True:
            a = ca.caget('scan:Scansetup',timeout=1,throw=False)
            time.sleep(0.5)
            print 'Scan initial setup Acquisition: ',a.ok
                
        print 'Proceeding...'
                
        sample={}
        #a = ca.caget('VELABPM:1X') #get a generic float type !!!! 20150211 changed !!!!
        a = ca.caget('c2:Posi') #get a generic float type 
        a.ok=False #set .ok to false huzzah
        res = [a]
                
        while res[0].ok == False:
            res = ca.caget(self.cbpmScankeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.cbpmScankeys.keys(),res))
        sample.update(res_dict)
        res = [a]
        self.d.WriteData(sample)

    def Acquire2DScansetups(self):
        print 'Acq>Acquiring Scan Initial Values'
                
                
        a = ca.caget('VELABPM:1X') #get a generic float type
        a.ok=False #set .ok to false huzzah
        while a.ok != True:
            a = ca.caget('scan:Scansetup',timeout=1,throw=False)
            time.sleep(0.5)
            print 'Scan initial setup Acquisition: ',a.ok
                
        print 'Proceeding...'
                
        sample={}
        a = ca.caget('VELABPM:1X') #get a generic float type
        a.ok=False #set .ok to false huzzah
        res = [a]
                
        while res[0].ok == False:
            res = ca.caget(self.cbpm2DScankeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.cbpm2DScankeys.keys(),res))
        sample.update(res_dict)
        res = [a]
        self.d.WriteData(sample)

    def AcquirePulseInfos(self):
       
        sample={}
        res = []
      
        res = ca.caget(self.pulseInfokeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.pulseInfokeys.keys(),res))
        sample.update(res_dict)
    
        self.d.WriteData(sample)

    def Acquire2DPulseInfos(self):
       
        sample={}
        res = []
      
        res = ca.caget(self.pulse2DscanInfokeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.pulse2DscanInfokeys.keys(),res))
        sample.update(res_dict)
    
        self.d.WriteData(sample)

           
    def AcquireBPMpositions(self):
       
        sample={}
        res = []
      
        res = ca.caget(self.bpmPositionkeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.bpmPositionkeys.keys(),res))
        sample.update(res_dict)
    
        self.d.WriteData(sample)

    def AcquireBPMVOLpositions(self):
       
        sample={}
        res = []
      
        res = ca.caget(self.bpmVOLPositionkeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.bpmVOLPositionkeys.keys(),res))
        sample.update(res_dict)
    
        self.d.WriteData(sample)
        

    def AcquireCBPMpositions(self):

        #a = ca.caget('c2:wfmsl2ch0') #get a generic float type
        a = ca.caget('c2:Posi') #get a generic float type
        a.ok=False #set .ok to false huzzah
        while a.ok != True:
            a = ca.caget('VELABPM:1X',timeout=1,throw=False)
            time.sleep(0.5)
       
        sample={}
        #a = ca.caget('c2:wfmsl2ch0') #get a generic float type
        a = ca.caget('c2:Posi') #get a generic float type
        a.ok=False #set .ok to false huzzah
        res = [a]
                
        while res[0].ok == False:
            res = ca.caget(self.cbpmPositionkeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.cbpmPositionkeys.keys(),res))
        sample.update(res_dict)
        res = [a]
        self.d.WriteData(sample)
        
      
    def SingleAcquireVars(self):
        print 'lwAcq>Acquiring Once Only Vars'


    def CloseAcq(self) :
        """CloseAcq() - closes the data file - no further writing
        possible to the data file associated with this instance of lwAcq.
        
        """
        
        self.d.CloseFile()
        print '\nData written to ',self.d.filepath

    def ContinueInNewFile(self):
        self.d.CloseFile()
        self.d.NewFile(self.suffix,self.infolist)
        self.filename = self.d.filename
        self.filepath = self.d.filepath
        print 'New Data File '+self.filepath

    ####
    # Collecting information about BPM in VELA beam line : Vacuum & Positions
    ####
    def BPMcollection(self) :

        # Vaccume
        self.BPM01X = ca.caget('EBT-INJ-DIA-BPMC-02:X',count=1)
        self.BPM02X = ca.caget('EBT-INJ-DIA-BPMC-04:X',count=1)
        self.BPM03X = ca.caget('EBT-INJ-DIA-BPMC-06:X',count=1)
        self.BPM04X = ca.caget('EBT-INJ-DIA-BPMC-10:X',count=1)
        self.BPM05X = ca.caget('EBT-INJ-DIA-BPMC-12:X',count=1)
        self.BPM01Y = ca.caget('EBT-INJ-DIA-BPMC-02:Y',count=1)
        self.BPM02Y = ca.caget('EBT-INJ-DIA-BPMC-04:Y',count=1)
        self.BPM03Y = ca.caget('EBT-INJ-DIA-BPMC-06:Y',count=1)
        self.BPM04Y = ca.caget('EBT-INJ-DIA-BPMC-10:Y',count=1)
        self.BPM05Y = ca.caget('EBT-INJ-DIA-BPMC-12:Y',count=1)

       
        # Positions
        ca.caput('VELABPM:1X',self.BPM01X)
        ca.caput('VELABPM:2X',self.BPM02X)
        ca.caput('VELABPM:3X',self.BPM03X)
        ca.caput('VELABPM:4X',self.BPM04X)
        ca.caput('VELABPM:5X',self.BPM05X)
        ca.caput('VELABPM:1Y',self.BPM01Y)
        ca.caput('VELABPM:2Y',self.BPM02Y)
        ca.caput('VELABPM:3Y',self.BPM03Y)
        ca.caput('VELABPM:4Y',self.BPM04Y)
        ca.caput('VELABPM:5Y',self.BPM05Y)


    def BPMVOLcollection(self) :

        # Vaccume
        self.BPM01VOL = ca.caget('EBT-INJ-DIA-BPMC-02:DATA:B2V.VALA',count=9)
        self.BPM02VOL = ca.caget('EBT-INJ-DIA-BPMC-04:DATA:B2V.VALA',count=9)
        self.BPM03VOL = ca.caget('EBT-INJ-DIA-BPMC-06:DATA:B2V.VALA',count=9)
        self.BPM04VOL = ca.caget('EBT-INJ-DIA-BPMC-10:DATA:B2V.VALA',count=9)
        self.BPM05VOL = ca.caget('EBT-INJ-DIA-BPMC-12:DATA:B2V.VALA',count=9)

       
        # infomation
        ca.caput('VELABPMVOL:1',self.BPM01VOL)
        ca.caput('VELABPMVOL:2',self.BPM02VOL)
        ca.caput('VELABPMVOL:3',self.BPM03VOL)
        ca.caput('VELABPMVOL:4',self.BPM04VOL)
        ca.caput('VELABPMVOL:5',self.BPM05VOL)
            

class LoggerDataPulse :
    """Logger Class
        
        Usage:
        Logger()
        Logger.Log(integernumberofpulses)
        
        Data saved to ../dat/raw/YYMMDD_HHMM_log.dat.
        
        """
    
    def __init__(self,filezip = False) :
        

        self.filezip = filezip
        
    
    def Log(self,option,npulse) :
        
        print 'Pulse > ',npulse
       
        a = Acq(self.filezip,option,[option,npulse])
               
        his = History()
        his.RecordLog(npulse,a.filename)

        a.AcquireCBPMsetups()
        a.AcquireVELAsetups()
        
    
        
        try:
            a.AcquireN(npulse)
            a.CloseAcq()
        except KeyboardInterrupt:
            a.Reset()
            print 'Pulse' + option + '> Logging Quit'

class Positions :
    def __init__(self) :
        pass

    def Save(self) :

        print 'Saving positions\n'
        a = Acq('pos',['positions'])
        his = History()
        his.RecordPos(a.filename)
        
        a.AcquireN(1)
        a.CloseAcq()


class CalCBPM :
    """
    Scan

    Scan(axis,npulse,start,stop,nstep)
    axis can be one of
    'phasev'
    'chaver'
    'chahor'
    'manver'
    'manang'
    'laser'
    
    """

    
    def __init__(self,filezip = False) :
        ca.caput('scan:progress',0)
        ca.caput('scan:cstep',0)
        self.filezip = filezip

    ####
    # Scanning the stages with MoveRelative mode in MINT controller
    ####
    def MoveRelativeScan(self, option, axis, clnpulse, posrange, nstep) :

        readHoriPosi = ca.caget('c2:OutHoriPosi')
        readVertPosi = ca.caget('c2:OutVerPosi')
        
        print 'Mover:Relative Scan> ', clnpulse,posrange,nstep

        comment = ca.caget('scan:comment')
        
        a = Acq(self.filezip, option,[option,comment])

        
        a.AcquireScansetups()
        #a.AcquireVELAsetups()
        
        start = -posrange/2
        stop  =  posrange/2

        # Log scan into master history
        t = time.strftime('%Y%m%d_%H%M')
        starttime = time.time()
        
               
        cm = ControlMovers.ControlMovers(axis)   
              
        #step = posrange/nstep
        #print 'Step > ', step, '\n'


        # Move to initial position 
        
        print "Moving by",-posrange/2
	moved = cm.MoveRelative(-posrange/2)
        ca.caput('scan:rlposi',-posrange/2)

        if not moved :
            print 'Moved:',moved
            print 'No move detected! Aborting...'
            raise KeyboardInterrupt

        # Acquire data
        
        a.clAcquireN(clnpulse)

        step = posrange/nstep
        print 'Step > ', step, '\n'
       
        # current step number : cstep   
        cstep = 0

        # Move to next positions
        try:
            for i in range(0,int(nstep)):

                ca.caput('scan:rlposi',-posrange/2+step*(i+1))
                               
                cstep = cstep + 1
                ca.caput('scan:cstep',cstep)
                print 'MoverScan:Scan > Step',cstep,'\n'
                
               
                print "Moving by",step
                moved = cm.MoveRelative(step)

                
                if not moved :
                    print 'Moved:',moved
                    print 'No move detected! Aborting...'
                    raise KeyboardInterrupt

                
             
                print 'Acquirering!!'
                a.clAcquireN(clnpulse)
                      
       
        except KeyboardInterrupt:
            a.Reset()
            cm.Reset()
            print '\n\n Move Absolute Scan:Scan> Abort! Abort! Abort!\n\n'

        print '\nMoverScan:Scan> Returning to start position\n\n'
        print "Moving by",-posrange/2

        # Return to the initial position
        
        moved = cm.MoveRelative(-posrange/2)             
        if not moved :
            print 'Moved:',moved
            print 'No move detected! Aborting...'
            raise KeyboardInterrupt
        
        print "*** Moving finisshed ***"

        a.CloseAcq()
        cm.Reset()
        #cm.MoveRelative(-posrange/2)             
        stoptime = time.time()
        totaltime = stoptime-starttime
        his      = History()
        his.RecordScan(axis,clnpulse,start,stop,nstep,a.filename,totaltime,posrange)
        
        #self.filepath = lwa.filepath
        #self.filename = lwa.filename
        


    ####
    # Relative movement in 2D scan : (Not in use yet !!! 27/01/2015)
    ####
    
    def MoveRelative2DScan(self, option, axis, cl2Dnpulse, posrange_1stMove, nstep_1stMove, posrange_2ndMove, nstep_2ndMove) :

        readHoriPosi = ca.caget('c2:OutHoriPosi')
        readVertPosi = ca.caget('c2:OutVerPosi')

        if axis == 'Hori':
            axis_1stMove = 'Hori'
            axis_2ndMove = 'Vert'
        elif axis == 'Vert':
            axis_1stMove = 'Vert'
            axis_2ndMove = 'Hori'
        else :
            print 'setting of axis is wrong !!!'
            raise KeyboardInterrupt
        
        print 'Mover:Relative 2D Scan START> ', axis_1stMove, posrange_1stMove ,nstep_1stMove

        comment = ca.caget('scan:comment')
        a = Acq(self.filezip,option,[option,comment])

        
        a.Acquire2DScansetups()
        a.AcquireVELAsetups()
        
        start = -posrange_1stMove/2
        stop  =  posrange_1stMove/2

        # Log scan into master history
        t = time.strftime('%Y%m%d_%H%M')
        starttime = time.time()
        
        #####################
        # 1st stage is moving
        #
        #####################

     
        cm1 = ControlMovers.ControlMovers(axis_1stMove)
        
        step_1stMove = posrange_1stMove/nstep_1stMove
        step_2ndMove = posrange_2ndMove/nstep_2ndMove
        
        cstep = 0
        
        try:
            for i in range(0,int(nstep_1stMove)+1):
                cstep = i
                if cstep == 0:
                    step = -posrange_1stMove/2
                    # relative position for 1st move
                    ca.caput('scan:2Drlposi_1st',-posrange_1stMove/2)
                else :
                    step = step_1stMove
                    # relative position for 1st move
                    ca.caput('scan:2Drlposi_1st',-posrange_1stMove/2+step*(i+1))
                
                ca.caput('scan:2Dcstep1st',cstep)
                ca.caput('c2:Axis',axis_1stMove)
                print 'MoverScan:1D Scan > Step',cstep
                print "Moving by",step

                moved1 = cm1.MoveRelative(step)
                if not moved1 :
                    print 'Moved:',moved1
                    print 'No move detected! Aborting...'
                    raise KeyboardInterrupt

                
               
                #########################
                #
                # second stage is moving
                #
                #
                #########################
                ca.caput('c2:Axis',axis_2ndMove)
                print '---'
                print '2nd stage....axis > ', axis_2ndMove   
         
                cm2 = ControlMovers.ControlMovers(axis_2ndMove)   
               
                cstep_2ndMove = 0

                
                for i in range(0,int(nstep_2ndMove)+1):
                    cstep_2ndMove = i
                    if cstep_2ndMove == 0:
                        step = -posrange_2ndMove/2
                        # relative position information for 2nd move
                        ca.caput('scan:2Drlposi_2nd',-posrange_2ndMove/2)
                    else :
                        step = step_2ndMove 
                        # relative position information for 2nd move
                        ca.caput('scan:2Drlposi_2nd',-posrange_2ndMove/2+step*(i+1))
                        
                    ca.caput('scan:2Dcstep2nd',cstep_2ndMove)
                    print 'MoverScan:2D Scan > Step',cstep_2ndMove
                    print "Moving by",step

                    moved2 = cm2.MoveRelative(step)
                    if not moved2 :
                        print 'Moved:',moved2
                        print 'No move detected! Aborting...'
                        raise KeyboardInterrupt
                
                    print 'Acquirering!!'
                    a.cl2DAcquireN(cl2Dnpulse)
                       
                         
                 
                print '\nMoverScan:Scan> 2nd stage : Returning to start position'
                print "Moving by",-posrange_2ndMove/2, '\n'
                
                cm2.MoveRelative(-posrange_2ndMove/2)

               
        except KeyboardInterrupt:
            a.Reset()
            cm2.Reset()
            cm1.Reset()
            ca.caput('c2:Switch','Off')
            ca.caput('c2:Posi',0.0)
            print '\n\n Move Absolute Scan:Scan> Abort! Abort! Abort!\n\n'
            raise

        stoptime = time.time()
        totaltime = stoptime-starttime
        his      = History()
        his.RecordScan(axis,cl2Dnpulse,start,stop, nstep_1stMove, a.filename,totaltime)
        
        #self.filepath = lwa.filepath
        #self.filename = lwa.filename
        
        print '\nMoverScan:Scan> 1st stage : Returning to start position\n\n'
        print "Moving by",-posrange_1stMove/2
        ca.caput('c2:Axis',axis_1stMove)
        cm1.MoveRelative(-posrange_1stMove/2)

        a.CloseAcq()

class History:
    def __init__(self):
        pass

    def RecordMove(self,axis,currentpos,targetpos):
        #self.f      = open('../dat/his/lwMoveHistory.dat','a',0)
        self.f      = open('/home/epics/ipsbpm/bpm/run/dat/his/MoveHistory.dat','a',0)
        #self.f      = open('/home/epics/ioc/run/test/MoveHistory.dat','a',0)
        t           = time.strftime('%Y%m%d_%H%M%S')
        linetowrite = 'Move> '+t+' axis='+str(axis)+' cpos='+str(currentpos)+' tpos='+str(targetpos)+'\n'
        self.f.write(linetowrite)
        self.f.close()

    def RecordScan(self,axis,npulse,start,stop,nstep,filename,totaltime,range):
        self.f      = open('/home/epics/ipsbpm/bpm/run/dat/his/ScanHistory.dat','a',0)
     #   self.f      = open('/home/epics/ioc/run/test/History.dat','a',0)
        linetowrite = '   * Scan> '+str(filename)+' axis='+str(axis)+', npulse='+str(npulse)+', start='+str(start)+', stop='+str(stop)+', nstep='+str(nstep)+', totaltime='+str(totaltime)+', range='+str(range)+'\n'
        self.f.write(linetowrite)
        self.f.close()

    def RecordLog(self,npulse,filename):
        self.f      = open('/home/epics/ipsbpm/bpm/run/dat/his/RecordHistory.dat','a',0)
        #self.f      = open('/home/epics/ioc/run/test/History.dat','a',0)
        linetowrite = '   * Log> '+str(filename)+' npulse='+str(npulse)+'\n'
        self.f.write(linetowrite)
        self.f.close()

    def RecordPos(self,filename):
        self.f      = open('/home/epics/ipsbpm/bpm/run/dat/his/RecordHistory.dat','a',0)
        #self.f      = open('/home/epics/ioc/run/test/History.dat','a',0)
        linetowrite = '   * Pos> Positions saved '+str(filename)+'\n'
        self.f.write(linetowrite)
        self.f.close()
    

def main() :

    usage = ''
    parser = optparse.OptionParser(usage)
    parser.add_option('-a','--logsl2sl3',action='store_true',default=False)   #log n pulses for slot2 and slot3
    parser.add_option('-b','--pos',action='store_true',default=False)   #log 1 pulse for positions
    parser.add_option('-c','--scansl2',action='store_true',default=False)   #scan n pulses for slot2
    parser.add_option('-d','--scansl3',action='store_true',default=False)   #scan n pulse for slot3
    parser.add_option('-e','--logsl2',action='store_true',default=False)   #log n pulses for slot2
    parser.add_option('-f','--logsl3',action='store_true',default=False)   #log n pulse for slot3
    parser.add_option('-g','--scan2Dsl2',action='store_true',default=False)   #2Dscan n pulse for slot2
    parser.add_option('-i','--scan2Dsl3',action='store_true',default=False)   #2Dscan n pulse for slot3
    parser.add_option('-j','--logRP',action='store_true',default=False)  #RP log n pulse for Red Pitaya
    parser.add_option('-k','--scanRP',action='store_true',default=False)   #RP scan n pulse for Red Pitaya

    options,args = parser.parse_args()
    
    if options.logsl2sl3 :
        
        l = LoggerDataPulse()
        npulse = ca.caget('c2:npulse')
        l.Log('logsl2sl3',npulse)
        ca.caput('c2:Posi',0.0)
        

    if options.pos :
        p = Positions()
        p.Save()
        ca.caput('c2:Posi',0.0)

    if options.scansl2 :
        #Pull values from PVs / EDM
        clnpulse    = ca.caget('scan:npulse')
        posrange    = ca.caget('scan:range')
        nstep       = ca.caget('scan:nstep')
        axis        = ca.caget('c2:Axis')
        l = CalCBPM()
        l.MoveRelativeScan('scansl2',axis, clnpulse, posrange, nstep)
      
        ca.caput('c2:Posi',0.0)

    if options.scansl3 :
        #Pull values from PVs / EDM
        clnpulse    = ca.caget('scan:npulse')
        posrange    = ca.caget('scan:range')
        nstep       = ca.caget('scan:nstep')
        axis        = ca.caget('c2:Axis')
        l = CalCBPM()
        l.MoveRelativeScan('scansl3',axis, clnpulse, posrange, nstep)
      
        ca.caput('c2:Posi',0.0)

    if options.scanRP :
        #Pull values from PVs / EDM
        clnpulse    = ca.caget('scan:npulse')
        posrange    = ca.caget('scan:range')
        nstep       = ca.caget('scan:nstep')
        axis        = ca.caget('c2:Axis')
        l = CalCBPM()
        l.MoveRelativeScan('scanRP',axis, clnpulse, posrange, nstep)
      
        ca.caput('c2:Posi',0.0)

    if options.scan2Dsl2 :
        #Pull values from PVs / EDM
        cl2Dnpulse    = ca.caget('scan:2Dnpulse')
        posrange1    = ca.caget('scan:2Drange1stMove')
        nstep1       = ca.caget('scan:2Dnstep1stMove')
        posrange2    = ca.caget('scan:2Drange2ndMove')
        nstep2       = ca.caget('scan:2Dnstep2ndMove')
        axis        = ca.caget('c2:Axis')
        l = CalCBPM()
        l.MoveRelative2DScan('scan2Dsl2',axis, cl2Dnpulse, posrange1, nstep1, posrange2, nstep2)
      
        ca.caput('c2:Posi',0.0)
        
    if options.scan2Dsl3 :
        #Pull values from PVs / EDM
        cl2Dnpulse   = ca.caget('scan:2Dnpulse')
        posrange1    = ca.caget('scan:2Drange1stMove')
        nstep1       = ca.caget('scan:2Dnstep1stMove')
        posrange2    = ca.caget('scan:2Drange2ndMove')
        nstep2       = ca.caget('scan:2Dnstep2ndMove')
        axis         = ca.caget('c2:Axis')
        l = CalCBPM()
        l.MoveRelative2DScan('scan2Dsl3',axis, cl2Dnpulse, posrange1, nstep1, posrange2, nstep2)
      
        ca.caput('c2:Posi',0.0)

   
    if options.logsl2 :
       
        l = LoggerDataPulse()
        npulse = ca.caget('c2:npulse')
        l.Log('logsl2',npulse)
       
        ca.caput('c2:Posi',0.0)
        
    if options.logsl3 :
        l = LoggerDataPulse()
        npulse = ca.caget('c2:npulse')
        l.Log('logsl3',npulse)
       
        ca.caput('c2:Posi',0.0)

    if options.logRP :
        l = LoggerDataPulse()
        npulse = ca.caget('c2:npulse')
        l.Log('logRP',npulse)
       
        ca.caput('c2:Posi',0.0)


if __name__ == "__main__":
    main()
