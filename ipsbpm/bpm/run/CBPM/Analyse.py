import csv
import math
import numpy as np
import pylab as pl
import matplotlib.pyplot as plt
import os
import shutil
import time
import gzip
import cothread
import cothread.catools as ca
from tempfile import mkstemp as _mkstemp
from shutil import move as _move
from os import remove as _remove
from os import close as _close
import gzip


from ROOT import TCanvas, TGraph, TGraphErrors, TLegend,TPad,TF1,gApplication,gStyle 

import Data
saves = {}

class BPMAnalysis :

    def __init__(self,bpmname) :
      
        self.d = Data.DataFormat(bpmname)
        self.scan2d = Data.DataFormat2DScan()
        
        self.ch0 = []
        self.ch1 = []
        self.PXIch0 = ca.caget('plot:PXIch0')
     
    # 1D scan all for plot data
    def CalAnalysis_all(self,fd,op) :
        
        
        self.filepath = fd
        file = '/home/epics/ipsbpm/bpm/run/dat/raw/'+fd
        if fd.split('_')[-1][-2:] == 'gz':
            print 'reading zip file...'
            
            f = gzip.open(file,'rb')
            self.filename = fd[-28:]
        else :
            f = open(file,'r')
            self.filename = fd
        
        print 'Ca.Analysis. readingfile...' , self.filepath, self.filename
        self.d.Readlogs(f)
        
        
        d1         = []
        d2         = []
        time       = []
        tpx        = []
        tpy        = []
        samplerate = 5e8
        dt         = 1/samplerate
        
        
        npulse = int(self.d.scannpulse[0])
        nstep = int(self.d.scannstep[0])
        
        print 'Analysis.Cl.Plot pulse>' , npulse,nstep
        
        if op == 'scanlgsl2' :
            for i in self.d.wfmsl2ch0:
                d1.append(i)
            for i in self.d.wfmsl2ch1:
                d2.append(i)
        elif op == 'scanlgsl3' :
            for i in self.d.wfmsl3ch0:
                d1.append(i)
            for i in self.d.wfmsl3ch1:
                d2.append(i)
        else :
            print 'option is not found'
        
        self.ch0 = pl.array(d1)
        self.ch1 = pl.array(d2)
        
        
        ## Append CBPM positions Hori
        for i in self.d.posix:
            tpx.append(i)
        ## Append CBPM positions Vert
        for i in self.d.posiy:
            tpy.append(i)
        
        self.px = pl.array(tpx)
        self.py = pl.array(tpy)
        
        ## Append time information of wave form
        for k in range(0,nstep+1):
            for j in range(0,npulse):
                for i in range(0,3000):
                    time.append((i+1)*dt)
        self.time = pl.array(time)
        
        XMAX = []
        YMAX = []
        Vmaxch0 = []
        Vmaxch1 = []
        
        for j in range(0,nstep+1):
            print 'step>', j+1
            for i in range(j*(npulse),(j+1)*(npulse)) :
                V0 =self.ch0[i*3000:(i+1)*3000]
                V1 =self.ch1[i*3000:(i+1)*3000]
                # peak value of each pulse
                Vp0 = max(V0)
                Vp1 = max(V1)
                Vmaxch0.append(float(Vp0))
                Vmaxch1.append(float(Vp1))
                XMAX.append(self.px[j])
                YMAX.append(self.py[j])
        self.Vmaxch0 = pl.array(Vmaxch0)
        self.Vmaxch1 = pl.array(Vmaxch1)
        self.XMAX = pl.array(XMAX)
        self.YMAX = pl.array(YMAX)
        
        #print 'calAna>', len(self.XMAX), self.XMAX
               
        f.close()

    # 1D scan all for plot data RP
    def CalAnalysisRP_all(self,fd) :
        
        
        self.filepath = fd
        file = '/home/epics/ipsbpm/bpm/run/dat/raw/'+fd
        if fd.split('_')[-1][-2:] == 'gz':
            print 'reading zip file...'
            
            f = gzip.open(file,'rb')
            self.filename = fd[-28:]
        else :
            f = open(file,'r')
            self.filename = fd
        
        print 'Ca.Analysis. readingfile...' , self.filepath, self.filename
        self.d.Readlogs(f)
        
        
        d1         = []
        d2         = []
        time       = []
        tpx        = []
        tpy        = []
        samplerate = 5e8
        dt         = 1/samplerate
        
        
        npulse = int(self.d.scannpulse[0])
        nstep = int(self.d.scannstep[0])
        
        print 'Analysis.Cl.RP Plot pulse>' , npulse,nstep
        
        for i in self.d.chawf:
            if i < 0:
                i = -i
            d1.append(i)
        for i in self.d.chbwf:
            if i < 0:
                i = -i
            d2.append(i)
        
        self.ch0 = pl.array(d1)
        self.ch1 = pl.array(d2)
                
        ## Append CBPM positions Hori
        for i in self.d.posix:
            tpx.append(i)
        ## Append CBPM positions Vert
        for i in self.d.posiy:
            tpy.append(i)
        
        self.px = pl.array(tpx)
        self.py = pl.array(tpy)
        
        ## Append time information of wave form
        for k in range(0,nstep+1):
            for j in range(0,npulse):
                for i in range(0,1024):
                    time.append((i+1)*dt)
        self.time = pl.array(time)
        
        XMAX = []
        YMAX = []
        Vmaxch0 = []
        Vmaxch1 = []
        
        for j in range(0,nstep+1):
            print 'step>', j+1
            for i in range(j*(npulse),(j+1)*(npulse)) :
                V0 =self.ch0[i*1024:(i+1)*1024]
                V1 =self.ch1[i*1024:(i+1)*1024]
                # peak value of each pulse
                Vp0 = max(V0)
                Vp1 = max(V1)
                Vmaxch0.append(float(Vp0))
                Vmaxch1.append(float(Vp1))
                XMAX.append(self.px[j])
                YMAX.append(self.py[j])
        self.Vmaxch0 = pl.array(Vmaxch0)
        self.Vmaxch1 = pl.array(Vmaxch1)
        self.XMAX = pl.array(XMAX)
        self.YMAX = pl.array(YMAX)
        
        print 'calAnaRP>', len(self.XMAX), self.ch0
               
        f.close()


    # 1D scan all for plot data RP
    def CalAnalysisRP_ddcall(self,fd) :
        
        
        self.filepath = fd
        file = '/home/epics/ipsbpm/bpm/run/dat/raw/'+fd
        if fd.split('_')[-1][-2:] == 'gz':
            print 'reading zip file...'
            
            f = gzip.open(file,'rb')
            self.filename = fd[-28:]
        else :
            f = open(file,'r')
            self.filename = fd
        
        print 'Ca.Analysis. readingfile...' , self.filepath, self.filename
        self.d.Readlogs(f)
        
        
        d1         = []
        d2         = []
        d3         = []
        d4         = []
   
        tpx        = []
        tpy        = []

        chxmax     = []
        chymax     = []
              
        npulse = int(self.d.scannpulse[0])
        nstep = int(self.d.scannstep[0])
        
        print 'Analysis.Cl.RP Plot pulse>' , npulse,nstep
        
        for i in self.d.xddcfilteredReal:
            d1.append(i)
        for i in self.d.xddcfilteredImag:
            d2.append(i)
        for i in self.d.yddcfilteredReal:
            d3.append(i)
        for i in self.d.yddcfilteredImag:
            d4.append(i)
        
        self.chxreal = pl.array(d1)
        self.chximag = pl.array(d2)
        self.chyreal = pl.array(d3)
        self.chyimag = pl.array(d4)
                
        ## Append CBPM positions Hori
        for i in self.d.posix:
            tpx.append(i)
        ## Append CBPM positions Vert
        for i in self.d.posiy:
            tpy.append(i)
        
        self.px = pl.array(tpx)
        self.py = pl.array(tpy)
        
        
              
        for j in range(0,nstep+1):
            print 'step>', j+1
            for i in range(j*(npulse),(j+1)*(npulse)) :
                chxreal =self.chxreal[i*1024:(i+1)*1024]
                chximag =self.chximag[i*1024:(i+1)*1024]
                chyreal =self.chyreal[i*1024:(i+1)*1024]
                chyimag =self.chyimag[i*1024:(i+1)*1024]
                
                
                chx = pl.sqrt(chxreal*chxreal+chximag*chximag)
                chy = pl.sqrt(chyreal*chyreal+chyimag*chyimag)
                
                max_chx = max(chx)
                max_chy = max(chy)
                
                
                chxmax.append(max_chx)
                chymax.append(max_chy)
        self.chxmax = pl.array(chxmax)
        self.chymax = pl.array(chymax)

        print self.px
        print self.chxmax
               
        f.close()


    # 1D scan trial
    # Absolute value of stage positions are taken into account
    #
    
    def CalAnalysis_abs(self,fd,op) :

        
        self.filepath = fd
        file = '/home/epics/ipsbpm/bpm/run/dat/raw/'+fd
        if fd.split('_')[-1][-2:] == 'gz':
            print 'reading zip file...'
            
            #f = gzip.open(self.filepath,'rb')
            f = gzip.open(file,'rb')
            self.filename = fd[-28:]
        else :
            #f = open(self.filepath,'r')
            f = open(file,'r')
            self.filename = fd
       
        print 'Ca.Analysis. readingfile...' , self.filepath, self.filename
        self.d.Readlogs(f)
      
       
        d1         = []
        d2         = []
        time       = []
        tpx        = []
        tpy        = []
        samplerate = 5e8
        dt         = 1/samplerate
        Vmeanch0   = []
        Vmeanch1   = []
        Verrch0    = []
        Verrch1    = []
        Vpmaxch0   = []
        Vpmaxch1   = []
       
        npulse = int(self.d.scannpulse[0])
        nstep = int(self.d.scannstep[0])
        
        print 'Analysis.Cl.Plot pulse>' , npulse,nstep
        
        if op == 'scanlgsl2' :
            for i in self.d.wfmsl2ch0:
                d1.append(i)
            for i in self.d.wfmsl2ch1:
                d2.append(i)
        elif op == 'scanlgsl3' :
            for i in self.d.wfmsl3ch0:
                d1.append(i)
            for i in self.d.wfmsl3ch1:
                d2.append(i)
        else :
            print 'option is not found'
                
        self.ch0 = pl.array(d1)     
        self.ch1 = pl.array(d2)
      
        
        ## Append CBPM positions Hori
        for i in self.d.posix:
            tpx.append(i)
        ## Append CBPM positions Vert
        for i in self.d.posiy:
            tpy.append(i)

        self.px = pl.array(tpx)
        self.py = pl.array(tpy)
            
        ## Append time information of wave form 
        for k in range(0,int(nstep+1)):
            for j in range(0,npulse):
                for i in range(0,3000):
                    time.append((i+1)*dt)
        self.time = pl.array(time)
        
        for j in range(0,int(nstep+1)):
            
            print 'step>', j+1
            Vmaxch0 = []
            Vmaxch1 = []
          
            for i in range(j*(npulse),(j+1)*(npulse)) :                       
                V0 =self.ch0[i*3000:(i+1)*3000]
                V1 =self.ch1[i*3000:(i+1)*3000]
                # peak value of each pulse
                Vp0 = max(V0)
                Vp1 = max(V1)

                Vmaxch0.append(float(Vp0))
                Vmaxch1.append(float(Vp1))
            print 'peak value of ch0:ch1',Vmaxch0,Vmaxch1
            
            # Max value of peak values
            mvch0  = max(Vmaxch0)
            mvch1  = max(Vmaxch1)
           

            # Mean value of peak value & error
            vmch0 = np.mean(Vmaxch0)
            vmch1 = np.mean(Vmaxch1)
            verch0  = mvch0 - vmch0
            verch1  = mvch1 - vmch1
                      
            # append Mean value & error
            Vpmaxch0.append(float(mvch0))
            Vpmaxch1.append(float(mvch1))
            Vmeanch0.append(float(vmch0))
            Vmeanch1.append(float(vmch1))
            Verrch0.append(float(verch0))
            Verrch1.append(float(verch1))

        self.Vpmaxch0 = np.array(Vpmaxch0)
        self.Vpmaxch1 = np.array(Vpmaxch1)
        self.Vmeanch0 = np.array(Vmeanch0)
        self.Vmeanch1 = np.array(Vmeanch1)
        self.Verrch0 = np.array(Verrch0)
        self.Verrch1 = np.array(Verrch1)
       

        print 'max of each max value>',self.Vpmaxch0,self.Vpmaxch1
        print 'vmean', self.Vmeanch0,self.Vmeanch1
        print 'err',self.Verrch0,self.Verrch1

        f.close()

    # 1D scan trial
    # Absolute value of stage positions are taken into account RP
    #
    
    def CalAnalysisRP_abs(self,fd,op) :

        
        self.filepath = fd
        file = '/home/epics/ipsbpm/bpm/run/dat/raw/'+fd
        if fd.split('_')[-1][-2:] == 'gz':
            print 'reading zip file...'
            
            #f = gzip.open(self.filepath,'rb')
            f = gzip.open(file,'rb')
            self.filename = fd[-28:]
        else :
            #f = open(self.filepath,'r')
            f = open(file,'r')
            self.filename = fd
       
        print 'Ca.Analysis. RP readingfile...' , self.filepath, self.filename
        self.d.Readlogs(f)
      
       
        d1         = []
        d2         = []
        time       = []
        tpx        = []
        tpy        = []
        samplerate = 5e8
        dt         = 1/samplerate
        Vmeanch0   = []
        Vmeanch1   = []
        Verrch0    = []
        Verrch1    = []
        Vpmaxch0   = []
        Vpmaxch1   = []
       
        npulse = int(self.d.scannpulse[0])
        nstep = int(self.d.scannstep[0])
        
        print 'Analysis.Cl.RP Plot pulse>' , npulse,nstep
        
        for i in self.d.chawf:
            d1.append(i)
        for i in self.d.chbwf:
            d2.append(i)
                
        self.ch0 = pl.array(d1)     
        self.ch1 = pl.array(d2)
      
        
        ## Append CBPM positions Hori
        for i in self.d.posix:
            tpx.append(i)
        ## Append CBPM positions Vert
        for i in self.d.posiy:
            tpy.append(i)

        self.px = pl.array(tpx)
        self.py = pl.array(tpy)
            
        ## Append time information of wave form 
        for k in range(0,int(nstep+1)):
            for j in range(0,npulse):
                for i in range(0,1024):
                    time.append((i+1)*dt)
        self.time = pl.array(time)
        
        for j in range(0,int(nstep+1)):
            
            print 'step>', j+1
            Vmaxch0 = []
            Vmaxch1 = []
          
            for i in range(j*(npulse),(j+1)*(npulse)) :                       
                V0 =self.ch0[i*1024:(i+1)*1024]
                V1 =self.ch1[i*1024:(i+1)*1024]
                # peak value of each pulse
                Vp0 = max(V0)
                Vp1 = max(V1)

                Vmaxch0.append(float(Vp0))
                Vmaxch1.append(float(Vp1))
            print 'peak value of ch0:ch1',Vmaxch0,Vmaxch1
            
            # Max value of peak values
            mvch0  = max(Vmaxch0)
            mvch1  = max(Vmaxch1)
           

            # Mean value of peak value & error
            vmch0 = np.mean(Vmaxch0)
            vmch1 = np.mean(Vmaxch1)
            verch0  = mvch0 - vmch0
            verch1  = mvch1 - vmch1
                      
            # append Mean value & error
            Vpmaxch0.append(float(mvch0))
            Vpmaxch1.append(float(mvch1))
            Vmeanch0.append(float(vmch0))
            Vmeanch1.append(float(vmch1))
            Verrch0.append(float(verch0))
            Verrch1.append(float(verch1))

        self.Vpmaxch0 = np.array(Vpmaxch0)
        self.Vpmaxch1 = np.array(Vpmaxch1)
        self.Vmeanch0 = np.array(Vmeanch0)
        self.Vmeanch1 = np.array(Vmeanch1)
        self.Verrch0 = np.array(Verrch0)
        self.Verrch1 = np.array(Verrch1)
       

        print 'max of each max value>',self.Vpmaxch0,self.Vpmaxch1
        print 'vmean', self.Vmeanch0,self.Vmeanch1
        print 'err',self.Verrch0,self.Verrch1

        f.close()

    # 1D scan
    # Relative positions of stages (N.B. Not real psotion values.)
    def CalAnalysis(self,fd,op) :

        
        self.filepath = fd
        file = '/home/epics/ipsbpm/bpm/run/dat/raw/'+fd
        if fd.split('_')[-1][-2:] == 'gz':
            print 'rreading zip file...'
            
            #f = gzip.open(self.filepath,'rb')
            f = gzip.open(file,'rb')
            self.filename = fd[-28:]
        else :
            #f = open(self.filepath,'r')
            f = open(file,'r')
            self.filename = fd
       
        print 'Ca.Analysis. readingfile...' , self.filepath, self.filename
        self.d.Readlogs(f)
      
       
        d1         = []
        d2         = []
        time       = []
        tp         = []
        samplerate = 5e8
        dt         = 1/samplerate
        Vmeanch0   = []
        Vmeanch1   = []
        Verrch0    = []
        Verrch1    = []
        Vpmaxch0   = []
        Vpmaxch1   = []
        RelPosi    = []
        rp         = []
        
        npulse = int(self.d.scannpulse[0])
        nstep = int(self.d.scannstep[0])
        
        print 'Analysis.Cl.Plot pulse>' , npulse,nstep
        
        if op == 'scanlgsl2' :
            for i in self.d.wfmsl2ch0:
                d1.append(i)
            for i in self.d.wfmsl2ch1:
                d2.append(i)
        elif op == 'scanlgsl3' :
            for i in self.d.wfmsl3ch0:
                d1.append(i)
            for i in self.d.wfmsl3ch1:
                d2.append(i)
        else :
            print 'option is not found'
                
        self.ch0 = pl.array(d1)     
        self.ch1 = pl.array(d2)
      
        
        ## Append relative CBPM positions
        for i in self.d.scanrlposi:
            tp.append(i)
        ## Append time information of wave form 
        for k in range(0,nstep+1):
            for j in range(0,npulse):
                for i in range(0,3000):
                    time.append((i+1)*dt)
        self.time = pl.array(time)
        
        for j in range(0,int(nstep+1)):
            
            print 'step>', j+1
            Vmaxch0 = []
            Vmaxch1 = []
            rp.append(tp[npulse*j])
                       
            for i in range(j*(npulse),(j+1)*(npulse)) :                       
                V0 =self.ch0[i*3000:(i+1)*3000]
                V1 =self.ch1[i*3000:(i+1)*3000]

                # peak value of each pulse
                Vp0 = max(V0)
                Vp1 = max(V1)

                Vmaxch0.append(float(Vp0))
                Vmaxch1.append(float(Vp1))
            print 'peak value of ch0:ch1',Vmaxch0,Vmaxch1
            
            # Max value of peak values
            mvch0 = max(Vmaxch0)
            mvch1 = max(Vmaxch1)

            # Mean value of peak value & error
            vmch0 = np.mean(Vmaxch0)
            vmch1 = np.mean(Vmaxch1)
            verch0  = mvch0 - vmch0
            verch1  = mvch1 - vmch1
            # append Mean value & error
            Vpmaxch0.append(float(mvch0))
            Vpmaxch1.append(float(mvch1))
            Vmeanch0.append(float(vmch0))
            Vmeanch1.append(float(vmch1))
            Verrch0.append(float(verch0))
            Verrch1.append(float(verch1))

        self.Vpmaxch0 = np.array(Vpmaxch0)
        self.Vpmaxch1 = np.array(Vpmaxch1)
        self.Vmeanch0 = np.array(Vmeanch0)
        self.Vmeanch1 = np.array(Vmeanch1)
        self.Verrch0 = np.array(Verrch0)
        self.Verrch1 = np.array(Verrch1)
        self.RelPosi = np.array(rp)
      

        print 'max of each max value>',self.Vpmaxch0,self.Vpmaxch1
        print 'vmean', self.Vmeanch0,self.Vmeanch1
        print 'err',self.Verrch0,Verrch1
        print 'relative posi',self.RelPosi
       
        f.close()

    # 2D scan
    # Not in use yet !!! 27/01/2015
    def CalAnalysis2D(self,fd,op) :

        
        self.filepath = fd
        file = '/home/epics/ipsbpm/bpm/run/dat/raw/'+fd
        if fd.split('_')[-1][-2:] == 'gz':
            print 'reading zip file...'
            
            #f = gzip.open(self.filepath,'rb')
            f = gzip.open(file,'rb')
            self.filename = fd[-28:]
        else :
            #f = open(self.filepath,'r')
            f = open(file,'r')
            self.filename = fd
       
        print '2D Scan Ca.Analysis. readingfile...' , self.filepath, self.filename
        self.scan2d.Readlogs(f)
      
       
        d1         = []
        d2         = []
        time       = []
        tp1st      = []
        tp2nd      = []
        samplerate = 5e8
        dt         = 1/samplerate
        
        npulse   = int(self.scan2d.scan2Dnpulse[0])
        nstep1st = int(self.scan2d.scan2Dnstep1st[0])
        nstep2nd = int(self.scan2d.scan2Dnstep2nd[0])
        
        print 'Analysis.Cl.2D Plot pulse>' , npulse
        
        if op == 'scanlgsl2' :
            for i in self.scan2d.wfmsl2ch0:
                d1.append(i)
            for i in self.scan2d.wfmsl2ch1:
                d2.append(i)
        elif op == 'scanlgsl3' :
            for i in self.scan2d.wfmsl3ch0:
                d1.append(i)
            for i in self.scan2d.wfmsl3ch1:
                d2.append(i)
        else :
            print 'option is not found'
                
        self.ch0 = pl.array(d1)     
        self.ch1 = pl.array(d2)
      
        
        ## Append relative CBPM 1st positions
        for i in self.scan2d.scan2Drlposi1st:
            tp1st.append(i)
        ## Append relative CBPM 1st positions
        for i in self.scan2d.scan2Drlposi2nd:
            tp2nd.append(i)
        
        ## Append time information of wave form 
        for k in range(0,int(nstep1st+1)):
            for j in range(0,int(npulse)):
                for i in range(0,3000):
                    time.append((i+1)*dt)
        self.time = pl.array(time)

        RelPosi1st = []
        RelPosi2nd = []
        rp1st      = []
        rp2nd      = []
        Vmeanch0   = []
        Vmeanch1   = []
        Verrch0    = []
        Verrch1    = []
        Vpmaxch0   = []
        Vpmaxch1   = []
        #1st stage 
        for k in range(0,int(nstep1st+1)):
            
            rp1st.append(tp1st[k*npulse*(nstep2nd+1)])
            for j in range((nstep2nd+1)*k,(nstep2nd+1)*(k+1)):
            
                print 'step>', j+1
                Vmaxch0 = []
                Vmaxch1 = []
                
                rp2nd.append(tp2nd[npulse*j])
                       
                for i in range(j*(npulse),(j+1)*(npulse)) :                       
                    V0 =self.ch0[i*3000:(i+1)*3000]
                    V1 =self.ch1[i*3000:(i+1)*3000]

                    # peak value of each pulse
                    Vp0 = max(V0)
                    Vp1 = max(V1)

                    Vmaxch0.append(float(Vp0))
                    Vmaxch1.append(float(Vp1))
                print 'peak value of ch0:ch1',Vmaxch0,Vmaxch1
            
                # Max value of peak values
                mvch0 = max(Vmaxch0)
                mvch1 = max(Vmaxch1)

                # Mean value of peak value & error
                vmch0 = np.mean(Vmaxch0)
                vmch1 = np.mean(Vmaxch1)
                verch0  = mvch0 - vmch0
                verch1  = mvch1 - vmch1
                # append Mean value & error
                Vpmaxch0.append(float(mvch0))
                Vpmaxch1.append(float(mvch1))
                Vmeanch0.append(float(vmch0))
                Vmeanch1.append(float(vmch1))
                Verrch0.append(float(verch0))
                Verrch1.append(float(verch1))

        self.Vpmaxch0 = np.array(Vpmaxch0)
        self.Vpmaxch1 = np.array(Vpmaxch1)
        self.Vmeanch0 = np.array(Vmeanch0)
        self.Vmeanch1 = np.array(Vmeanch1)
        self.Verrch0 = np.array(Verrch0)
        self.Verrch1 = np.array(Verrch1)
        self.RelPosi1st = np.array(rp1st)
        self.RelPosi2nd = np.array(rp2nd)
      

        print 'max of each max value>',self.Vpmaxch0,self.Vpmaxch1
        print 'vmean', self.Vmeanch0,self.Vmeanch1
        print 'err',self.Verrch0,Verrch1
        print 'relative posi',self.RelPosi1st,self.RelPosi2nd
       
        f.close()


    # Logged data
    def CalAnalysisForLog(self,fd,op) :

        
        self.filepath = fd
        file = '/home/epics/ipsbpm/bpm/run/dat/raw/'+fd
        if fd.split('_')[-1][-2:] == 'gz':
            print 'rreading zip file...'
            
            #f = gzip.open(self.filepath,'rb')
            f = gzip.open(file,'rb')
            self.filename = fd[-28:]
        else :
            #f = open(self.filepath,'r')
            f = open(file,'r')
            self.filename = fd
       
        print 'Ca.Analysis For Log. readingfile...' , self.filepath, self.filename
        self.d.Readlogs(f)
      
       
        d1         = []
        d2         = []
        time       = []
        tp         = []
        samplerate = 5e8
        dt         = 1/samplerate
        Vmeanch0   = []
        Vmeanch1   = []
        Verrch0    = []
        Verrch1    = []
        Vpmaxch0   = []
        Vpmaxch1   = []
        RelPosi    = []
        rp         = []
        
        npulse = int(self.d.npulse[0])
               
        print 'Analysis.Cl.Plot pulse>' , npulse
        
        if op == 'lgsl2' :
            for i in self.d.wfmsl2ch0:
                d1.append(i)
            for i in self.d.wfmsl2ch1:
                d2.append(i)
        elif op == 'lgsl3' :
            for i in self.d.wfmsl3ch0:
                d1.append(i)
            for i in self.d.wfmsl3ch1:
                d2.append(i)
        else :
            print 'option is not found'
                
        self.ch0 = pl.array(d1)     
        self.ch1 = pl.array(d2)
      
        
        ## Append CBPM Set positions
        for i in self.d.SetcbpmPosi:
            tp.append(i)
        ## Append time information of wave form 
        for j in range(0,npulse):
            for i in range(0,3000):
                time.append((i+1)*dt)
        self.time = pl.array(time)
        
       
        Vmaxch0 = []
        Vmaxch1 = []
                              
        for i in range(0,(npulse)) :                       
            V0 =self.ch0[i*3000:(i+1)*3000]
            V1 =self.ch1[i*3000:(i+1)*3000]

            # peak value of each pulse
            Vp0 = max(V0)
            Vp1 = max(V1)

            Vmaxch0.append(float(Vp0))
            Vmaxch1.append(float(Vp1))
        print 'peak value of ch0:ch1',Vmaxch0,Vmaxch1
            
        # Max value of peak values
        mvch0 = max(Vmaxch0)
        mvch1 = max(Vmaxch1)

        # Mean value of peak value & error
        vmch0 = np.mean(Vmaxch0)
        vmch1 = np.mean(Vmaxch1)
        verch0  = mvch0 - vmch0
        verch1  = mvch1 - vmch1
        # append Mean value & error
        Vpmaxch0.append(float(mvch0))
        Vpmaxch1.append(float(mvch1))
        Vmeanch0.append(float(vmch0))
        Vmeanch1.append(float(vmch1))
        Verrch0.append(float(verch0))
        Verrch1.append(float(verch1))

        self.Vpmaxch0 = np.array(Vpmaxch0)
        self.Vpmaxch1 = np.array(Vpmaxch1)
        self.Vmeanch0 = np.array(Vmeanch0)
        self.Vmeanch1 = np.array(Vmeanch1)
        self.Verrch0 = np.array(Verrch0)
        self.Verrch1 = np.array(Verrch1)
        
      

        print 'max of each max value>',self.Vpmaxch0,self.Vpmaxch1
        print 'vmean', self.Vmeanch0,self.Vmeanch1
        print 'err',self.Verrch0,Verrch1
        
       
        f.close()

    
    # Analysis for integral of power
    # Relative (ideal) stage position (N.B. it is not real stage position)
    def IntegralAnalysis(self,fd,op) :
              
        self.filepath = fd
        
        if fd.split('_')[-1][-2:] == 'gz':
            print 'rreading zip file...'
            f = gzip.open(self.filepath,'rb')
            self.filename = fd[-28:]
        else :
            f = open(self.filepath,'r')
            self.filename = fd
       
        print 'Log.Ca.Analysis. readingfile...' , self.filepath, self.filename
        self.d.Readlogs(f)
      
       
        d1         = []
        d2         = []
        tp         = []
        samplerate = 5e8
        dt         = 1/samplerate
        RelPosi    = []
        rp         = []
        IntVch0    = []
        IntVch1    = []
        IntErch0   = []
        IntErch1   = []
       
        
        npulse = int(self.d.scannpulse[0])
        nstep = int(self.d.scannstep[0])
        
        print 'Analysis.Cl.Plot pulse>' , npulse,nstep
        
        if op == 'scanlgsl2' :
            for i in self.d.wfmsl2ch0:
                d1.append(i)
            for i in self.d.wfmsl2ch1:
                d2.append(i)
        elif op == 'scanlgsl3' :
            for i in self.d.wfmsl3ch0:
                d1.append(i)
            for i in self.d.wfmsl3ch1:
                d2.append(i)
        else :
            print 'option is not found'
                
        self.ch0 = pl.array(d1)     
        self.ch1 = pl.array(d2)
        sumch0     = 0
        sumch1     = 0
        
        ## Append relative CBPM positions
        for i in self.d.scanrlposi:
            tp.append(i)
            
        for k in range(0,nstep+1):
            print 'step>', k+1
            Intch0    = []
            Intch1    = []
            rp.append(tp[npulse*k])
            for j in range(npulse*k,npulse*(k+1)):

                for i in range(j*3000,(j+1)*3000) :
                    
                    V0 =self.ch0[i]
                    V1 =self.ch1[i]
                   
                
                    sumch0 += V0*dt
                    sumch1 += V1*dt
                    
                    
                Intch0.append(sumch0)
                Intch1.append(sumch1)
                sumch0     = 0
                sumch1     = 0
                
            
            meanch0 = np.mean(Intch0)
            meanch1 = np.mean(Intch1)
            maxch0  = np.max(Intch0)
            maxch1  = np.max(Intch1)
            erch0   = maxch0 - meanch0
            erch1   = maxch1 - meanch1
           
            
            IntVch0.append(float(meanch0))
            IntVch1.append(float(meanch1))
            IntErch0.append(float(erch0))
            IntErch1.append(float(erch1))
        self.IntVch0 = pl.array(IntVch0)
        self.IntVch1 = pl.array(IntVch1)
        self.IntErch0 = pl.array(IntErch0)
        self.IntErch1 = pl.array(IntErch1)
        self.RelPosi = np.array(rp)
        
        print 'IntV ch0', self.IntVch0,self.IntVch1
        print 'err',self.IntErch0,self.IntErch1
        print 'relative posi',self.RelPosi
       
        f.close()

############ Plot functions ##########

    # Waveform plots
    def WaveFormPlot(self):
        global saves
        ymin = ca.caget("plot:wfymin")
        ymax = ca.caget("plot:wfymax")
        axis = self.d.axis[0]

        npulse = int(self.d.scannpulse[0])
        nstep = int(self.d.scannstep[0])
        num = 3000*npulse

        print 'Plotting>',npulse

        cv = TCanvas("cv","",10,40,800,1100)
        cv.SetGrid()
        cv.SetFillColor( 0 )
        saves['cv']=cv

        pad1 = TPad( 'pad1', 'ch0', 0.05, 0.52, 0.98, 0.98, 0 )
        pad1.Draw()
        pad2 = TPad( 'pad2', 'ch1', 0.05, 0.02, 0.98, 0.48, 0 )
        pad2.Draw()
        saves[ 'pad1' ] = pad1
        saves[ 'pad2' ] = pad2

        ## waveform ch0
        pad1.cd()
       
        pad1.SetBorderMode( 0 )
        pad1.SetBorderSize( 1 )
        pad1.SetFrameBorderMode( 0 )
        pad1.SetGrid()
        pad1.SetFillColor( 0 )

        gStyle.SetTitleOffset(100,"Y")
        gStyle.SetTitleFillColor(0)
        gStyle.SetTitleBorderSize(0)
        lch0 = TLegend(0.78,0.3,0.98,0.95)
        lch0.SetFillColor(0)

    
        for i in range(0,int(nstep+1)) :
            self.controlWF(npulse,i,i+1)              
            gri = TGraph(num,self.x,self.ych0)
        
            gri.SetTitle('waveformch0:' + self.d.axis[0]+ '_' + self.filename)
            gri.GetXaxis().SetTitle( 'Relative position [mm]' )
            gri.GetYaxis().SetTitle( 'Amplitude [V]' )
            gri.GetXaxis().SetTitleSize(0.05)
            gri.GetYaxis().SetTitleSize(0.05)
            gri.GetXaxis().SetTitleOffset(0.9)
            gri.GetYaxis().SetTitleOffset(0.9)
            
            gri.SetMarkerColor(2+i)
            gri.SetMarkerStyle(3+i)
            
            gri.SetMaximum(ymax)
            gri.SetMinimum(ymin)
            if i == 0:
                gri.Draw("AP")
            else:
                gri.Draw("same P")
            saves['grch0'+str(i)]=gri
       
           
            lch0.AddEntry(gri,'ch0:'+str(i) + 'step',"p")
            lch0.Draw()
            
        saves['lch0'] = lch0

        ## waveform ch1
        pad2.cd()
       
        pad2.SetBorderMode( 0 )
        pad2.SetBorderSize( 1 )
        pad2.SetFrameBorderMode( 0 )
        pad2.SetGrid()
        pad2.SetFillColor( 0 )

        gStyle.SetTitleOffset(100,"Y")
        gStyle.SetTitleFillColor(0)
        gStyle.SetTitleBorderSize(0)
        lch1 = TLegend(0.78,0.3,0.98,0.95)
        lch1.SetFillColor(0)

    
        for i in range(0,int(nstep+1)) :
            self.controlWF(npulse,i,i+1)              
            gri = TGraph(num,self.x,self.ych1)
        
            gri.SetTitle('waveformch1:' + self.d.axis[0]+ '_' + self.filename)
            gri.GetXaxis().SetTitle( 'Relative position [mm]' )
            gri.GetYaxis().SetTitle( 'Amplitude [V]' )
            gri.GetXaxis().SetTitleSize(0.05)
            gri.GetYaxis().SetTitleSize(0.05)
            gri.GetXaxis().SetTitleOffset(0.9)
            gri.GetYaxis().SetTitleOffset(0.9)
            
            gri.SetMarkerColor(2+i)
            gri.SetMarkerStyle(3+i)
            
            gri.SetMaximum(ymax)
            gri.SetMinimum(ymin)
            if i == 0:
                gri.Draw("AP")
            else:
                gri.Draw("same P")
            saves['grch1'+str(i)]=gri
       
           
            lch1.AddEntry(gri,'ch1:'+str(i) + 'step',"p")
            lch1.Draw()
            
        saves['lch1'] = lch1

    def WaveFormPlotForLog(self):
        global saves
        ymin = ca.caget("plot:wfymin")
        ymax = ca.caget("plot:wfymax")
        axis = self.d.axis[0]

        npulse = int(self.d.npulse[0])
        num = 3000*npulse

        print 'Plotting>',npulse

        cv = TCanvas("cv","",10,40,800,1100)
        cv.SetGrid()
        cv.SetFillColor( 0 )
        saves['cv']=cv

        pad1 = TPad( 'pad1', 'ch0', 0.05, 0.52, 0.98, 0.98, 0 )
        pad1.Draw()
        pad2 = TPad( 'pad2', 'ch1', 0.05, 0.02, 0.98, 0.48, 0 )
        pad2.Draw()
        saves[ 'pad1' ] = pad1
        saves[ 'pad2' ] = pad2

        ## waveform ch0
        pad1.cd()
       
        pad1.SetBorderMode( 0 )
        pad1.SetBorderSize( 1 )
        pad1.SetFrameBorderMode( 0 )
        pad1.SetGrid()
        pad1.SetFillColor( 0 )

        gStyle.SetTitleOffset(100,"Y")
        gStyle.SetTitleFillColor(0)
        gStyle.SetTitleBorderSize(0)
              
        self.controlWF(npulse,0,1)              
        grch0 = TGraph(num,self.x,self.ych0)
        
        grch0.SetTitle('waveformch0:' + self.d.axis[0]+ '_' + self.filename)
        grch0.GetXaxis().SetTitle( 'Relative position [mm]' )
        grch0.GetYaxis().SetTitle( 'Amplitude [V]' )
        grch0.GetXaxis().SetTitleSize(0.05)
        grch0.GetYaxis().SetTitleSize(0.05)
        grch0.GetXaxis().SetTitleOffset(0.9)
        grch0.GetYaxis().SetTitleOffset(0.9)
            
        grch0.SetMarkerColor(2)
        grch0.SetMarkerStyle(3)
            
        grch0.SetMaximum(ymax)
        grch0.SetMinimum(ymin)
        
        grch0.Draw("AP")
        saves['grch0']=grch0
       
        lch0 = TLegend(0.78,0.85,0.98,0.95)
        lch0.SetFillColor(0)
        lch0.AddEntry(grch0,'ch0',"p")
        lch0.Draw()
            
        saves['lch0'] = lch0

        ## waveform ch1
        pad2.cd()
       
        pad2.SetBorderMode( 0 )
        pad2.SetBorderSize( 1 )
        pad2.SetFrameBorderMode( 0 )
        pad2.SetGrid()
        pad2.SetFillColor( 0 )

        gStyle.SetTitleOffset(100,"Y")
        gStyle.SetTitleFillColor(0)
        gStyle.SetTitleBorderSize(0)
             
        self.controlWF(npulse,0,1)              
        grch1 = TGraph(num,self.x,self.ych1)
        
        grch1.SetTitle('waveformch1:' + self.d.axis[0]+ '_' + self.filename)
        grch1.GetXaxis().SetTitle( 'Relative position [mm]' )
        grch1.GetYaxis().SetTitle( 'Amplitude [V]' )
        grch1.GetXaxis().SetTitleSize(0.05)
        grch1.GetYaxis().SetTitleSize(0.05)
        grch1.GetXaxis().SetTitleOffset(0.9)
        grch1.GetYaxis().SetTitleOffset(0.9)
            
        grch1.SetMarkerColor(2)
        grch1.SetMarkerStyle(3)
        
        grch1.SetMaximum(ymax)
        grch1.SetMinimum(ymin)
        grch1.Draw("AP")
    
        saves['grch1']=grch1
       
        lch1 = TLegend(0.78,0.85,0.98,0.95)
        lch1.SetFillColor(0)
        lch1.AddEntry(grch1,'ch1:',"p")
        lch1.Draw()
            
        saves['lch1'] = lch1

    def controlWF(self,npulse,ini,fin):

        self.x = self.time[ini*3000*npulse:(ini+1)*3000*npulse]
        self.ych0 = self.ch0[ini*3000*npulse:(ini+1)*3000*npulse]
        self.ych1 = self.ch1[ini*3000*npulse:(ini+1)*3000*npulse]
            
    # Power and voltage at the Port of CBPM
    def AtPortPlot(self): 
        global saves
       
        pymin = ca.caget("plot:pymin")
        pymax = ca.caget("plot:pymax")
        vymin = ca.caget("plot:vymin")
        vymax = ca.caget("plot:vymax")
        axis = self.d.axis[0]

        self.PositionError(axis)

        cv = TCanvas("cv","",10,40,1000,600)
        cv.SetGrid()
        cv.SetFillColor( 0 )
        saves['cv']=cv

        pad1 = TPad( 'pad1', '', 0.05, 0.02, 0.48, 0.98, 0 )
        pad2 = TPad( 'pad2', '', 0.55, 0.02, 0.98, 0.98, 0 )
        pad1.Draw()
        pad2.Draw()
        saves[ 'pad1' ] = pad1
        saves[ 'pad2' ] = pad2

        pad1.cd()
        
       
        pad1.SetFillColor( 0 )
        pad1.SetBorderMode( 0 )
        pad1.SetBorderSize( 1 )
        pad1.SetFrameBorderMode( 0 )
        pad1.SetGrid()
        
       

        gStyle.SetTitleOffset(100,"Y")
        gStyle.SetTitleFillColor(0)
        gStyle.SetTitleBorderSize(0)

        p0ch0 = 20.82
        p1ch0 = -0.02141
        p0ch1 = 20.64
        p1ch1 = 0.004654
        Z = 50.

        Pch0 = []
        Pch1 = []
        Vpch0 = []
        Vpch1 = []
        Verrch0 = []
        Verrch1 = []
        Vmaxch0 = []
        Vmaxch1 = []
        Pmaxch0 = []
        Pmaxch1 = []
        Perrch0 = []
        Perrch1 = []
        
        for i in self.Vmeanch0:
            powerch0 = (i-p1ch0)/p0ch0*math.pow(10,1.64/20)*1e-3
            vpeakch0 = pl.sqrt(2*powerch0*Z)
            print 'powc vol  ch0>', i, powerch0, vpeakch0
            Pch0.append(powerch0)
            Vpch0.append(vpeakch0)
        self.Pch0 = pl.array(Pch0)
        self.Vpch0 = pl.array(Vpch0)
        for j in self.Vmeanch1:
            powerch1 = (j-p1ch1)/p0ch1*math.pow(10,1.64/20)*1e-3
            vpeakch1 = pl.sqrt(2*powerch1*Z)
            print 'pow vol ch1>', j , powerch1,vpeakch1
            Pch1.append(powerch1)
            Vpch1.append(vpeakch1)
        self.Pch1 = pl.array(Pch1)
        self.Vpch1 = pl.array(Vpch1)
        for i in self.Vpmaxch0: ## Error ch0
            powerch0 = (i-p1ch0)/p0ch0*math.pow(10,1.64/20)*1e-3
            vpeakch0 = pl.sqrt(2*powerch0*Z)
            print 'error ch0>' , i, powerch0, vpeakch0
            Pmaxch0.append(powerch0)
            Vmaxch0.append(vpeakch0)
        for j in self.Vpmaxch1: ## Error ch1
            powerch1 = (j-p1ch1)/p0ch1*math.pow(10,1.64/20)*1e-3
            vpeakch1 = pl.sqrt(2*powerch1*Z)
            print 'error ch1>' , j, powerch1, vpeakch1
            Pmaxch1.append(powerch1)
            Vmaxch1.append(vpeakch1)
        for i in range(0,len(self.Vpmaxch0)):
            Perrch0.append(Pmaxch0[i] - Pch0[i])
            Perrch1.append(Pmaxch1[i] - Pch1[i])
            Verrch0.append(Vmaxch0[i] - Vpch0[i])
            Verrch1.append(Vmaxch1[i] - Vpch1[i])
            
        self.Perrch0 = pl.array(Perrch0)
        self.Perrch1 = pl.array(Perrch1)
        self.Verrch0 = pl.array(Verrch0)
        self.Verrch1 = pl.array(Verrch1)
        
        print 'Pow, PowErr, V, Verr ch0>',self.Pch0, self.Perrch0, self.Vpch0,self.Verrch0
        print 'Pow, PowErr, V, Verr ch1>',self.Pch1, self.Perrch1, self.Vpch1,self.Verrch1
        grch0 = TGraphErrors(len(self.RelPosi),self.RelPosi,self.Vpch0,self.PosiCh0err,self.Verrch0)
        grch1 = TGraphErrors(len(self.RelPosi),self.RelPosi,self.Vpch1,self.PosiCh1err,self.Verrch1)
        
        grch0.SetTitle(axis+ ':' + self.filename + ' V_{p}. at the output port of CBPM' )
        grch0.GetXaxis().SetTitle( 'Relative position [mm]' )
        grch0.GetYaxis().SetTitle( 'V_{p} at the exit port [V]' )
        grch0.GetXaxis().SetTitleSize(0.05)
        grch0.GetYaxis().SetTitleSize(0.05)
        grch0.GetXaxis().SetTitleOffset(0.9)
        grch0.GetYaxis().SetTitleOffset(0.9)

        grch0.SetMarkerColor(2)
        grch0.SetMarkerStyle(4)
        grch0.SetMaximum(vymax)
        grch0.SetMinimum(vymin)

        grch1.SetMarkerColor(4)
        grch1.SetMarkerStyle(5)
        grch1.SetMaximum(vymax)
        grch1.SetMinimum(vymin)
        
        grch0.Draw("AP")
        grch1.Draw("same P")
        saves['grch0']=grch0
        saves['grch1']=grch1

        
        ## Plot Power at the exit of Port
        pad2.cd()
        pad2.SetFillColor( 0 )
        pad2.SetBorderMode( 0 )
        pad2.SetBorderSize( 1 )
        pad2.SetFrameBorderMode( 0 )
        pad2.SetGrid()

        grPowch0 = TGraphErrors(len(self.RelPosi),self.RelPosi,self.Pch0,self.PosiCh0err,self.Perrch0)
        grPowch1 = TGraphErrors(len(self.RelPosi),self.RelPosi,self.Pch1,self.PosiCh1err,self.Perrch1)
        
        grPowch0.SetTitle(axis+ ':' + self.filename + ' P at the output port of CBPM')
        grPowch0.GetXaxis().SetTitle( 'Relative position [mm]' )
        grPowch0.GetYaxis().SetTitle( 'P at the exit port [W]' )
        grPowch0.GetXaxis().SetTitleSize(0.05)
        grPowch0.GetYaxis().SetTitleSize(0.05)
        grPowch0.GetXaxis().SetTitleOffset(0.9)
        grPowch0.GetYaxis().SetTitleOffset(0.9)

        grPowch0.SetMarkerColor(2)
        grPowch0.SetMarkerStyle(4)
        grPowch0.SetMaximum(pymax)
        grPowch0.SetMinimum(pymin)

        grPowch1.SetMarkerColor(4)
        grPowch1.SetMarkerStyle(5)
        grPowch1.SetMaximum(pymax)
        grPowch1.SetMinimum(pymin)

        grPowch0.Draw("AP")
        grPowch1.Draw("same P")
        saves['grPowch0']=grPowch0
        saves['grPowch1']=grPowch1

        cv.Modified()
        cv.Update()


    def AtPortPlot_abs(self): ## Power and voltage at the Port of CBPM
        global saves

        pymin = ca.caget("plot:pymin")
        pymax = ca.caget("plot:pymax")
        vymin = ca.caget("plot:vymin")
        vymax = ca.caget("plot:vymax")
        xmin = ca.caget("plot:xmin")
        xmax = ca.caget("plot:xmax")
        fixpara = ca.caget("plot:fixpara")
        axis = self.d.axis[0]
    
        cv = TCanvas("cv","",10,40,1200,700)
        cv.SetGrid()
        cv.SetFillColor( 0 )
        saves['cv']=cv
    
        pad1 = TPad( 'pad1', '', 0.05, 0.02, 0.48, 0.98, 0 )
        pad2 = TPad( 'pad2', '', 0.55, 0.02, 0.98, 0.98, 0 )
        pad1.Draw()
        pad2.Draw()
        saves[ 'pad1' ] = pad1
        saves[ 'pad2' ] = pad2
    
        pad1.cd()
    
    
        pad1.SetFillColor( 0 )
        pad1.SetBorderMode( 0 )
        pad1.SetBorderSize( 1 )
        pad1.SetFrameBorderMode( 0 )
        pad1.SetGrid()
    
        gStyle.SetTitleOffset(100,"Y")
        gStyle.SetTitleFillColor(0)
        gStyle.SetTitleBorderSize(0)
        
        f = TF1("f", "[0]*fabs(x[0]-[1])+[2]")
        f.SetLineWidth(2)
        f.SetLineStyle(1)
        f.SetParName(0,"A")
        f.SetParName(1,"B")
        f.SetParName(2,"C")
        
        f2 = TF1("f2", "[0]*pow((x[0]-[1]),2)+[2]")
        f2.SetLineWidth(2)
        f2.SetLineStyle(1)
        f2.SetParName(0,"a")
        f2.SetParName(1,"b")
        f2.SetParName(2,"c")
    
        Z = 50.
    
        P = []
        Vp = []
        
         
        if axis == 'Vert':
            p0 = 20.82
            p1 = -0.02141
        
            for i in self.Vmaxch0:
                power = (i-p1)/p0*math.pow(10,1.64/20)*math.pow(10,2.06/20)*1e-3 # [W]
                vpeak = pl.sqrt(2*power*Z)
                
                P.append(power)
                Vp.append(vpeak)
    
            self.P = pl.array(P)
            self.Vp = pl.array(Vp)
            
            
            gr = TGraph(len(self.py),self.py,self.Vp) #<-self.py
            grPow = TGraph(len(self.py),self.py,self.P)
            gr.SetMarkerColor(6)
            gr.SetMarkerStyle(5)
            grPow.SetMarkerColor(6)
            grPow.SetMarkerStyle(5)
            f.SetLineColor(6)
            f2.SetLineColor(6)

        elif axis == 'Hori':
            p0 = 20.64
            p1 = 0.004654

            for j in self.Vmaxch1:
                power = (j-p1)/p0*math.pow(10,1.64/20)*math.pow(10,1.81/20)*1e-3 # [W]
                vpeak = pl.sqrt(2*power*Z)
                
                P.append(power)
                Vp.append(vpeak)

            self.P = pl.array(P)
            self.Vp = pl.array(Vp)
            
            gr = TGraph(len(self.px),self.px,self.Vp)#<-self.px
            grPow = TGraph(len(self.px),self.px,self.P)
            gr.SetMarkerColor(2)
            gr.SetMarkerStyle(4)
            grPow.SetMarkerColor(2)
            grPow.SetMarkerStyle(4)
            f.SetLineColor(2)
            f2.SetLineColor(2)
            
        print self.P, self.Vp, len(self.P),len(self.Vp)

        gr.SetTitle('V '+axis+ ':' + self.filename )
        gr.GetXaxis().SetTitle( 'CBPM position [mm]' )
        gr.GetYaxis().SetTitle( 'V_{p} at the exit port [V]' )
        gr.GetXaxis().SetTitleSize(0.05)
        gr.GetYaxis().SetTitleSize(0.05)
        gr.GetXaxis().SetTitleOffset(1)
        gr.GetYaxis().SetTitleOffset(1)
        
        gr.SetMaximum(vymax)
        gr.SetMinimum(vymin)
        
        gr.Draw("AP")
        gStyle.SetOptFit (111)
        gStyle.SetStatX ( 0.49 )
        gStyle.SetStatY ( 0.89 )
        gStyle.SetFillColor(0)
        gStyle.SetStatBorderSize (1)
        gStyle.SetFrameLineColor (2)
        f.FixParameter(1,fixpara)
        gr.Fit(f,"same P", "",xmin,xmax)
        
        saves['gr']=gr
        l = TLegend(0.6,0.75,0.92,0.88)
        l.SetFillColor(0)
        l.AddEntry(gr,'Data',"p")
        l.AddEntry(f,'y=A|x-B|+C',"l")
        l.Draw()
        saves['l'] = l
    
    
        ## Plot Power at the exit of Port
        pad2.cd()
        pad2.SetFillColor( 0 )
        pad2.SetBorderMode( 0 )
        pad2.SetBorderSize( 1 )
        pad2.SetFrameBorderMode( 0 )
        pad2.SetGrid()
    
    

        grPow.SetTitle('P '+axis+ ':' + self.filename)
        grPow.GetXaxis().SetTitle( 'CBPM position [mm]' )
        grPow.GetYaxis().SetTitle( 'P at the exit port [W]' )
        grPow.GetXaxis().SetTitleSize(0.05)
        grPow.GetYaxis().SetTitleSize(0.05)
        grPow.GetXaxis().SetTitleOffset(1)
        grPow.GetYaxis().SetTitleOffset(1)
    
        grPow.SetMaximum(pymax)
        grPow.SetMinimum(pymin)
    
        grPow.Draw("AP")
        

        gStyle.SetOptFit (111)
        gStyle.SetStatX ( 0.49 )
        gStyle.SetStatY ( 0.89 )
        gStyle.SetFillColor(0)
        gStyle.SetStatBorderSize (1)
        gStyle.SetFrameLineColor (2)
        f2.SetParameter(1,fixpara)
        grPow.Fit(f2,"R","",xmin,xmax)
        saves['grPow']=grPow
    
        lpow = TLegend(0.6,0.75,0.92,0.88)
        lpow.SetFillColor(0)
        lpow.AddEntry(grPow,'Data',"p")
        lpow.AddEntry(f2,'y=a(x-b)^{2}+c',"l")
        lpow.Draw()
        saves['lpow'] = lpow

    
        cv.Modified()
        cv.Update()

        cv.SaveAs(axis+'_'+self.filename+'_VolPowatPort.eps')


    ## Integral voltage at the PXI
    def IntVolPlot(self): 
        global saves
        xmin = ca.caget("plot:xmin")
        xmax = ca.caget("plot:xmax")
        ymin = ca.caget("plot:intymin")
        ymax = ca.caget("plot:intymax")
        axis = self.d.axis[0]

        self.PositionError(axis)

        cv = TCanvas("cv","",10,40,700,600)
        cv.SetGrid()
        cv.SetFillColor( 0 )
        saves['cv']=cv

        pad1 = TPad( 'pad1', '', 0.1, 0.02, 0.98, 0.98, 0 )
        pad1.Draw()
        saves[ 'pad1' ] = pad1
        
        pad1.cd()
        pad1.SetFillColor( 0 )
        pad1.SetBorderMode( 0 )
        pad1.SetBorderSize( 1 )
        pad1.SetFrameBorderMode( 0 )
        pad1.SetGrid()
        
        gStyle.SetTitleOffset(100,"Y")
        gStyle.SetTitleFillColor(0)
        gStyle.SetTitleBorderSize(0)

     
        grch0 = TGraphErrors(len(self.RelPosi),self.RelPosi,self.IntVch0,self.PosiCh0err,self.IntErch0)
        grch1 = TGraphErrors(len(self.RelPosi),self.RelPosi,self.IntVch1,self.PosiCh1err,self.IntErch1)
        
        grch0.SetTitle(axis+ ':' + self.filename + ' Pow. at the output port of CBPM')
        grch0.GetXaxis().SetTitle( 'Relative position [mm]' )
        grch0.GetYaxis().SetTitle( '#int V_{p} dt [V*s]' )
        grch0.GetXaxis().SetTitleSize(0.05)
        grch0.GetYaxis().SetTitleSize(0.03)
        grch0.GetXaxis().SetTitleOffset(0.9)
        grch0.GetYaxis().SetTitleOffset(0.9)

        grch0.SetMarkerColor(2)
        grch0.SetMarkerStyle(4)
        grch0.SetMaximum(ymax)
        grch0.SetMinimum(ymin)
        
        grch1.SetMarkerColor(4)
        grch1.SetMarkerStyle(5)
        grch1.SetMaximum(ymax)
        grch1.SetMinimum(ymin)

        grch0.Draw("AP")
        grch1.Draw("same P")
     
        saves['grch0']=grch0
        saves['grch1']=grch1
            
        l = TLegend(0.78,0.8,0.98,0.95)
        l.SetFillColor(0)
        l.AddEntry(grch0,'ch0',"p")
        l.AddEntry(grch1,'ch1',"p")
        l.Draw()
        saves['l'] = l

        cv.Modified()
        cv.Update()
        f.close()

    ## Power at the PXI digitisers
    def scanPlot(self): 
        global saves

        xmin = ca.caget("plot:xmin")
        xmax = ca.caget("plot:xmax")
        ymin = ca.caget("plot:scanymin")
        ymax = ca.caget("plot:scanymax")
        axis = self.d.axis[0]

        self.PositionError(axis)

        cv = TCanvas("cv","",10,40,1100,1000)
        cv.SetGrid()
        cv.SetFillColor( 0 )
        saves['cv']=cv

        pad1 = TPad( 'pad1', 'This is pad1', 0.05, 0.02, 0.98, 0.98, 0 )
        pad1.Draw()
        saves[ 'pad1' ] = pad1

        pad1.cd()
       
        pad1.SetFillColor( 42 )
        pad1.SetBorderMode( 0 )
        pad1.SetBorderSize( 1 )
        pad1.SetFrameBorderMode( 0 )
        pad1.SetGrid()
        pad1.SetFillColor( 0 )

        gStyle.SetTitleOffset(100,"Y")
        gStyle.SetTitleFillColor(0)
        gStyle.SetTitleBorderSize(0)

        
        
        grch0 = TGraphErrors(len(self.RelPosi),self.RelPosi,self.Vmeanch0,self.PosiCh0err,self.Verrch0)
        grch1 = TGraphErrors(len(self.RelPosi),self.RelPosi,self.Vmeanch1,self.PosiCh1err,self.Verrch1)

        grch0.SetTitle(axis+ ':' + self.filename)
        grch0.GetXaxis().SetTitle( 'Relative position [mm]' )
        grch0.GetYaxis().SetTitle( 'V_{p} [V]' )
        grch0.GetXaxis().SetTitleSize(0.05)
        grch0.GetYaxis().SetTitleSize(0.05)
        grch0.GetXaxis().SetTitleOffset(0.9)
        grch0.GetYaxis().SetTitleOffset(0.9)

        grch0.SetMarkerColor(2)
        grch1.SetMarkerColor(4)

        grch0.SetMarkerStyle(4)
        grch1.SetMarkerStyle(5)

        grch0.SetMaximum(ymax)
        grch0.SetMinimum(ymin)
        
        grch1.SetMaximum(ymax)
        grch1.SetMinimum(ymin)

        grch0.Draw("AP")
        grch1.Draw("same P")

        fch0 = TF1('fch0','pol2')
        fch1 = TF1('fch1','pol2')
        
        #fch0.SetParameters(0,0)
        #fch0.SetParameters(1,0.1)
        
        fch0.SetLineColor(2)
        fch1.SetLineColor(4)
        fch0.SetLineStyle(1)
        fch1.SetLineStyle(2)
        fch0.SetLineWidth(1)
        fch1.SetLineWidth(1)

        # To present a fitting parameters on a window 
        #gStyle.SetOptFit (1111)
        #gStyle.SetStatX ( 0.89 )
        #gStyle.SetStatY ( 0.89 )
        #gStyle.SetFillColor(2)
        #gStyle.SetStatBorderSize (0) 
    
        grch0.Fit(fch0,"R","",xmin,xmax)

        # To present a fitting parameters on a window 
        #gStyle.SetOptFit (1111)
        #gStyle.SetStatX ( 0.89 )
        #gStyle.SetStatY ( 0.49 )
        #gStyle.SetStatBorderSize (0)
        
        grch1.Fit(fch1,"R+","",xmin,xmax)
        

        saves['grch0']=grch0
        saves['grch1']=grch1
            
        l = TLegend(0.78,0.8,0.98,0.95)
        l.SetFillColor(0)
        l.AddEntry(grch0,'ch0',"p")
        l.AddEntry(grch1,'ch1',"p")
        l.Draw()
        saves['l'] = l

        parch0 = fch0.GetParameters()
        errch0 = fch0.GetParErrors()
        parch1 = fch1.GetParameters()
        errch1 = fch1.GetParErrors()
        Chich0 = fch0.GetChisquare()
        Chich1 = fch1.GetChisquare()
        NDFch0 = fch0.GetNDF()
        NDFch1 = fch1.GetNDF()

        file = axis+ '_' + self.filename + '_scanFitParameters' +'.txt'
        f = open(file,'w')
        f.write('fitting result ch0: chi2/ndf = ' + str(Chich0)+ '/' + str(NDFch0) + '\n')
        f.write('                    c = ' + str(parch0[0])+ ' +- ' +str(errch0[0])+'\n')
        f.write('                    b = ' + str(parch0[1])+ ' +- ' +str(errch0[1])+'\n')
        f.write('                    a = ' + str(parch0[2])+ ' +- ' +str(errch0[2])+'\n\n') 

        f.write('fitting result ch1: chi2/ndf = ' + str(Chich1)+'/'+str(NDFch1)+'\n')
        f.write('                    c = ' + str(parch1[0])+ ' +- ' +str(errch1[0])+'\n')
        f.write('                    b = ' + str(parch1[1])+ ' +- ' +str(errch1[1])+'\n')
        f.write('                    a = ' + str(parch1[2])+ ' +- ' +str(errch1[2])+'\n\n')

        ca.caput("plot:ach0",parch0[2])
        ca.caput("plot:bch0",parch0[1])
        ca.caput("plot:cch0",parch0[0])
        ca.caput("plot:ach1",parch1[2])
        ca.caput("plot:bch1",parch1[1])
        ca.caput("plot:cch1",parch1[0])


        if parch0[2] == 0:
            xch0 = 0
            ych0 = 0
        elif parch0[2] != 0:
            xch0 = -parch0[1]/2./parch0[2]
            ych0 = parch0[0] - parch0[1]*parch0[1]/4./parch0[2]

        if parch1[2] == 0:
            xch1 = 0
            ych1 = 0
        elif parch1[2] != 0:
            xch1 = -parch1[1]/2./parch1[2]
            ych1 = parch1[0] - parch1[1]*parch1[1]/4./parch1[2]
            
        print 'Scan ch0 (x0,y0) = (',xch0,',',ych0,')'
        print 'Scan ch1 (x0,y0) = (',xch1,',',ych1,')' 
        
        cv.Modified()
        cv.Update()
        f.close()


     ## Power at the PXI digitisers
    def scanPlot_all(self):
        global saves


        xmin = ca.caget("plot:xmin")
        xmax = ca.caget("plot:xmax")
        ymin = ca.caget("plot:scanymin")
        ymax = ca.caget("plot:scanymax")
        axis = self.d.axis[0]
    
        #self.PositionError(axis)
    
        cv = TCanvas("cv","",10,40,1100,1000)
        cv.SetGrid()
        cv.SetFillColor( 0 )
        saves['cv']=cv
    
        pad1 = TPad( 'pad1', 'This is pad1', 0.05, 0.02, 0.98, 0.98, 0 )
        pad1.Draw()
        saves[ 'pad1' ] = pad1
    
        pad1.cd()
    
        pad1.SetFillColor( 42 )
        pad1.SetBorderMode( 0 )
        pad1.SetBorderSize( 1 )
        pad1.SetFrameBorderMode( 0 )
        pad1.SetGrid()
        pad1.SetFillColor( 0 )
    
        gStyle.SetTitleOffset(100,"Y")
        gStyle.SetTitleFillColor(0)
        gStyle.SetTitleBorderSize(0)
    
    
        if axis == 'Vert':
            gr = TGraph(len(self.py),self.py,self.Vmaxch0)
            posi_ave = np.mean(self.px)
            print self.py, self.Vmaxch0
            print len(self.py),len(self.Vmaxch0)
    
        elif axis == 'Hori':
            gr = TGraph(len(self.px),self.px,self.Vmaxch1)
            posi_ave = np.mean(self.py)
            print self.px, self.Vmaxch1
            print len(self.px),len(self.Vmaxch1)
    
    
    
        gr.SetTitle(axis+ ':' + self.filename)
        gr.GetXaxis().SetTitle( 'Position [mm]' )
        gr.GetYaxis().SetTitle( 'V_{p} [V]' )
        gr.GetXaxis().SetTitleSize(0.05)
        gr.GetYaxis().SetTitleSize(0.05)
        gr.GetXaxis().SetTitleOffset(0.9)
        gr.GetYaxis().SetTitleOffset(0.9)
        gr.SetMaximum(ymax)
        gr.SetMinimum(ymin)
    
        f = TF1("f","[0]*pow((x[0]-[1]),2)+[2]",xmin,xmax)
    
        if axis == 'Hori':
            gr.SetMarkerColor(2)
            gr.SetMarkerStyle(4)
            f.SetLineColor(2)
    
        elif axis == 'Vert':
            gr.SetMarkerColor(6)
            gr.SetMarkerStyle(5)
            f.SetLineColor(6)
    
    
        gr.Draw("AP")
    
        f.SetLineStyle(1)
        f.SetLineWidth(1)
    
        f.SetParName(0,"a")
        f.SetParName(1,"b")
        f.SetParName(2,"c")
    
        gStyle.SetOptFit (111)
        gStyle.SetStatX ( 0.49 )
        gStyle.SetStatY ( 0.85 )
        gStyle.SetFillColor(2)
        gStyle.SetStatBorderSize (1)
        gStyle.SetFrameLineColor (2)
    
        gr.Fit(f,"R","",xmin,xmax)
    
        saves['gr']=gr
    
        l = TLegend(0.78,0.8,0.98,0.95)
        l.SetFillColor(0)
        l.AddEntry(gr,'Data',"p")
        l.AddEntry(f,'y=a(x-b)^{2}+c',"l")
        l.Draw()
        saves['l'] = l
    
        par = f.GetParameters()
        err = f.GetParErrors()
        Chi = f.GetChisquare()
        NDF = f.GetNDF()
    
    
        file = axis+ '_' + self.filename + '_scanFitParameters' +'.txt'
        f = open(file,'w')
        f.write('fitting result ch0: chi2/ndf = ' + str(Chi)+ '/' + str(NDF) + '\n')
        f.write('                    c = ' + str(par[0])+ ' +- ' +str(err[0])+'\n')
        f.write('                    b = ' + str(par[1])+ ' +- ' +str(err[1])+'\n')
        f.write('                    a = ' + str(par[2])+ ' +- ' +str(err[2])+'\n')
        f.write('                    posi_ave = ' + str(posi_ave) +'\n\n')
    
    
    
        cv.Modified()
        cv.Update()
        cv.SaveAs(axis+ '_' + self.filename + '.eps')
        f.close()


 ## Power at the Red Pitaya digitisers
    def scanPlotRP_all(self):
        global saves


        xmin = ca.caget("plot:xmin")
        xmax = ca.caget("plot:xmax")
        ymin = ca.caget("plot:scanymin")
        ymax = ca.caget("plot:scanymax")
        axis = self.d.axis[0]
    
        #self.PositionError(axis)
    
        cv = TCanvas("cv","",10,40,1100,1000)
        cv.SetGrid()
        cv.SetFillColor( 0 )
        saves['cv']=cv
    
        pad1 = TPad( 'pad1', 'This is pad1', 0.05, 0.02, 0.98, 0.98, 0 )
        pad1.Draw()
        saves[ 'pad1' ] = pad1
    
        pad1.cd()
    
        pad1.SetFillColor( 42 )
        pad1.SetBorderMode( 0 )
        pad1.SetBorderSize( 1 )
        pad1.SetFrameBorderMode( 0 )
        pad1.SetGrid()
        pad1.SetFillColor( 0 )
    
        gStyle.SetTitleOffset(100,"Y")
        gStyle.SetTitleFillColor(0)
        gStyle.SetTitleBorderSize(0)
    
    
        if axis == 'Vert':
            grv = TGraph(len(self.py),self.py,self.Vmaxch0)
            grh = TGraph(len(self.py),self.py,self.Vmaxch1)
            #posi_ave = np.mean(self.px)
            print self.py, self.Vmaxch0
            print len(self.py),len(self.Vmaxch0)
       
        elif axis == 'Hori':
            grv = TGraph(len(self.px),self.px,self.Vmaxch0)
            grh = TGraph(len(self.px),self.px,self.Vmaxch1)
            #posi_ave = np.mean(self.py)
            print self.px, self.Vmaxch1
            print len(self.px),len(self.Vmaxch1)
    
        grv.SetTitle(axis+ ':' + self.filename)
        grv.GetXaxis().SetTitle( 'Position [mm]' )
        grv.GetYaxis().SetTitle( 'Counts' )
        grv.GetXaxis().SetTitleSize(0.05)
        grv.GetYaxis().SetTitleSize(0.05)
        grv.GetXaxis().SetTitleOffset(0.9)
        grv.GetYaxis().SetTitleOffset(0.9)
        grv.SetMaximum(ymax)
        grv.SetMinimum(ymin)
    
        f = TF1("f","[0]*pow((x[0]-[1]),2)+[2]",xmin,xmax)
    
        #if axis == 'Hori':
        grv.SetMarkerColor(2)
        grv.SetMarkerStyle(4)
        #f.SetLineColor(2)
    
        #elif axis == 'Vert':
        grh.SetMarkerColor(6)
        grh.SetMarkerStyle(5)
        #f.SetLineColor(6)
    
    
        grv.Draw("AP")
        grh.Draw("same P")
    
        #f.SetLineStyle(1)
        #f.SetLineWidth(1)
    
        #f.SetParName(0,"a")
        #f.SetParName(1,"b")
        #f.SetParName(2,"c")
    
        gStyle.SetOptFit (111)
        gStyle.SetStatX ( 0.49 )
        gStyle.SetStatY ( 0.85 )
        gStyle.SetFillColor(2)
        gStyle.SetStatBorderSize (1)
        gStyle.SetFrameLineColor (2)
    
        #gr.Fit(f,"R","",xmin,xmax)
    
        saves['grv']=grv
        saves['grh']=grh
    
        l = TLegend(0.78,0.8,0.98,0.95)
        l.SetFillColor(0)
        l.AddEntry(grv,'cha',"p")
        l.AddEntry(grh,'chb',"p")
        #l.AddEntry(f,'y=a(x-b)^{2}+c',"l")
        l.Draw()
        saves['l'] = l
    
        par = f.GetParameters()
        err = f.GetParErrors()
        Chi = f.GetChisquare()
        NDF = f.GetNDF()
    
    
        #file = axis+ '_' + self.filename + '_scanFitParameters' +'.txt'
        #f = open(file,'w')
        #f.write('fitting result ch0: chi2/ndf = ' + str(Chi)+ '/' + str(NDF) + '\n')
        #f.write('                    c = ' + str(par[0])+ ' +- ' +str(err[0])+'\n')
        #f.write('                    b = ' + str(par[1])+ ' +- ' +str(err[1])+'\n')
        #f.write('                    a = ' + str(par[2])+ ' +- ' +str(err[2])+'\n')
        #f.write('                    posi_ave = ' + str(posi_ave) +'\n\n')
    
    
    
        cv.Modified()
        cv.Update()
        
        cv.SaveAs('/home/epics/ipsbpm/bpm/run/dat/figure/'+axis+ '_' + self.filename + '_raw.eps')
        #f.close()
        
    def scanPlotRP_all_py(self):
        xmin = ca.caget("plot:xmin")
        xmax = ca.caget("plot:xmax")
        ymin = ca.caget("plot:scanymin")
        ymax = ca.caget("plot:scanymax")
        axis = self.d.axis[0]

        if axis == 'Vert':
            pl.plot(self.py,self.Vmaxch0,"ro")
            pl.plot(self.py,self.Vmaxch1,"b+")
                 
        if axis == 'Hori':
            pl.plot(self.px,self.Vmaxch0,"ro")
            pl.plot(self.px,self.Vmaxch1,"b+")

        pl.xlim(xmin,xmax)
        pl.ylim(ymin,ymax)
        pl.ylabel("Maximum counts of raw signal")
        pl.xlabel("stage position [mm]")

        pl.grid(True)
        pl.savefig('/home/epics/ipsbpm/bpm/run/dat/figure/'+axis+ '_' + self.filename + '_raw.eps')


    def scanPlot_RPddc_py(self):
        xmin = ca.caget("plot:xmin")
        xmax = ca.caget("plot:xmax")
        ymin = ca.caget("plot:scanymin")
        ymax = ca.caget("plot:scanymax")
        axis = self.d.axis[0]

        if axis == 'Hori':
            pl.plot(self.px,self.chxmax,"ro")
        if axis == 'Vert':
            pl.plot(self.py,self.chymax,"b+")

        pl.ylabel("amplitude of filtered signals")
        pl.xlabel("stage position [mm]")

        pl.xlim(xmin,xmax)
        pl.ylim(ymin,ymax)
        pl.grid(True)
        pl.savefig('/home/epics/ipsbpm/bpm/run/dat/figure/'+axis+ '_' + self.filename + '_ddc.eps')

    ## Power at the Red Pitaya digitisers

    def scanPlot_RPddc(self):
        global saves
        
       
        xmin = ca.caget("plot:xmin")
        xmax = ca.caget("plot:xmax")
        ymin = ca.caget("plot:scanymin")
        ymax = ca.caget("plot:scanymax")
        axis = self.d.axis[0]
    
        
        V = []
        
        #self.PositionError(axis)
        
        cv = TCanvas("cv","",10,40,700,500)
        cv.SetGrid()
        cv.SetFillColor( 0 )
        saves['cv']=cv
        
        pad1 = TPad( 'pad1', 'This is pad1', 0.05, 0.02, 0.98, 0.98, 0 )
        pad1.Draw()
        saves[ 'pad1' ] = pad1
        
        pad1.cd()
        
        pad1.SetFillColor( 42 )
        pad1.SetBorderMode( 0 )
        pad1.SetBorderSize( 1 )
        pad1.SetFrameBorderMode( 0 )
        pad1.SetGrid()
        pad1.SetFillColor( 0 )
        
        gStyle.SetTitleOffset(100,"Y")
        gStyle.SetTitleFillColor(0)
        gStyle.SetTitleBorderSize(0)
               
        if axis == 'Hori':
            for i in self.chxmax:
                vpeak = i
                V.append(vpeak)
            self.V = pl.array(V)
            
            gr = TGraph(len(self.px),self.px,self.V)
    
        elif axis == 'Vert':
            for i in self.chymax:
                vpeak = i
                V.append(vpeak)
            self.V = pl.array(V)
            gr = TGraph(len(self.py),self.py,self.V)
        
        
        
        gr.SetTitle(self.filename)
        gr.GetXaxis().SetTitle( 'Position [mm]' )
        gr.GetYaxis().SetTitle( 'Amplitude of filtered signal' )
        gr.GetXaxis().SetTitleSize(0.05)
        gr.GetYaxis().SetTitleSize(0.05)
        gr.GetXaxis().SetTitleOffset(0.9)
        gr.GetYaxis().SetTitleOffset(0.9)
        gr.SetMaximum(ymax)
        gr.SetMinimum(ymin)
      
        
        f = TF1("f","[0]*fabs(x[0]-[1])+[2]",xmin,xmax)
        
        if axis == 'Hori':
            gr.SetMarkerColor(2)
            gr.SetMarkerStyle(4)
            f.SetLineColor(2)
        
        elif axis == 'Vert':
            gr.SetMarkerColor(6)
            gr.SetMarkerStyle(5)
            f.SetLineColor(6)
        
        
        gr.Draw("AP")
        
        f.SetLineStyle(1)
        f.SetLineWidth(1)
        
        f.SetParName(0,"a")
        f.SetParName(1,"b")
        f.SetParName(2,"c")
        
        gStyle.SetOptFit (111)
        gStyle.SetStatX ( 0.69 )
        gStyle.SetStatY ( 0.85 )
        gStyle.SetFillColor(2)
        gStyle.SetStatBorderSize (1)
        gStyle.SetFrameLineColor (2)
        
        #f.SetParameter(1,0.1)
        #f.SetParameter(2,0.03)
        
        gr.Fit(f,"R","",xmin,xmax)
        
        saves['gr']=gr
        
        l = TLegend(0.78,0.8,0.98,0.95)
        l.SetFillColor(0)
        l.AddEntry(gr,'Measurement',"p")
        l.AddEntry(f,'y=a|x-b|+c',"l")
        l.Draw()
        saves['l'] = l
        
               
        cv.Modified()
        cv.Update()

        
        cv.SaveAs('/home/epics/ipsbpm/bpm/run/dat/figure/'+axis+ '_' + self.filename + '_ddc.eps')


    # Power at the PXI digitisers
    # Stage positions are real value (not ideal positions)
    def scanPlot_abs(self):
        global saves

        xmin = ca.caget("plot:xmin")
        xmax = ca.caget("plot:xmax")
        ymin = ca.caget("plot:scanymin")
        ymax = ca.caget("plot:scanymax")
        axis = self.d.axis[0]
        
        self.PositionError(axis)

        cv = TCanvas("cv","",10,40,1100,1000)
        cv.SetGrid()
        cv.SetFillColor( 0 )
        saves['cv']=cv

        pad1 = TPad( 'pad1', 'This is pad1', 0.05, 0.02, 0.98, 0.98, 0 )
        pad1.Draw()
        saves[ 'pad1' ] = pad1

        pad1.cd()
       
        pad1.SetFillColor( 42 )
        pad1.SetBorderMode( 0 )
        pad1.SetBorderSize( 1 )
        pad1.SetFrameBorderMode( 0 )
        pad1.SetGrid()
        pad1.SetFillColor( 0 )

        gStyle.SetTitleOffset(100,"Y")
        gStyle.SetTitleFillColor(0)
        gStyle.SetTitleBorderSize(0)


        if axis == 'Vert':
            gr = TGraphErrors(len(self.PosiCh0),self.PosiCh0,self.Vmeanch0,self.PosiCh0err,self.Verrch0)
    
        elif axis == 'Hori':
            gr = TGraphErrors(len(self.PosiCh1),self.PosiCh1,self.Vmeanch1,self.PosiCh1err,self.Verrch1)
        
        gr.SetTitle(axis+ ':' + self.filename)
        gr.GetXaxis().SetTitle( 'Position [mm]' )
        gr.GetYaxis().SetTitle( 'V_{p} [V]' )
        gr.GetXaxis().SetTitleSize(0.05)
        gr.GetYaxis().SetTitleSize(0.05)
        gr.GetXaxis().SetTitleOffset(0.9)
        gr.GetYaxis().SetTitleOffset(0.9)
        gr.SetMaximum(ymax)
        gr.SetMinimum(ymin)
        
        f = TF1('f',"[0]*pow((x[0]-[1]),2)+[2]")

        if axis == 'Hori':
            gr.SetMarkerColor(2)
            gr.SetMarkerStyle(4)
            f.SetLineColor(2)
        
        
        elif axis == 'Vert':
            gr.SetMarkerColor(6)
            gr.SetMarkerStyle(5)
            f.SetLineColor(6)
        

        gr.Draw("AP")
        
        f.SetLineStyle(1)
        f.SetLineWidth(1)
        
        f.SetParName(0,"a")
        f.SetParName(1,"b")
        f.SetParName(2,"c")
        f.SetParameter(0,0.4)
        f.SetParameter(1,-0.05)
        
        
        # To present a fitting parameters on a window
        gStyle.SetOptFit (1111)
        gStyle.SetStatX ( 0.49 )
        gStyle.SetStatY ( 0.89 )
        gStyle.SetStatBorderSize (1)
        gStyle.SetFillColor(0)

        gr.Fit(f,"R","",xmin,xmax)

        saves['gr']=gr
            
        l = TLegend(0.68,0.8,0.98,0.95)
        l.SetFillColor(0)
        l.AddEntry(gr,'Data',"p")
        l.AddEntry(f,'y=a(x-b)^{2}+c',"l")
        l.Draw()
        saves['l'] = l  
        
        
        par = f.GetParameters()
        err = f.GetParErrors()
        Chi = f.GetChisquare()
        NDF = f.GetNDF()

        
        file = axis+ '_' + self.filename + '_scanFitParameters' +'.txt'
        f = open(file,'w')
        f.write('fitting result a(x+b)^2+c: chi2/ndf = ' + str(Chi)+ '/' + str(NDF) + '\n')
        f.write('                    c = ' + str(par[0])+ ' +- ' +str(err[0])+'\n')
        f.write('                    b = ' + str(par[1])+ ' +- ' +str(err[1])+'\n')
        f.write('                    a = ' + str(par[2])+ ' +- ' +str(err[2])+'\n\n')
        
        cv.Modified()
        cv.Update()
        f.close()

        ca.caput("plot:ach0",par[2])
        ca.caput("plot:bch0",par[1])
        ca.caput("plot:cch0",par[0])
      
    ## Power at the PXI digitisers
    ## For 2D scan
    def scanPlot2D(self,Step1st,Step2nd): 
        global saves
        
        xmin = ca.caget("plot:xmin")
        xmax = ca.caget("plot:xmax")
        ymin = ca.caget("plot:scan2Dymin")
        ymax = ca.caget("plot:scan2Dymax")
        axis1st = self.scan2d.axis[0]
        
        
        self.PositionErrorScan2D(axis1st)
        

        for i in range(0,int(Step1st+1)):
            cv_i = TCanvas("cv","",10,40,1100,1000)
            cv_i.SetGrid()
            cv_i.SetFillColor( 0 )
            saves['cv_'+str(i)]=cv_i
    
            pad1_i = TPad( 'pad1_i', 'This is pad1', 0.05, 0.02, 0.98, 0.98, 0 )
            pad1_i.Draw()
            
            saves[ 'pad1_'+str(i) ] = pad1_i
    
           
            pad1_i.cd()
            pad1_i.SetFillColor( 42 )
            pad1_i.SetBorderMode( 0 )
            pad1_i.SetBorderSize( 1 )
            pad1_i.SetFrameBorderMode( 0 )
            pad1_i.SetGrid()
            pad1_i.SetFillColor( 0 )
    
            gStyle.SetTitleOffset(100,"Y")
            gStyle.SetTitleFillColor(0)
            gStyle.SetTitleBorderSize(0)
    
    
            print 'Plot>' , self.RelPosi2nd, self.Vmeanch0, self.PosiCh0err, self.Verrch0
            grch0_i = TGraphErrors(len(self.RelPosi2nd[i*(Step2nd):(i+1)*(Step2nd)]),self.RelPosi2nd[i*(Step2nd):(i+1)*(Step2nd)],self.Vmeanch0[i*(Step2nd):(i+1)*(Step2nd)],self.PosiCh0err[i*(Step2nd):(i+1)*(Step2nd)],self.Verrch0[i*(Step2nd):(i+1)*(Step2nd)])
            grch1_i = TGraphErrors(len(self.RelPosi2nd[i*(Step2nd):(i+1)*(Step2nd)]),self.RelPosi2nd[i*(Step2nd):(i+1)*(Step2nd)],self.Vmeanch1[i*(Step2nd):(i+1)*(Step2nd)],self.PosiCh1err[i*(Step2nd):(i+1)*(Step2nd)],self.Verrch1[i*(Step2nd):(i+1)*(Step2nd)])
    
            grch0_i.SetTitle(axis1st+ ' ' + str(self.RelPosi1st[i]) + ' :' + self.filename)
            grch0_i.GetXaxis().SetTitle( 'Relative position [mm]' )
            grch0_i.GetYaxis().SetTitle( 'V_{p} [V]' )
            grch0_i.GetXaxis().SetTitleSize(0.05)
            grch0_i.GetYaxis().SetTitleSize(0.05)
            grch0_i.GetXaxis().SetTitleOffset(0.9)
            grch0_i.GetYaxis().SetTitleOffset(0.9)
    
            grch0_i.SetMarkerColor(2)
            grch1_i.SetMarkerColor(4)
            
            grch0_i.SetMarkerStyle(4)
            grch1_i.SetMarkerStyle(5)
    
            grch0_i.SetMaximum(ymax)
            grch0_i.SetMinimum(ymin)
    
            grch1_i.SetMaximum(ymax)
            grch1_i.SetMinimum(ymin)
    
            grch0_i.Draw("AP")
            grch1_i.Draw("same P")
    
            fch0_i = TF1('fch0','pol2')
            fch1_i = TF1('fch1','pol2')
    
            #fch0.SetParameters(0,0)
            #fch0.SetParameters(1,0.1)
    
            fch0_i.SetLineColor(2)
            fch1_i.SetLineColor(4)
            fch0_i.SetLineStyle(1)
            fch1_i.SetLineStyle(2)
            fch0_i.SetLineWidth(1)
            fch1_i.SetLineWidth(1)
            
            # To present a fitting parameters on a window
            #gStyle.SetOptFit (1111)
            #gStyle.SetStatX ( 0.89 )
            #gStyle.SetStatY ( 0.89 )
            #gStyle.SetFillColor(2)
            #gStyle.SetStatBorderSize (0)
            
            grch0_i.Fit(fch0_i,"R","",xmin,xmax)
            
            # To present a fitting parameters on a window
            #gStyle.SetOptFit (1111)
            #gStyle.SetStatX ( 0.89 )
            #gStyle.SetStatY ( 0.49 )
            #gStyle.SetStatBorderSize (0)
            
            grch1_i.Fit(fch1_i,"R+","",xmin,xmax)
    
    
            saves['grch0_'+str(i)]=grch0_i
            saves['grch1+'+str(i)]=grch1_i
    
            l_i = TLegend(0.78,0.8,0.98,0.95)
            l_i.SetFillColor(0)
            l_i.AddEntry(grch0_i,'ch0',"p")
            l_i.AddEntry(grch1_i,'ch1',"p")
            l_i.Draw()
            saves['l_'+str(i)] = l_i
    
            parch0_i = fch0_i.GetParameters()
            errch0_i = fch0_i.GetParErrors()
            parch1_i = fch1_i.GetParameters()
            errch1_i = fch1_i.GetParErrors()
            Chich0_i = fch0_i.GetChisquare()
            Chich1_i = fch1_i.GetChisquare()
            NDFch0_i = fch0_i.GetNDF()
            NDFch1_i = fch1_i.GetNDF()
            
            #file = axis1st+ '_' + str(self.RelPosi1st[i]) + '_'  + self.filename + '_scanFitParameters' +'.txt'
            #f = open(file,'w')
            
            #f.write('fitting result ch0: chi2/ndf = ' + str(Chich0)+ '/' + str(NDFch0) + '\n')
            #f.write('                    c = ' + str(parch0[0])+ ' +- ' +str(errch0[0])+'\n')
            #f.write('                    b = ' + str(parch0[1])+ ' +- ' +str(errch0[1])+'\n')
            #f.write('                    a = ' + str(parch0[2])+ ' +- ' +str(errch0[2])+'\n\n')
            
            #f.write('fitting result ch1: chi2/ndf = ' + str(Chich1)+'/'+str(NDFch1)+'\n')
            #f.write('                    c = ' + str(parch1[0])+ ' +- ' +str(errch1[0])+'\n')
            #f.write('                    b = ' + str(parch1[1])+ ' +- ' +str(errch1[1])+'\n')
            #f.write('                    a = ' + str(parch1[2])+ ' +- ' +str(errch1[2])+'\n\n')
            
                       
            if parch0_i[2] == 0:
                xch0 = 0
                ych0 = 0
            elif parch0_i[2] != 0:
                xch0 = -parch0_i[1]/2./parch0_i[2]
                ych0 = parch0_i[0] - parch0_i[1]*parch0_i[1]/4./parch0_i[2]
                
            if parch1_i[2] == 0:
                xch1 = 0
                ych1 = 0
            elif parch1_i[2] != 0:
                xch1 = -parch1_i[1]/2./parch1_i[2]
                ych1 = parch1_i[0] - parch1_i[1]*parch1_i[1]/4./parch1_i[2]
                
            print 'Scan ch0 (x0,y0) = (',xch0,',',ych0,')'
            print 'Scan ch1 (x0,y0) = (',xch1,',',ych1,')'
                
            cv_i.Modified()
            cv_i.Update()
            #f.close()

    def PositionError(self,axis):
     
        PosiXerr = []
        PosiYerr = []
        PosiXmean = []
        PosiYmean = []
        PosiXmax = []
        PosiYmax = []
        nstep = self.d.scannstep[0]
        delta = float(self.d.scanrange[0])/nstep
       
              
        # Position error Anaylisis
        
        for i in range(0,int(nstep+1)) :
            
            PX =self.px[i*20:(i+1)*20]
            PY =self.py[i*20:(i+1)*20]
            # peak value of each pulse
            PpX = max(PX)
            PpY = max(PY)
            
            PosiXmax.append(float(PpX))
            PosiYmax.append(float(PpY))
            
            # Mean value of peak value & error
            vmX = np.mean(PX)
            vmY = np.mean(PY)
            verX  = PpX - vmX
            verY  = PpY - vmY
            
            # append Mean value & error
            PosiXmean.append(float(vmX))
            PosiYmean.append(float(vmY))
            PosiXerr.append(float(verX))
            PosiYerr.append(float(verY))
        
        
        self.PosiXmean = np.array(PosiXmean)
        self.PosiYmean = np.array(PosiYmean)
        self.PosiXerr = np.array(PosiXerr)
        self.PosiYerr = np.array(PosiYerr)
        self.PosiXmax = np.array(PosiXmax)
        self.PosiYmax = np.array(PosiYmax)
        
        print 'max Position value>',self.PosiXmax,self.PosiYmax
        print 'Posi mean', self.PosiXmean,self.PosiYmean
        print 'Posi err',self.PosiXerr,self.PosiYerr
        
        if self.PXIch0 == 'Vert':
            self.PosiCh0err = self.PosiYerr
            self.PosiCh1err = self.PosiXerr
            self.PosiCh0    = self.PosiYmean
            self.PosiCh1    = self.PosiXmean
        elif self.PXIch0 == 'Hori':
            self.PosiCh0err = self.PosiXerr
            self.PosiCh1err = self.PosiYerr
            self.PosiCh0    = self.PosiXmean
            self.PosiCh1    = self.PosiYmean
    
    def PositionErrorScan2D(self,axis1st):
        abpX = []    # absolute value of horizontal CBPM position
        abpY = []    # absolute value of vertical CBPM position
        PosiXerr = []
        PosiYerr = []
        nstep1st = self.scan2d.scan2Dnstep1st[0]
        nstep2nd = self.scan2d.scan2Dnstep2nd[0]
        delta = float(self.scan2d.scan2Drange2nd[0])/nstep2nd
       
        # Append absolute CBPM hori positions
        for i in self.scan2d.posix:
            abpX.append(i)
        # Append absolute CBPM vert positions
        for i in self.scan2d.posiy:
            abpY.append(i)
       
        # Position error Anaylisis
        if axis1st == 'Vert':
            pXer = 0
            pYer = abpY[0]
            PosiXerr.append(pXer)
            for i in range(0,int((nstep1st+1)*(nstep2nd+1)-1)):

                print i, abpX[i+1],abpX[i]
                pXer = pl.fabs((abpX[i+1]-abpX[i])-delta)
                pYer += abpY[i+1]
                PosiXerr.append(pXer)
               
            for i in range(0,int((nstep1st+1)*(nstep2nd+1)-1)):
                PosiYerr.append(pl.fabs(float(pYer)/int(nstep2nd+1)))
            self.PosiXerr = pl.array(PosiXerr)
            self.PosiYerr = pl.array(PosiYerr)
        elif axis1st == 'Hori':
            pXer = abpX[0]
            pYer = 0
            PosiYerr.append(pYer)
            for i in range(0,int((nstep1st+1)*(nstep2nd+1)-1)):
                pXer += abpX[i+1]
                pYer = pl.fabs((abpY[i+1]-abpY[i])-delta)
                PosiYerr.append(pYer)
               
            for i in range(0,int((nstep1st+1)*(nstep2nd+1)-1)):
                PosiXerr.append(pl.fabs(float(pXer)/int(nstep2nd+1)))
            self.PosiXerr = pl.array(PosiXerr)
            self.PosiYerr = pl.array(PosiYerr)
        print 'error position, X,Y>', self.PosiXerr, self.PosiYerr
        if self.PXIch0 == 'Vert':
            self.PosiCh0err = self.PosiYerr
            self.PosiCh1err = self.PosiXerr
        elif self.PXIch0 == 'Hori':
            self.PosiCh0err = self.PosiXerr
            self.PosiCh1err = self.PosiYerr


class Pol2:
     def __call__(self,x,par):
        return par[0] + x[0]*par[1] +x[1]*x[1]*par[2]

            
