import time
import os
import stat
import fnmatch
import math
import StringIO
import xml.dom.minidom
import signal
import cothread
import cothread.catools
import numpy
import pylab as pl
import gzip

class Info:
    '''Class for information on single BPM'''
        
    def __init__(self,name,freq,type,
                 xpv, xnsamp, xdig,
                 ypv, ynsamp, ydig,
                 xrname, xrpv,
                 yrname, yrpv,
                 mname, process):
        self.name   = name
        self.freq   = freq
        self.type   = type
        self.xpv    = xpv
        self.xnsamp = xnsamp
        self.xdig   = xdig
        self.ypv    = ypv
        self.ynsamp = ynsamp
        self.ydig   = ydig
        self.xrname = xrname
        self.xrpv    = xrpv
        self.yrname = yrname
        self.yrpv   = yrpv
        self.mname  = mname
        self.process= process

    def __str__(self) :
        s =   self.name+' '+self.freq+' '+self.type+' '
        s = s+self.xpv+' '+str(self.xnsamp)+' '+self.xdig+' '
        s = s+self.ypv+' '+str(self.ynsamp)+' '+self.ydig+' '
        s = s+self.xrname+' '+self.xrpv+' '+self.yrname+' '+self.yrpv+' '
        s = s+self.mname+' '+str(self.process)
        return s


class Map:
    """Bpm to hardware map class"""

    directions  = ['x','y']
    globaldata  = ['CDIODE:t0','cbpmnew:xpos','cbpmnew:ypos','diode:t0_det']
    inputparams = ['signalstart','ddcfreq','ddcdecay','ddcfilterparams','ddcisamp','iqrot','posscale','tiltscale','debuglevel',
                   'calsignalstart','calddcfreq','calddcisamp','calmode','caliave','calqave','posflip','posscaleflip','bbaOffset']
    outputdata  = ['ampnoise','pedestal','lastunsat','signalmax','signalabovenoise','firstzero','phasimple','freq','decay',
                   'amp','pha','i','q','ampnorm','phadiff','pos','tilt','calamp','calpha','cali','calq','calampnorm','calphadiff']
    casrvars    = inputparams
    

    def __init__(self, filename):

        self.index   = [] # index 
        self.name    = [] # bpm name
        self.freq    = [] # bpm frequency
        self.type    = [] # type of cavity
        self.xpv     = [] # pv for x cavity
        self.xnsamp  = [] # number of samples in x waveform
        self.xdig    = [] # digitizer bits in x waveform
        self.ypv     = [] # pv for y waveform
        self.ynsamp  = [] # number of samples in y waveform
        self.ydig    = [] # digitizer bits in y
        self.xrname  = [] # x polarisation reference name
        self.yrname  = [] # y polarisation reference name
        self.xrpv    = [] # x reference waveform
        self.yrpv    = [] # y reference waveform
        self.mname   = [] # mover name
        self.xattn   = [] # x attenuator
        self.yattn   = [] # y attenuator
        self.cal     = [] # Is there a calibration tone?
        self.process = [] # process or not
        self.nbunch  = [] # bunch number
        self.diode   = [] # diode name
        
        print 'Bpm.Map.__init__> Loading : ',filename
        self.filename = filename
        f = open(filename,'r')
        for line in f:
            v = line.split()
            self.index.append(int(v[0]))
            self.name.append(v[1])
            self.freq.append(v[2])
            self.type.append(v[3])
            self.xpv.append(v[4])
            self.xnsamp.append(int(v[5]))
            self.xdig.append(v[6])
            self.ypv.append(v[7])
            self.ynsamp.append(v[8])
            self.ydig.append(v[9])
            self.xrname.append(v[10])
            self.yrname.append(v[11])
            self.mname.append(v[12])
            self.xattn.append(int(v[13]))
            self.yattn.append(int(v[14]))            
            self.cal.append(int(v[15]))
            self.process.append(int(v[16]))
            self.xrpv.append('NONE')
            self.yrpv.append('NONE')

                                        
        f.close()
        print 'Bpm.Map.__init__> Loaded : ',self.filename


        # Expand reference names for dipole cavities
        for d in self.name :            
            indd = self.name.index(d)
            if self.type[indd] == 'DIPOLE' :
                # find references for this dipole
                for r in self.name :
                    indr = self.name.index(r)
                    if self.type[indr] == 'MONOPOLE' and self.name[indr] == self.xrname[indd] :
                        self.xrpv[indd] = self.xpv[indr]
                    if self.type[indr] == 'MONOPOLE' and self.name[indr] == self.yrname[indd] :
                        self.yrpv[indd] = self.xpv[indr]

    def sysConfig(self,filename):
        print 'Bpm.Map.__sysConfig__> Loading : ',filename
       
        f = open(filename,'r')
        for line in f:
            v = line.split()
            self.diode.append(v[0])
            self.nbunch.append(int(v[1]))
        f.close()
    
    def findIndex(self,bpmName) :
        return self.name.index(bpmName)

    def findName(self,bpmIndex) :
        return self.name[bpmIndex]

    def getTok(self) :
        tok = 0
        for dig in self.xdig:
            if dig[-3:-2] == 'u' :
                tok = 0
            elif dig[-3:-2] == 'b':
                tok = 1
        return tok

    def getNBpm(self) :
        nbpm = 0
        for bpm in self.name :
            ind = self.name.index(bpm)
            if self.type[ind] == 'DIPOLE' :
                nbpm = nbpm+1
        return nbpm
        
    def getNPol(self) :
        npol = 0
        for bpm in self.name :
            ind = self.name.index(bpm)
            if self.type[ind] == 'DIPOLE' :
                npol = npol+2
            else :
                npol = npol+1
        return npol

    def makeDbConfig(self):
        print 'Bpm.Map.makeDbConfig'
        self.sysConfig(os.environ['SYSCONFIG'])
       
        # Write IOC start up script
        f = open(os.environ['BPMROOT']+'/iocBoot/iocbpm/st.cmd','w')
        f.write('#!../../bin/linux-x86/bpm\n')
        f.write('\n')
        f.write('< envPaths\n')
        
        f.write('\n')
        f.write('cd ${TOP}\n')
        f.write('\n')
        f.write('## Register all support components\n')
        f.write('dbLoadDatabase "dbd/bpm.dbd" \n')
        f.write('bpm_registerRecordDeviceDriver pdbbase\n')


        f.write('dbLoadRecords("db/cbpmLabVIEW.db","name=c2")\n')
        f.write('dbLoadRecords("db/plot.db","name=plot")\n')
        f.write('dbLoadRecords("db/scan.db","name=scan")\n')
        f.write('dbLoadRecords("db/acquire.db","name=RP2")\n')
        

        nbpm=self.getNBpm()
        tok = self.getTok()

        # process
        for ind in self.index :
            if self.type[ind] == 'DIPOLE':
                l = 'dbLoadRecords("db/bpmPolProc.vdb","name='+self.name[ind]+'x,diodename='+self.diode[0]+',waveformName='+self.xpv[ind]+',nbunch='+str(self.nbunch[0])+',tok='+str(tok)+',nowf='+str(self.xnsamp[ind])+'")\n'+'dbLoadRecords("db/bpmPolProc.vdb","name='+self.name[ind]+'y,diodename='+self.diode[0]+',waveformName='+self.ypv[ind]+',nbunch='+str(self.nbunch[0])+',tok='+str(tok)+',nowf='+str(self.ynsamp[ind])+'")\n'

            elif self.type[ind] == 'MONOPOLE':
                l = 'dbLoadRecords("db/bpmPolProc.vdb","name='+self.name[ind]+',diodename='+self.diode[0]+',waveformName='+self.xpv[ind]+',nbunch='+str(self.nbunch[0])+',tok='+str(tok)+',nowf='+str(self.xnsamp[ind])+'")\n'
                
            elif self.type[ind] == 'DIODE':
                l = 'dbLoadRecords("db/bpmDiodeProc.db","name='+self.name[ind]+',waveformName='+self.xpv[ind]+',ns='+str(self.xnsamp[ind])+'")\n'
                
            f.write(l)
                
        # IQ and position calculations
        for ind in self.index :
            if self.type[ind] == 'DIPOLE':
                l = 'dbLoadRecords("db/bpmPolProcIQpos.vdb","name='+self.name[ind]+'x,rname='+self.xrname[ind]+',diodename='+self.diode[0]+',nowf='+str(self.xnsamp[ind])+',nbunch='+str(self.nbunch[0])+',nbpms='+str(nbpm)+'")\n'+'dbLoadRecords("db/bpmPolProcIQpos.vdb","name='+self.name[ind]+'y,rname='+self.yrname[ind]+',diodename='+self.diode[0]+',nowf='+str(self.ynsamp[ind])+',nbunch='+str(self.nbunch[0])+',nbpms='+str(nbpm)+'")\n'
            f.write(l)

        i = 0
        bpmNo = []
        trig = []
        trigNo = []
        for ind in self.index:
           if self.type[ind] == 'DIPOLE':
               bpmNo.append(self.name[ind])
        print 'bpmNo>',bpmNo       

        # cbpm poss
        l = 'dbLoadRecords("db/system.vdb","cbpm=cbpm'+',nb='+str(nbpm)+'")\n'
        f.write(l)
            

        f.write('\n')
        f.write('cd ${TOP}/iocBoot/${IOC}\n')
        f.write('iocInit()')
        f.write('\n')
        f.write('## Start any sequence programs\n')
        f.write('#seq sncxxx,"user=ipsbpm"\n')

        f.close()

       # os.chmod(os.environ['BPMROOT']+'/iocBoot/iocbpm/st.cmd',stat.S_IWUSR | stat.S_IWGRP | stat.S_IRUSR | stat.S_IRGRP | stat.S_IXUSR | stat.S_IXGRP)

        print 'Bpm.Map.makeDbConfig> made st.cmd'        
        
    
    def makeEdm(self) : 
        self.makeEdmSummary()
        #self.makeEdmWaveform()
        #self.makeEdmParameters()

        #os.system('edm -x /home/epics/ipsbpm/bpm/edm/summary_auto.edl')
    
    def makeEdmSummary(self) :
        print 'Bpm.Map.makeEdmSummary'
        
        nBpmPerColumn            = 21
        buttonWidth              = 60
        buttonActionWidth        = 70
        buttonHeight             = 25
        bpmY0                    = 100
        bpmYGrid                 = 30
        bpmXGrid                 = 330        
        bpmLabelXOffset          = 3
        bpmLabelYOffset          = 7
        bpmRelatedDisplayXOffset = 20
        bpmCalStatDisplayXOffset = 87
        bpmStatDisplayXOffset    = 132
        bpmTuneDisplayXOffset    = 170
        bpmCalDisplayXOffset     = 220
        bpmLogDisplayXOffset     = 270

        fname = os.environ['EDMFILE']+'/summary_auto.edl'
        os.system("cat ../../edm/summary_auto_template.edl > "+fname)
        f=open(fname,"a")

        # need these at the end
        icol = 0
        irow = 0
        
        # loop over all BPMs 
        for bpm in self.name :
            ind = self.name.index(bpm)
            
            icol = (ind-(ind%nBpmPerColumn))/nBpmPerColumn
            irow = ind%nBpmPerColumn

            # text label
            f.write('# (Static Text)\n')
            f.write('object activeXTextClass\n')
            f.write('beginObjectProperties\n')
            f.write('major 4\n')
            f.write('minor 1\n')
            f.write('release 0\n')
            f.write('x '+str(icol*bpmXGrid+bpmLabelXOffset)+'\n')
            f.write('y '+str(irow*bpmYGrid+bpmY0+bpmLabelYOffset)+'\n')
            f.write('w 12\n')
            f.write('h 12\n')
            f.write('font "helvetica-bold-r-10.0"\n')
            f.write('fgColor index 20\n')
            f.write('bgColor index 0\n')
            f.write('useDisplayBg\n')
            f.write('value {\n')
            f.write('    "'+str(ind)+'"\n')
            f.write('}\n')
            f.write('autoSize\n')
            f.write('endObjectProperties\n')

            # create related displays
            f.write('# (Related Display)\n')
            f.write('object relatedDisplayClass\n')
            f.write('beginObjectProperties\n')
            f.write('major 4\n')
            f.write('minor 2\n')
            f.write('release 0\n')
            f.write('x '+str(icol*bpmXGrid+bpmRelatedDisplayXOffset)+'\n')
            f.write('y '+str(irow*bpmYGrid+bpmY0)+'\n')
            f.write('w '+str(buttonWidth)+'\n')
            f.write('h '+str(buttonHeight)+'\n')
            f.write('fgColor index 14\n')
            f.write('bgColor index 1\n')
            f.write('topShadowColor index 0\n')
            f.write('botShadowColor index 14\n')
            f.write('font "helvetica-medium-r-12.0"\n')
            f.write('buttonLabel "'+self.name[ind]+'"\n')
            f.write('numPvs 4\n')
            f.write('numDsps 1\n')
            f.write('displayFileName {\n')
            if self.type[ind] == 'DIPOLE' :
                f.write('    0 "bpm.edl"\n')
            elif self.type[ind] == 'MONOPOLE' :
                f.write('    0 "ref.edl"\n')
            elif self.type[ind] == 'DIODE' :
                f.write('    0 "diode.edl"\n')
            f.write('}\n')
            f.write('menuLabel {\n')
            f.write('    0 "cbpm1"\n')
            f.write('}\n')
            f.write('symbols {\n')
            if self.type[ind] == 'DIPOLE' :
                f.write('    0 "xpv='+self.xpv[ind]+',ypv='+self.ypv[ind]+',xrpv='+self.xrpv[ind]+',yrpv='+self.yrpv[ind]+',bpm='+self.name[ind]+',xrbpm='+self.xrname[ind]+',yrbpm='+self.yrname[ind]+'"\n')
            elif self.type[ind] == 'MONOPOLE' :
                f.write('    0 "xpv='+self.xpv[ind]+',ypv='+self.ypv[ind]+',name='+self.name[ind]+'"\n')
            elif self.type[ind] == 'DIODE' :
                f.write('    0 "xpv='+self.xpv[ind]+',ypv='+self.ypv[ind]+',qpv='+self.yrpv[ind]+',bpm='+self.name[ind]+',qbpm='+self.xrname[ind]+',xqbpm='+self.xrname[ind]+'"\n')
            f.write('}\n')
            f.write('endObjectProperties\n')

            # create related displays
            f.write('# (Related Display)\n')
            f.write('object relatedDisplayClass\n')
            f.write('beginObjectProperties\n')
            f.write('major 4\n')
            f.write('minor 2\n')
            f.write('release 0\n')
            f.write('x '+str(icol*bpmXGrid+bpmRelatedDisplayXOffset+60)+'\n')
            f.write('y '+str(irow*bpmYGrid+bpmY0)+'\n')
            f.write('w '+str(buttonWidth+10)+'\n')
            f.write('h '+str(buttonHeight)+'\n')
            f.write('fgColor index 14\n')
            f.write('bgColor index 1\n')
            f.write('topShadowColor index 0\n')
            f.write('botShadowColor index 14\n')
            f.write('font "helvetica-medium-r-12.0"\n')
            f.write('buttonLabel "Waveforms"\n')
            f.write('numPvs 4\n')
            f.write('numDsps 1\n')
            f.write('displayFileName {\n')
            f.write('    0 "waveform.edl"\n')
            f.write('}\n')
            f.write('menuLabel {\n')
            f.write('    0 "cbpm1"\n')
            f.write('}\n')
            f.write('symbols {\n')
            f.write('    0 "xpv='+self.xpv[ind]+',ypv='+self.ypv[ind]+',xrpv='+self.xrpv[ind]+',yrpv='+self.yrpv[ind]+',bpm='+self.name[ind]+',xrbpm='+self.xrname[ind]+',yrbpm='+self.yrname[ind]+'"\n')
            f.write('}\n')
            f.write('endObjectProperties\n')

            # scan log data
            f.write('# (Related Display)\n')
            f.write('object relatedDisplayClass\n')
            f.write('beginObjectProperties\n')
            f.write('major 4\n')
            f.write('minor 2\n')
            f.write('release 0\n')
            f.write('x '+str(icol*bpmXGrid+bpmRelatedDisplayXOffset+150)+'\n')
            f.write('y '+str(irow*bpmYGrid+bpmY0)+'\n')
            f.write('w '+str(buttonWidth)+'\n')
            f.write('h '+str(buttonHeight)+'\n')
            f.write('fgColor index 14\n')
            f.write('bgColor index 1\n')
            f.write('topShadowColor index 0\n')
            f.write('botShadowColor index 14\n')
            f.write('font "helvetica-medium-r-12.0"\n')
            f.write('buttonLabel "Scan"\n')
            f.write('numPvs 4\n')
            f.write('numDsps 1\n')
            f.write('displayFileName {\n')
            f.write('    0 "scanRP.edl"\n')
            f.write('}\n')
            f.write('symbols {\n')
            f.write('    0 "bpm='+self.name[ind]+'"\n')
            f.write('}\n')
            f.write('endObjectProperties\n')

            
            
            
            # log data
            f.write('# (Shell Command)\n')
            f.write('object shellCmdClass\n')
            f.write('beginObjectProperties\n')
            f.write('major 4\n')
            f.write('minor 2\n')
            f.write('release 0\n')
            f.write('x '+str(icol*bpmXGrid+bpmLogDisplayXOffset)+'\n')
            f.write('y '+str(irow*bpmYGrid+bpmY0)+'\n')
            f.write('w '+str(buttonActionWidth)+'\n')
            f.write('h '+str(buttonHeight)+'\n')
            f.write('fgColor index 14\n')
            f.write('bgColor index 0\n')
            f.write('topShadowColor index 0\n')
            f.write('botShadowColor index 14\n')
            f.write('font "helvetica-medium-r-12.0"\n')
            f.write('buttonLabel "Log"\n')
            f.write('numCmds 1\n')
            f.write('command {\n')
            f.write('    0 "python '+os.environ['BPMRUN']+'/bpmLogData.py -b '+self.name[ind]+'"\n')
            f.write('}\n')
            f.write('endObjectProperties\n')


            # scan plot data
            f.write('# (Related Display)\n')
            f.write('object relatedDisplayClass\n')
            f.write('beginObjectProperties\n')
            f.write('major 4\n')
            f.write('minor 2\n')
            f.write('release 0\n')
            f.write('x '+str(icol*bpmXGrid+bpmLogDisplayXOffset+120)+'\n')
            f.write('y '+str(irow*bpmYGrid+bpmY0)+'\n')
            f.write('w '+str(buttonWidth+10)+'\n')
            f.write('h '+str(buttonHeight)+'\n')
            f.write('fgColor index 14\n')
            f.write('bgColor index 1\n')
            f.write('topShadowColor index 0\n')
            f.write('botShadowColor index 14\n')
            f.write('font "helvetica-medium-r-12.0"\n')
            f.write('buttonLabel "ScanPlot"\n')
            f.write('numPvs 4\n')
            f.write('numDsps 1\n')
            f.write('displayFileName {\n')
            f.write('    0 "PlotRP.edl"\n')
            f.write('}\n')
            f.write('symbols {\n')
            f.write('    0 "bpm='+self.name[ind]+'"\n')
            f.write('}\n')
            f.write('endObjectProperties\n')
            

        irow = irow+2

        # log all data
        f.write('# (Shell Command)\n')
        f.write('object shellCmdClass\n')
        f.write('beginObjectProperties\n')
        f.write('major 4\n')
        f.write('minor 2\n')
        f.write('release 0\n')
        f.write('x '+str(icol*bpmXGrid+bpmLogDisplayXOffset)+'\n')
        f.write('y '+str(irow*bpmYGrid+bpmY0)+'\n')
        f.write('w '+str(buttonActionWidth)+'\n')
        f.write('h '+str(buttonHeight)+'\n')                
        f.write('fgColor index 14\n')
        f.write('bgColor index 0\n')
        f.write('topShadowColor index 0\n')
        f.write('botShadowColor index 14\n')
        f.write('font "helvetica-medium-r-12.0"\n')
        f.write('buttonLabel "AllBPM"\n')
        f.write('numCmds 3\n')
        f.write('commandLabel {\n')
        f.write('0 "BPM"\n')
        f.write('1 "Pro"\n')
        f.write('2 "Wf"\n')
        f.write('}\n')
        f.write('command {\n')                        
        f.write('    0 "python '+os.environ['BPMRUN']+'/LogAllBPM.py"\n')
        f.write('    1 "python '+os.environ['BPMRUN']+'/LogAllData.py"\n')
        f.write('    2 "python '+os.environ['BPMRUN']+'/LogAllWf.py"\n')
        f.write('}\n')
        f.write('endObjectProperties\n')

        
        
        
        # close file
        f.close()


    def makeCasrConfig(self):
        print 'Bpm.Map.makeCasrConfig'
        f = open('bpmCasr.txt','w')

        f.write('data{\n');

        # general parameters
        f.write('   cbpm:systemstatus\n')        
        f.write('   cbpm:log-npulse\n')
        f.write('   cbpm:logall-npulse\n')

        # casr file status
        f.write('   cbpm:casrFileName\n')
        f.write('   cbpm:casrCalFileName\n')
        f.write('   cbpm:casrBeamFileName\n')                                                                                                                              
        for ind in self.index :
            if self.type[ind] == 'MONOPOLE':
                f.write('   '+self.name[ind]+':signalstart\n')
                f.write('   '+self.name[ind]+':ddcfreq\n')
                f.write('   '+self.name[ind]+':ddcdecay\n')
                f.write('   '+self.name[ind]+':ddcfilterparams\n')
                f.write('   '+self.name[ind]+':ddcisamp\n')
                f.write('   '+self.name[ind]+':delt\n')
            
                
            elif self.type[ind] == 'DIPOLE' :
                f.write('   '+self.name[ind]+'x:signalstart\n')
                f.write('   '+self.name[ind]+'x:ddcfreq\n')
                f.write('   '+self.name[ind]+'x:ddcdecay\n')
                f.write('   '+self.name[ind]+'x:ddcfilterparams\n')
                f.write('   '+self.name[ind]+'x:ddcisamp\n')
                f.write('   '+self.name[ind]+'x:iqrot\n')
                f.write('   '+self.name[ind]+'x:posscale\n')
                f.write('   '+self.name[ind]+'x:tiltscale\n')
                f.write('   '+self.name[ind]+'x:calt0\n')
                f.write('   '+self.name[ind]+'x:bbaOffset\n')
                f.write('   '+self.name[ind]+'x:posflip\n');
                f.write('   '+self.name[ind]+'x:posscaleflip\n');                
                f.write('   '+self.name[ind]+'x:delt\n');                
                
                f.write('   '+self.name[ind]+'y:signalstart\n')
                f.write('   '+self.name[ind]+'y:ddcfreq\n')
                f.write('   '+self.name[ind]+'y:ddcdecay\n')
                f.write('   '+self.name[ind]+'y:ddcfilterparams\n')
                f.write('   '+self.name[ind]+'y:ddcisamp\n')
                f.write('   '+self.name[ind]+'y:iqrot\n')
                f.write('   '+self.name[ind]+'y:posscale\n')
                f.write('   '+self.name[ind]+'y:tiltscale\n')
                f.write('   '+self.name[ind]+'y:calt0\n')
                f.write('   '+self.name[ind]+'y:bbaOffset\n')
                f.write('   '+self.name[ind]+'y:posflip\n');
                f.write('   '+self.name[ind]+'y:posscaleflip\n');                                
                f.write('   '+self.name[ind]+'y:delt\n');                                
        f.write('}\n')
        f.close()
        

    def bpmInfo(self, bpmname):
        print 'Bpm.Map.bpmInfo',bpmname
        
        for idx in self.index :
            if bpmname == self.name[idx] :
                print self.name[idx]+' '+self.type[idx]+' '+str(self.xsis[idx])+' '+str(self.xwf[idx])+' '+str(self.ysis[idx])+' '+str(self.ywf[idx])+' '+self.rname[idx]+' '+str(self.rsis[idx])+' '+str(self.rwf[idx])+' '+self.mname[idx];
                return;
        print 'Could not find bpm'

    def getBpmInfo(self, bpmname):

        if bpmname == 'MREF3WAKE' :
            i = self.getBpmInfo('QD10X')
            # i.name = 'MREF3WAKE'
            i.mname = 'MREF3WAKE'
            return i
        
        print 'Bpm.Map.getBpmInfo> ',bpmname
        for idx in self.index :
            if bpmname == self.name[idx] :
                if self.type[idx] == 'DIPOLE' :
                    i = Info(self.name[idx],self.freq[idx],self.type[idx],
                             self.xpv[idx],self.xnsamp[idx],self.xdig[idx],
                             self.ypv[idx],self.ynsamp[idx],self.ydig[idx],
                             self.xrname[idx],self.xrpv[idx],
                             self.yrname[idx],self.yrpv[idx],
                             self.mname[idx],self.process[idx])
                    return i
                elif self.type[idx] == 'MONOPOLE' :
                    i = Info(self.name[idx],self.freq[idx],self.type[idx],
                             self.xpv[idx],self.xnsamp[idx],self.xdig[idx],
                             self.xpv[idx],self.xnsamp[idx],self.xdig[idx],
                             self.xrname[idx],self.xrpv[idx],
                             self.yrname[idx],self.xrpv[idx],
                             self.mname[idx],self.process[idx])
                    return i
                elif self.type[idx] == 'DIODE' :
                    i = Info(self.name[idx],self.freq[idx],self.type[idx],
                             self.xpv[idx],self.xnsamp[idx],self.xdig[idx],
                             self.xpv[idx],self.xnsamp[idx],self.xdig[idx],
                             self.xrname[idx],self.xrpv[idx],
                             self.yrname[idx],self.yrpv[idx],
                             self.mname[idx],self.process[idx])
                    return i                

        return 0;

    def getBpmName(self, isis, iwf) :
        for idx in self.index :
            if isis == self.xsis[idx] and iwf == self.xwf[idx] :
                return self.name[idx]
            elif isis == self.ysis[idx] and iwf == self.ywf[idx] :
                return self.name[idx]
class Casr :
    """Save and restore class"""
    
    def __init__(self):
        print 'Bpm.Casr.init'
        
    def restoreLast(self):
        print 'Bpm.Casr.restorelast'
        
        lastfile = ''
        mtime = 0
        for name in os.listdir(os.environ['BPMDAT']+'/casr/'):
            full_path = os.path.join(os.path.realpath(os.environ['BPMDAT']+'/casr/'),name)
            if bool(os.path.getmtime(full_path) > mtime) & bool(fnmatch.fnmatch(full_path,'*.dat')):
                mtime = os.path.getmtime(full_path)                                
                fn = name
                lastfile = full_path
        print 'Bpm.Casr.restoreLast '+lastfile
        os.system('carestore -asciiin '+lastfile);
        cothread.catools.caput("cbpm:casrFileName",fn)
        return fn

    def restoreSelection(self):
        print 'Bpm.Casr.restoreSelection'
        os.system('casr')

    def restore(self,filename):
        print 'Bpm.Casr.restore'
        filename = os.environ['BPMDAT']+'/casr/'+filename
        os.system('carestore -asciiin '+filename)
        
    def save(self):
        print 'Bpm.Casr.save'
        t = time.strftime('%Y%m%d_%H%M%S')
        fn = 'bpmCasr_'+t+'.dat'
        print fn
        casavefilename = os.environ['BPMDAT']+'/casr/'+fn
        caconfigfilename = os.environ['BPMRUN']+'/bpmCasr.txt'
        os.system('casave -asciiin '+caconfigfilename+' -asciiout '+casavefilename)
        cothread.catools.caput('cbpm:casrFileName',fn)
        return fn

