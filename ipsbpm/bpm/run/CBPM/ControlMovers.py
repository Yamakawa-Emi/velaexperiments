#! /usr/bin/env python

import cothread
import cothread.catools as ca
import optparse
import time
import Acq as Acq
import os.path as pa

    
class ControlMovers :

    def __init__(self,axis) :
        self.evwait = cothread.Event(auto_reset = False)
        self.axis = axis
        self.latch = 'disarmed'
        self.tartol = 0.01

    def Reset(self) :
        self.evwait.Signal()
        #ca.caput('c2:Posi',0.0)


    def GetCurrent(self,pospv,stapv) :
        cpos       = cothread.catools.caget(pospv)
        movestatus = cothread.catools.caget(stapv)            
        print 'MoverControl:GetCurrent> ',cpos,movestatus
        return cpos,movestatus

    def RangeCheck(self,pospv,stapv,leap) :
        RangeOk = False
        cpos,movestatus = self.GetCurrent(pospv,stapv)
        if (cpos+leap < 1.9) and (cpos+leap > -1.9) :
            RangeOk = True
        return RangeOk

    def MoveRelative(self,leap) :
       
        self.evwait.Reset()
        axis = self.axis
        if axis == 'Hori' :
            pospv      = 'c2:OutHoriPosi'
            possta = 'Vert.Move'
        elif axis == 'Vert' :
            pospv      = 'c2:OutVerPosi'
            possta = 'Hori.Move'
        else :
            print 'MoverControl unknown axis'
            return

        stapv      = 'c2:status'
        cmdpv      = 'c2:Switch'
        valpv      = 'c2:Posi'
           
        cpos,movestatus = self.GetCurrent(pospv,stapv)
        RangeOk = self.RangeCheck(pospv,stapv,leap)
   
        moved = False

        if RangeOk :  
            print 'MoveRelative> ',leap
            print 'before> ' , ca.caget('c2:Posi')
             # Put target value
            ca.caput('c2:Posi',leap)
            print 'after> ' , ca.caget('c2:Posi')
            
            # Look for stop lock file
            if pa.exists('stop.lock') == True:
                print '\nstop.lock found!  ABORT ABORT ABORT!!!\n\n'
                raise KeyboardInterrupt
        
            #log move in master history
            #his = Acq.History()
            #his.RecordMove(axis,cpos,leap)

                    
            # Put target value
            #ca.caput(valpv,leap)
          
            time.sleep(3)
            #print ca.caget(stapv)
        
            # Monitor move and release when done
            #readback = 'Move'
            #while ca.caget(stapv) != 'STOP' :
            #    time.sleep(1)
            #    print 'Waiting...'

            
            latch = 'unarmed'
            ca.caput('c2:Switch','GO')
            mon = ca.camonitor(stapv,self.monCallback,events=ca.DBE_VALUE)
            #ca.caput(cmdpv,'GO')
            try:
                self.evwait.Wait(timeout=30)
            except KeyboardInterrupt:
                self.evwait.Reset()
            except cothread.Timedout:
                print '****** Timeout detected, move may be corrupt!!!'
                
            mon.close()
            moved = True
            ca.caput('c2:Switch','Off')
            #log move in master history
            his = Acq.History()
            his.RecordMove(axis,cpos,leap)

        else :
            if not RangeOk :
                print 'MoveRelative:Leap out of range!!! Ignoring!!!'
            elif movestatus != 'STOP' :
                print 'MoveRelative:Mover not ready!!!'
            else :
                print 'Not sure what happened, but it is wrong!'

        return moved

    def monCallback(self,val) :
        print 'command > ', ca.caget('c2:Switch')
        if val == 'STOP' and self.latch == 'unarmed':
            self.latch = 'armed'
            print 'MoveRelative:Move has not started...'
        elif val == 'STOP' and self.latch == 'armed' :
            self.latch = 'unarmed'
            self.evwait.Signal()
            print 'MoveRelative:Move finished'
        else :
            print 'MoveRelative:Move in progress!!!'
            self.latch = 'armed'
        print 'MoveRelative:Status:',val

        return


  
