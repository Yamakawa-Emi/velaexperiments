import numpy as np
import pylab as pl
import os
import shutil
import time
import gzip

from tempfile import mkstemp as _mkstemp
from shutil import move as _move
from os import remove as _remove
from os import close as _close


def DataType(filepath):
    filename = filepath.split('/')[-1][:-4]
    datatype = filename[-3:]
    return datatype

def GetLocalSettings():
    a = GeneralString()
    a.Read('../LOCALSETTINGS.txt')
    return a.data

def UpdateRunDir():
    #this should be run from the run dir!
    rundir = os.getcwd()+'/'
    a = GeneralString()
    a.Read('../LOCALSETTINGS.txt')
    dd = a.data
    dd['rundir'] = rundir
    a.OverWrite(dd)

class Data :
    """
    Read('../path/to/filename_typ.dat')

    Produces
    self.data - dictionary containing all data in file.
    self.datainfo - diciontary containing
    'type', 'noofdata','sampindex'

    NewFile('suffix',infolist=None)
    Creates new file

    Write(sample)
    sample is dictionary
    """
    
    def __init__(self,filezip = False) :
        self.data     = {}
        self.datainfo = {}
        self.filezip = filezip
        

    def Clear(self) :
        self.data     = {}
        self.datainfo = {}

    def Read(self, filepath):
        self.filepath = filepath
        self.filename = filepath.split('/')[-1][:-7]
        datatype      = self.filename[-6:]
        
        self.datainfo['type'] = datatype
        
        if self.filename.startswith('20'):
            daydate   = int(self.filename[:8])
        else:
            daydate   = int(self.filename[:-7][-8:])
        print 'File Recorded on '+str(daydate)
        
        #read data
        if self.datainfo['type'] == 'INI':
            print 'Initialisation Data Format'
            a = DataFormatIni()
            a.Read(filepath)
            self.data = a.data
        #elif self.datainfo['type'] == 'scan':
        #    print 'Data Scan'
        #    a = DataScan()
        #    a.Read(filepath)
        #    self.data = a.data
        #    self.setupdata = a.setupdata
        elif (daydate > 20120000) :
            print 'Data Format'
            a = DataFormat3()
            a.Read(filepath)
            self.data = a.data
            self.setupdata = a.setupdata
        else:
            print 'Unknown Data Format'

        #prepare some extra variables
        if self.data.has_key('refc1amp') == True:
            self.data['sampindex']      = np.arange(len(self.data['refc1amp']))
            self.datainfo['noofdata']   = len(self.data['refc1amp'])
        #else:
         #   l = self.data.keys()[0]
          #  if l == 'info':
          #      l = self.data.keys()[1]
          #      self.data['sampindex']      = np.arange(len(self.data[l]))
          #      self.datainfo['noofdata']   = len(self.data[l])

        if self.datainfo['type'] == 'INI':
            del self.data['info']
    
    def NewFile(self,filezip,suffix,infolist=[]) :
        """Creates a new data file:
        
        ../dat/raw/YYYYMMDD_HHMM_suffix.dat
        
        Usage: NewFile(suffixstring,infolist(optional))
        
        """
        self.filezip = filezip
        print 'new file>', self.filezip
        
        
        t                 = time.strftime('%Y%m%d_%H%M')
        if suffix == 'INI':
            self.filepath     = '/home/epics/ipsbpm/bpm/run/dat/ini/'+t+'_'+str(suffix)+'.dat'
            #self.filepath     = '/home/epics/ioc/run/test/ini/'+t+'_'+str(suffix)+'.dat'
            self.filename     = t+'_'+str(suffix)
        else:
            self.filepath     = '/home/epics/ipsbpm/bpm/run/dat/raw/'+t+'_'+str(suffix)+'.dat'
            #self.filepath     = '/home/epics/ioc/run/test/raw/'+t+'_'+str(suffix)+'.dat'
            self.filename     = t+'_'+str(suffix)
       
        
        if os.path.exists(self.filepath) == True:
            print 'File Exists - changing name to minute + 1'
            minutestring = t[-2:][0]
            if minutestring[0] == '0':
                minute = '0'+str(int(minutestring)+1)
                t      = t[:-2]+minute
            elif minutestring[0] == '59':
                hour   = str(int(t[-4:-2])+1)
                t      = t[:-4]+hour+'00'
            else:
                minute = str(int(minutestring)+1)
            self.filepath = '/home/epics/ipsbpm/bpm/run/dat/raw/'+t+'_'+suffix+'.dat'
            #self.filepath = '/home/epics/ioc/run/test/raw/'+t+'_'+suffix+'.dat'

        if self.filezip :
            self.f            = gzip.open(self.filepath+'.gz','a',0)
            self.filepath = self.filepath+'.gz'
        else:
            self.f            = open(self.filepath,'a',0)
        
        print 'lwData.Data> opening new file ',self.filepath
        
        #Write supplied info value list
        infostring      = 'info> '+' '.join(map(str,infolist))+'\n'
        self.f.write(infostring)

    def WriteData(self,sample):
        """Writes data sample. NewFile must be called beforehand to create a file.
        
        Usage: WriteData(sample)
        
        Sample is a ordereddict type like 
        {datakey1:value(single value or array),datakey2:value...}
        
        """
        
        #self.f.write(infostring)
        for datakey in sample:
            linestring = datakey+'> '+' '.join(map(str,np.array([sample[datakey]]).flatten()))+'\n'
            self.f.write(linestring)

    def CloseFile(self) :
        
        self.f.close()
        
class DataFormat :

    # data of slot2 & slot3
    def __init__(self,bpmname) :

        self.bpm       = bpmname
        self.wfmsl2ch1 = []
        self.wfmsl2ch0 = []
        self.wfmsl3ch1 = []
        self.wfmsl3ch0 = []
        self.xddcfilteredImag = []
        self.xddcfilteredReal = []
        self.yddcfilteredImag = []
        self.yddcfilteredReal = []
        self.chawf  = []
        self.chbwf  = []
        self.BPM01X = []
        self.BPM02X = []
        self.BPM03X = []
        self.BPM04X = []
        self.BPM05X = []
        self.BPM01Y = []
        self.BPM02Y = []
        self.BPM03Y = []
        self.BPM04Y = []
        self.BPM05Y = []
        self.npulse = []
        self.posix = []
        self.posiy = []
        self.SetcbpmPosi = []
        self.axis = []
        self.scannpulse = []
        self.scannstep = []
        self.scanrange = []
        self.scanrlposi = []
    
    def Readlogs(self,fd) :

              
        
        print 'CBPM.log read',fd
        
        self.iread =0
        
        for line in fd:
            line = line.strip('>')
            line = line.strip()            
            t = line.split(" ")
            nv = len(t)
            d = []

           

            if t[0] == 'slot2ch1>':
                for i in range(1,nv) :
                    self.wfmsl2ch1.append(float(t[i]))
                
            elif t[0] == 'slot2ch0>':
                for i in range(1,nv) :
                    self.wfmsl2ch0.append(float(t[i]))                              
            elif t[0] == 'slot3ch0>':
                for i in range(1,nv) :
                    self.wfmsl3ch0.append(float(t[i]))                              
            elif t[0] == 'slot3ch1>':
                for i in range(1,nv) :
                    self.wfmsl3ch1.append(float(t[i]))

            elif t[0] == 'cha:wf>':
                for i in range(1,nv) :
                    self.chawf.append(float(t[i]))                              
            elif t[0] == 'chb:wf>':
                for i in range(1,nv) :
                    self.chbwf.append(float(t[i]))

            

            elif t[0] == 'RP2x:ddcfilteredReal>':
                for i in range(1,nv) :
                    self.xddcfilteredReal.append(float(t[i]))                              
            elif t[0] == 'RP2x:ddcfilteredImag>':
                for i in range(1,nv) :
                    self.xddcfilteredImag.append(float(t[i]))
            elif t[0] == 'RP2y:ddcfilteredReal>':
                for i in range(1,nv) :
                    self.yddcfilteredReal.append(float(t[i]))                              
            elif t[0] == 'RP2y:ddcfilteredImag>':
                for i in range(1,nv) :
                    self.yddcfilteredImag.append(float(t[i]))

            
            elif t[0] == self.bpm+'x:ddcfilteredReal>':
                for i in range(1,nv) :
                    self.xddcfilteredReal.append(float(t[i]))                              
            elif t[0] == self.bpm+'x:ddcfilteredImag>':
                for i in range(1,nv) :
                    self.xddcfilteredImag.append(float(t[i]))
            elif t[0] == self.bpm+'y:ddcfilteredReal>':
                for i in range(1,nv) :
                    self.yddcfilteredReal.append(float(t[i]))                              
            elif t[0] == self.bpm+'y:ddcfilteredImag>':
                for i in range(1,nv) :
                    self.yddcfilteredImag.append(float(t[i]))
            elif t[0] == 'xwf>':
                for i in range(1,nv) :
                    self.chawf.append(float(t[i]))                              
            elif t[0] == 'ywf>':
                for i in range(1,nv) :
                    self.chbwf.append(float(t[i]))
                    
            elif t[0] == 'npulse>':
                for i in range(1,nv) :
                    self.npulse.append(float(t[i]))
                    
            elif t[0] == 'readPosiH>':
                for i in range(1,nv) :
                    self.posix.append(float(t[i]))          
            elif t[0] == 'readPosiV>':
                for i in range(1,nv) :
                    self.posiy.append(float(t[i]))
            elif t[0] == 'scanrelativeposi>':
                for i in range(1,nv) :
                    self.scanrlposi.append(float(t[i]))
            elif t[0] == 'SetcbpmPosi>':
                for i in range(1,nv) :
                    self.SetcbpmPosi.append(float(t[i]))
                    
            elif t[0] == 'Ini_Axis>':
                for i in range(1,nv) :
                    self.axis.append((t[i]))
                #self.axis = np.array(self.axis)
            elif t[0] == 'BPM01X>':
                for i in range(1,nv) :
                    self.BPM01X.append(float(t[i]))
            elif t[0] == 'BPM02X>':
                for i in range(1,nv) :
                    self.BPM02X.append(float(t[i]))
            elif t[0] == 'BPM03X>':
                for i in range(1,nv) :
                    self.BPM03X.append(float(t[i]))
            elif t[0] == 'BPM04X>':
                for i in range(1,nv) :
                    self.BPM04X.append(float(t[i]))
            elif t[0] == 'BPM05X>':
                for i in range(1,nv) :
                    self.BPM05X.append(float(t[i]))
            elif t[0] == 'BPM01Y>':
                for i in range(1,nv) :
                    self.BPM01Y.append(float(t[i]))
            elif t[0] == 'BPM02Y>':
                for i in range(1,nv) :
                    self.BPM02Y.append(float(t[i]))
            elif t[0] == 'BPM03Y>':
                for i in range(1,nv) :
                    self.BPM03Y.append(float(t[i]))
            elif t[0] == 'BPM04Y>':
                for i in range(1,nv) :
                    self.BPM04Y.append(float(t[i]))
            elif t[0] == 'BPM05Y>':
                for i in range(1,nv) :
                    self.BPM05Y.append(float(t[i]))
            elif t[0] == 'scannpulse>':
                for i in range(1,nv) :
                    self.scannpulse.append(float(t[i]))
            elif t[0] == 'scanrange>':
                for i in range(1,nv) :
                    self.scanrange.append(float(t[i]))
            elif t[0] == 'scannstep>':
                for i in range(1,nv) :
                    self.scannstep.append(float(t[i]))
                  
        #print self.wfmsl2ch1
                
        print 'Bpm. LoggedData.total pulses>',self.iread
   
class DataFormat2DScan :

    # data of 2Dscan slot2 & slot3
    def __init__(self) :
        
        self.wfmsl2ch1 = []
        self.wfmsl2ch0 = []
        self.wfmsl3ch1 = []
        self.wfmsl3ch0 = []
        self.chawf  = []
        self.chbwf  = []
        self.BPM01X = []
        self.BPM02X = []
        self.BPM03X = []
        self.BPM04X = []
        self.BPM05X = []
        self.BPM01Y = []
        self.BPM02Y = []
        self.BPM03Y = []
        self.BPM04Y = []
        self.BPM05Y = []
        self.posix = []
        self.posiy = []
        self.SetcbpmPosi = []
        self.axis = []
        self.scan2Dnpulse = []
        self.scan2Dnstep1st = []
        self.scan2Drange1st = []
        self.scan2Drlposi1st = []
        self.scan2Dnstep2nd = []
        self.scan2Drange2nd = []
        self.scan2Drlposi2nd = []
       
    
    def Readlogs(self, fd) :

              
        
        print 'CBPM.2DScan Log read',fd
        
        self.iread =0
        
        for line in fd:
            line = line.strip('>')
            line = line.strip()            
            t = line.split(" ")
            nv = len(t)
            d = []

           

            if t[0] == 'slot2ch1>':
                for i in range(1,nv) :
                    self.wfmsl2ch1.append(float(t[i]))
            elif t[0] == 'slot2ch0>':
                for i in range(1,nv) :
                    self.wfmsl2ch0.append(float(t[i]))                              
            elif t[0] == 'slot3ch0>':
                for i in range(1,nv) :
                    self.wfmsl3ch0.append(float(t[i]))                              
            elif t[0] == 'slot3ch1>':
                for i in range(1,nv) :
                    self.wfmsl3ch1.append(float(t[i]))                              
            elif t[0] == 'cha:wf>':
                for i in range(1,nv) :
                    self.chawf.append(float(t[i]))                              
            elif t[0] == 'chb:wf>':
                for i in range(1,nv) :
                    self.chbwf.append(float(t[i]))

            elif t[0] == 'readPosiH>':
                for i in range(1,nv) :
                    self.posix.append(float(t[i]))          
            elif t[0] == 'readPosiV>':
                for i in range(1,nv) :
                    self.posiy.append(float(t[i]))
            elif t[0] == 'scan2DrelativePosi1st>':
                for i in range(1,nv) :
                    self.scan2Drlposi1st.append(float(t[i]))
            elif t[0] == 'scan2DrelativePosi2nd>':
                for i in range(1,nv) :
                    self.scan2Drlposi2nd.append(float(t[i]))
            elif t[0] == 'Ini_SetPosi>':
                for i in range(1,nv) :
                    self.SetcbpmPosi.append(float(t[i]))
            elif t[0] == 'Ini_Axis>':
                for i in range(1,nv) :
                    self.axis.append((t[i]))
            elif t[0] == 'BPM01X>':
                for i in range(1,nv) :
                    self.BPM01X.append(float(t[i]))
            elif t[0] == 'BPM02X>':
                for i in range(1,nv) :
                    self.BPM02X.append(float(t[i]))
            elif t[0] == 'BPM03X>':
                for i in range(1,nv) :
                    self.BPM03X.append(float(t[i]))
            elif t[0] == 'BPM04X>':
                for i in range(1,nv) :
                    self.BPM04X.append(float(t[i]))
            elif t[0] == 'BPM05X>':
                for i in range(1,nv) :
                    self.BPM05X.append(float(t[i]))
            elif t[0] == 'BPM01Y>':
                for i in range(1,nv) :
                    self.BPM01Y.append(float(t[i]))
            elif t[0] == 'BPM02Y>':
                for i in range(1,nv) :
                    self.BPM02Y.append(float(t[i]))
            elif t[0] == 'BPM03Y>':
                for i in range(1,nv) :
                    self.BPM03Y.append(float(t[i]))
            elif t[0] == 'BPM04Y>':
                for i in range(1,nv) :
                    self.BPM04Y.append(float(t[i]))
            elif t[0] == 'BPM05Y>':
                for i in range(1,nv) :
                    self.BPM05Y.append(float(t[i]))
            elif t[0] == 'Ini_2Dscannpulse>':
                for i in range(1,nv) :
                    self.scan2Dnpulse.append(float(t[i]))
            elif t[0] == 'Ini_2Dscanrange_1st>':
                for i in range(1,nv) :
                    self.scan2Drange1st.append(float(t[i]))
            elif t[0] == 'Ini_2Dscanrange_2nd>':
                for i in range(1,nv) :
                    self.scan2Drange2nd.append(float(t[i]))
            elif t[0] == 'Ini_2Dscannstep_1st>':
                for i in range(1,nv) :
                    self.scan2Dnstep1st.append(float(t[i]))
            elif t[0] == 'Ini_2Dscannstep_2nd>':
                for i in range(1,nv) :
                    self.scan2Dnstep2nd.append(float(t[i]))
 
        
class General:
    """
    Laser-wire general format:
    
    key> value1 value2 etc

    If key already _exists, append in new dimension.
    If there is only one value, remove list / array structure
    """

    def __init__(self):
        self.data = {}

    def Clear(self):
        self.data = {}

    def Read(self,filepath):
        self.filepath = filepath
        self.filename = filepath.split('/')[-1]
        print 'lwData.General> reading ',filepath
        try:
            f = open(filepath,'r')
        except:
            print filepath,' does not exist'
            return
        
        lines = f.readlines()
        f.close()
        
        for line in lines:
            dataline  = line.split('>')
            datakey   = dataline[0]
            datavalue = dataline[1]
            datavalue = map(float,datavalue.strip('\n').strip().split(' '))
            self.data[datakey] = datavalue
            
        for key in self.data:
            if len(self.data[key]) == 1:
                self.data[key] = self.data[key][0]
            else:
                self.data[key] = np.array(self.data[key])

    def Write(self,filepath,datadict):
        f = open(filepath,'w')
        for key in datadict:
            if type(datadict[key]) != list:
                if type(datadict[key]) == np.ndarray:
                    datadict[key] = list(datadict[key])
                else:
                    datadict[key] = [datadict[key]]
        for key in datadict:
            linestring = str(key)+'> '+' '.join(map(str,datadict[key]))+'\n'
            f.write(linestring)
        f.close()
        print 'lwData.General> data written to ',filepath

    def WriteInOrder(self,filepath,datadict,keylist):
        f = open(filepath,'w')
        for key in datadict:
            if type(datadict[key]) != list:
                if type(datadict[key]) == np.ndarray:
                    datadict[key] = list(datadict[key])
                else:
                    datadict[key] = [datadict[key]]
        for key in keylist:
            linestring = str(key)+'> '+' '.join(map(str,datadict[key]))+'\n'
            f.write(linestring)
        f.close()
        print 'lwData.General> data written to ',filepath
    
class GeneralString:
    def __init__(self):
        self.data     = {}
        self.filename = 'unknown.dat'
        self.filepath = 'unknown.dat'
        
    def Clear(self):
        self.data = {}

    def Read(self,filepath):
        self.filename = filepath.split('/')[-1]
        self.filepath = filepath
        f = open(filepath,'r')
        lines = f.readlines()
        f.close()

        for line in lines:
            key   = line.split('>')[0]
            value = line.split('>')[1].strip('\n').strip().strip("'")
            if self.data.has_key(key) != True:
                self.data[key] = value
            else:
                if type(self.data[key]) != list:
                    self.data[key] = list(self.data[key])
                    self.data[key] = self.data[key].append(value)
                else:
                    self.data[key] = self.data[key].append(value)
        for key in self.data:
            if self.data[key][-1] != "/":
                self.data[key] = self.data[key]+'/'
            else:
                pass

    def Write(self,datadict,filepath):
        self.filename = filepath.split('/')[-1]
        self.filepath = filepath
        f = open(filepath,'w')
        for key in datadict:
            stringtowrite = str(key)+'> '+str(datadict[key])+'\n'
            f.write(stringtowrite)
        f.close()

    def Append(self,datadict,filepath):
        self.fileobject = open(filepath,'a',0)
        for key in datadict:
            stringtowrite = '\n'+str(key)+"> '"+str(datadict[key])+"'\n"
            self.fileobject.write(stringtowrite)

    def Close(self):
        if hasattr(self,'fileobject') == True:
            self.fileobject.close()
            del self.fileobject

    def OverWrite(self,datadict):
        fh,abs_path = _mkstemp()
        new_file = open(abs_path,'w')
        for key in datadict:
            stringtowrite = str(key)+'> '+str(datadict[key])+'\n'
            new_file.write(stringtowrite)
        new_file.close()
        _close(fh)
        _remove(self.filepath)
        _move(abs_path,self.filepath)

class MultiData:
    def __init__(self):
        self.totaldata = {}

    def TempMultiRead(self,pathlist):
        fl = []
        for file in pathlist:
            if file[-7:][:-4] == 'tmp':
                fl.append(file)

        self.MultiRead(fl)

    def TempMultiReadSeparate(self,pathlist):
        fl = []
        for file in pathlist:
            if file[-7:][:-4] == 'tmp':
                fl.append(file)

        self.MultiReadSeparate(fl)

    def MultiRead(self,filelist):

        for file in filelist:
            print file
            a = Data()
            a.Read(file)
            for key in a.data:
                if self.totaldata.has_key(key) == True:
                    self.totaldata[key].extend(a.data[key])
                else :
                    self.totaldata[key] = []
                    self.totaldata[key].extend(a.data[key])
            del a

    def MultiReadSeparate(self,filelist):
        for file in fl:
            a = Data()
            a.Read(file)
            self.totaldata[a.filename] = a.data
            del a


            
      

  
