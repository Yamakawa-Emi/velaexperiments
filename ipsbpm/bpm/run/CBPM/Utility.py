import os
import time
import gzip

import pylab as pl

import Bpm


def fileNameStrip(fn) :
    '''Get the end of a file name excluding the directory name
    fn : input file name
    return : string stripped filename'''
    sfn = fn.split('/')
    ifn = len(sfn)-1
    return sfn[ifn]

def removeExtension(fn) :
    sfn = fn.split('.')
    return sfn[0] 


def fileNameDateStrip(fn) :
    '''Get the date and time for a file returned as (date,time)
    fn : input file name
    return : (date,time)'''
    stripfn = fileNameStrip(fn)
    splitfn = stripfn.split('_')
    day = splitfn[1]
    time = splitfn[2].split('.')[0]

    return (day,time)
    
def workingDirPlotName(macro,day,time) :
    '''Determine plot file name
    macro : macro name generating plot 
    day : string date 
    time : string time 
    return : plot pdf filename'''
    wdpn = os.environ['BPMWORKING']+'/200910/'+macro+'_'+day+'_'+time+'.pdf'
    return wdpn 

def findHistMax(h) :
    '''Location of histogram maximum (x,y)
    h : input histogram from pylab.hist
    return : (x,y) location of peak'''
    imax = pl.nonzero(h[0].max() == h[0])[0][0]
    
    xmax = h[1][imax]
    ymax = h[0][imax]
    
    return (xmax,ymax)

def writeLog(message) :
    '''Write a message to a log file
    message : message to be written'''

    t = time.strftime('%Y%m%d_%H%M%S')
    
    f = open(os.environ['BPMRUN']+'/bpmControl.log','a')
    f.write(t+'> '+message+'\n')
    f.close()
    pass

def convertDataToAscii(fn) :
    '''Convert data file to flat ascii
    fn : input data file name
    return : none'''
    print "convertDataToAScii opening : ",fn

    # form output file name
    ofn = fn[fn.rfind('/')+1:fn.rfind('.')]+".txt"
    ofd = open(ofn,'w') 
    print "convertDataToAscii writing : ",ofn

    # open input file
    ifd = open(fn,'r')

    # load data into object
    ld = Bpm.LoggedData()
    ld.read(ifd);

    # dump data to ascii 
    ld.writeFlatAsciiWaveforms(ofd)


def cbpmLookup(name) :
    '''Service function to look up ATF2 BPM index from name'''    
    # 2012/04
    cnames = "QD10X QF11X QD12X QD16X QF17X QD18X QF19X QD20X QF21X IPT1 IPT2 IPT3 IPT4 QM16FF QM15FF QM14FF FB2FF QM13FF QM12FF QM11FF QD10BFF QD10AFF QF9BFF SF6FF QF9AFF QD8FF QF7FF QD6FF QF5BFF SF5FF QF5AFF QD4BFF SD4FF QD4AFF QF3FF QD2BFF QD2AFF SF1FF QF1FF SD0FF QD0FF PREIP IPA IPB M-PIP REFC1 REFC2 REFC3 CDIODE REFS1 REFS2 SDIODE SPHASE REFIPX REFIPY1 REFIPY2 REFIPTX REFIPTY IPDIODE TILT XBAND"

    cnames = cnames.split()
    try : 
        return cnames.index(name)
    except ValueError :
        return -1

def abpmLookup(name) :
    '''Service function to look up ATF2 BPM index from name'''
    # 2012/04
    anames = "QF1X QD2X QF3X QF4X QD5X QF6X QF7X QD8X QF9X QF13X QD14X QF15X QD10X QF11X QD12X QD16X QF17X QD18X QF19X QD20X QF21X IPT1 IPT2 IPT3 IPT4 QM16FF QM15FF QM14FF FB2FF QM13FF QM12FF QM11FF QD10BFF QD10AFF QF9BFF SF6FF QF9AFF QD8FF QF7FF QD6FF QF5BFF SF5FF QF5AFF QD4BFF SD4FF QD4AFF QF3FF QD2BFF QD2AFF SF1FF QF1FF SD0FF QD0FF PREIP IPA IPB M-PIP REFC1 REFC2 REFC3 CDIODE REFS1 REFS2 SDIODE SPHASE REFIPX REFIPY1 REFIPY2 REFIPTX REFIPTY IPDIODE TILT XBAND"

    anames = anames.split() 
    try : 
        return anames.index(name)    
    except ValueError :
        return -1

def analysis(d) :    
    ''' General analysis function, determines data and does analysis
    d : either a datafile or a loaded data object
    return : none'''
    # determine file type 
    pass

