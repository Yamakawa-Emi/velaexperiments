#!/bin/sh
# create edm and st.cmd files

case "$1" in 
    start)
    echo "Starting $0"
    ./bpmConfig.py
    ./bpmdb.sh start

    sleep 3
    ./bpmCars.py -l

    ;;

    stop)
    echo "Stopping $0"
    ./bpmCars.py -s
    ./bpmdb.sh stop
    ;;


    status)
    ./bpmdb.sh status
    ;;    

    else)
    echo "No option"


esac
