#! /usr/bin/env python  
import Bpm
import optparse

import cothread
import cothread.catools
import Utility 

def main():
    usage = "usage: %prog [bpmname]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-s","--save", dest="save",
                      action="store_true",
                      default=False,
                      help="save")
    parser.add_option("-r","--restore", dest="restore",
                      action="store_true",
                      default=False,
                      help="restore")
    parser.add_option("-l","--lastrestore", dest="restorelast",
                      action="store_true",
                      default=False,
                      help="restore last saved file");
    parser.add_option("-g","--gui", dest="gui",
                      action="store_true",
                      default=False,
                      help="start casr gui")

    options, args = parser.parse_args()

    print 'bpmCasr>'
    
    c = Bpm.Casr()

    # get system status
    try :
        ss = cothread.catools.caget("cbpm:systemstatus")
    except cothread.cothread.Timedout :
        print 'bpmCasr> db not running'
        return
    
    if ss == 0 : 
        status = 'norm'
    elif ss == 1 :
        status = 'tcal' 
    elif ss == 2 :
        status = 'mcal' 
    elif ss == 3 :
        status = 'ccal'
    elif ss == 4 :
        status = 'bcal'

    if options.save:
        fn = c.save()
        Utility.writeLog("bpmCasr:save "+fn+' '+str(ss))
        if status == 'norm' :
            cothread.catools.caput('cbpm:casrBeamFileName',fn)
        elif status == 'tcal' :
            cothread.catools.caput('cbpm:casrCalFileName',fn)            
    elif options.restorelast:
        casrFn = ''
        if status == 'norm' :
            casrFn = cothread.catools.caget('cbpm:casrBeamFileName')
        elif status == 'tcal' :
            casrFn = cothread.catools.caget('cbpm:casrCalFileName')
                
        print 'bpmCasr.restorelast '+casrFn

        if casrFn != '' :
            fn = c.restore(casrFn)
            cothread.catools.caput("cbpm:casrFileName",casrFn)
            Utility.writeLog("bpmCasr.restored "+casrFn)
        else :
            fn = c.restoreLast()
            Utility.writeLog("bpmCasr.restored "+casrFn)            
        
    elif options.restore:
        c.restore(args[0])
    elif options.gui:
        c.restoreSelection()
    else:
        parser.error('must give one option!')

if __name__ == "__main__":
    main()
