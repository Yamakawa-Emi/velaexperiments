#! /usr/bin/env python  
import os
import Bpm

def main():
    m = Bpm.Map(os.environ['BPMCONFIG'])
   # m.makeDbConfig()
    m.makeCasrConfig()
    m.makeEdmSummary()
    
    
if __name__ == "__main__":
    main()
