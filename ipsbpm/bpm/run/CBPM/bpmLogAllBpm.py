#! /usr/bin/env python  
import Bpm
import optparse
import cothread
import cothread.catools

import Utility

def main():
   
    l = Bpm.newLoggerAllBPM()
    np = int(cothread.catools.caget("cbpmnew:logall-npulse"))

    print 'newbpmLogAllBPMdata.py> Number of pulses',np
    fn = l.log(np)


if __name__ == "__main__":
    main()
