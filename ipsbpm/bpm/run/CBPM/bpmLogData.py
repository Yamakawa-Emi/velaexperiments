#! /usr/bin/env python  
import Bpm
import Acq
import optparse
import cothread
import cothread.catools as ca

import Utility

def main():
    usage = "usage: %prog [bpmname]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-b","--bpm", dest="bpm",
                      action="store",
                      default="",
                      type="string",
                      help="save")
    parser.add_option("-n","--npulses",dest="npulse",
                      action="store",
                      default=0,
                      help="save")
    parser.add_option("-a","--append",dest="append",
                      action="store",
                      default="",
                      type="string")
    parser.add_option("-s","--sync",dest="sync",
                      action="store_true",
                      default=False)
    
    (options, args) = parser.parse_args()

    if options.bpm != '' :
        ca.caput('c2:Switch','Off')
        ca.caput('c2:Posi',0.0)
        zipoption = ca.caget('c2:zip')
     
        l = Acq.LoggerDataPulse(zipoption)
       
    else :
        print 'newbpmLogData.py -b BPMNAME'
        return

    
    if options.npulse !=0 :
        np = int(options.npulse)

    else :
        np = 100

    print 'bpmLogdata.py> Number of pulses',np
    fn = l.Log(options.bpm,'logRP',np)


if __name__ == "__main__":
    main()
