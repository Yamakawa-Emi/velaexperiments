#!/bin/sh 
# start the bpmdb ioc

case "$1" in
    start)
    echo "Starting $0"

    if [ -f /tmp/bpmdb.lock ]; then
	echo "bpmdb alread running"
	exit 0;
    fi

    cd ../../iocBoot/iocbpm/
    screen -d -m -S bpmdb -h 1000 ./st.cmd
    touch /tmp/bpmdb.lock
    echo $USER >> /tmp/bpmdb.lock
    chmod ug+wr /tmp/bpmdb.lock
    ;;

    stop)
    echo "Stopping $0"
    pid=`ps aux | grep -i "screen -d -m -S bpmdb -h 1000 ./st.cmd" | grep -v grep | awk '{print $2;}'`
    echo "Killing pid: $pid"
    kill $pid
    rm -rf /tmp/bpmdb.lock
    ;;

    restart)
    echo "Restarting $0"
    sh $0 stop
    sh $0 start
    ;;

    status)
    if [ -f $lck ] 
    then 
	user=`grep -o "[a-z]\{1,20\}" $lck`
	pid=`grep -o "[0-9]\{1,5\}" $lck`
	psreport=`ps $pid | wc -l`
	if [ "$psreport" == "2" ] 
	then
	    psreport="Running"
	else
	    psreport="Not running"
	fi
	echo $0 $user $pid $psreport
    else 
	echo $0 Not running
    fi
esac
