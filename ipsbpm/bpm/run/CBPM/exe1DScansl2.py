#! /usr/bin/python

import cothread
import cothread.catools as ca
import numpy as np
import time
import gzip

import Acq 

def main():
     ca.caput('c2:Switch','Off')
     ca.caput('c2:Posi',0.0)     

     clnpulse    = ca.caget('scan:npulse')
     posrange    = ca.caget('scan:range')
     nstep       = ca.caget('scan:nstep')
     axis        = ca.caget('c2:Axis')
     zipoption   = ca.caget('scan:zip')
     l = Acq.CalCBPM(zipoption)
     l.MoveRelativeScan('scansl2',axis, clnpulse, posrange, nstep)
      
     ca.caput('c2:Posi',0.0)
    

if __name__ == "__main__":
    main()
