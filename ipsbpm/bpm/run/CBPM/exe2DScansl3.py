#! /usr/bin/python

import cothread
import cothread.catools as ca
import numpy as np
import time
import gzip

import Acq 

def main():
     ca.caput('c2:Switch','Off')
     ca.caput('c2:Posi',0.0)

     cl2Dnpulse    = ca.caget('scan:2Dnpulse')
     posrange1    = ca.caget('scan:2Drange1stMove')
     nstep1       = ca.caget('scan:2Dnstep1stMove')
     posrange2    = ca.caget('scan:2Drange2ndMove')
     nstep2       = ca.caget('scan:2Dnstep2ndMove')
     zipoption    = ca.caget('scan:zip')
     axis        = ca.caget('c2:Axis')
     l = Acq.CalCBPM(zipoption)
     l.MoveRelative2DScan('scan2Dsl3',axis, cl2Dnpulse, posrange1, nstep1, posrange2, nstep2)
     
     ca.caput('c2:Posi',0.0)

     
if __name__ == "__main__":
    main()
