#! /usr/bin/python

import cothread
import cothread.catools as ca
import numpy as np
import time
import gzip

import Acq 

def main():

   
    ca.caput('c2:Switch','Off')

    ca.caput('c2:Posi',0.0)    

    zipoption = ca.caget('c2:zip')
    l = Acq.LoggerDataPulse(zipoption)
    npulse = ca.caget('c2:npulse')
    l.Log('logRP',npulse)

    ca.caput('c2:Posi',0.0)
    


    	

if __name__ == "__main__":
    main()
