#! /usr/bin/python

import cothread
import cothread.catools as ca
import numpy as np
import time
import gzip

import Analyse as ana 

def main():
    an = ana.BPMAnalysis()
    filepath = ca.caget('plot:2Dfilepath')
    option = ca.caget('plot:2Doption')
    step1st = ca.caget('plot:2Dnstep1st')
    step2nd = ca.caget('plot:2Dnstep2nd')
  
    an.CalAnalysis2D(filepath,option)
    an.scanPlot2D(step1st,step2nd)
    
if __name__ == "__main__":
    main()
    rep = ''
    while not rep in ['q','Q']:
        rep = raw_input('enter "q" to quit: ')
        if 1 < len(rep):
            rep = rep[0]

