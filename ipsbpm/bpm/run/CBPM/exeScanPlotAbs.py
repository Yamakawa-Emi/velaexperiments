#! /usr/bin/python

import cothread
import cothread.catools as ca
import numpy as np
import time
import gzip

import Analyse as ana 

def main():
    an = ana.BPMAnalysis()
    filepath = ca.caget('plot:filepath')
    option = ca.caget('plot:option')
  
    an.CalAnalysis_abs(filepath,option)
    an.scanPlot_abs()
    
if __name__ == "__main__":
    main()
    rep = ''
    while not rep in ['q','Q']:
        rep = raw_input('enter "q" to quit: ')
        if 1 < len(rep):
            rep = rep[0]

