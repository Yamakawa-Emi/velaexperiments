#! /usr/bin/python

import cothread
import cothread.catools as ca
import numpy as np
import time
import gzip
import optparse

import Analyse as ana 

def main():
    usage = "usage: %prog [bpmname]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-b","--bpm", dest="bpm",
                      action="store",
                      default="",
                      type="string",
                      help="save")
    
    (options, args) = parser.parse_args()

    if options.bpm != '' :
        an = ana.BPMAnalysis(options.bpm)
        
    else :
        print 'newbpmLogData.py -b BPMNAME'
        return
    
    filepath = ca.caget('plot:filepath')
    an.CalAnalysisRP_all(filepath)
    an.scanPlotRP_all_py()
       
if __name__ == "__main__":
    main()
#    rep = ''
#    while not rep in ['q','Q']:
#        rep = raw_input('enter "q" to quit: ')
#        if 1 < len(rep):
#            rep = rep[0]
            
            
