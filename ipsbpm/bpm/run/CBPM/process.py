#! /usr/bin/python

import cothread
import cothread.catools as cc
import time

class Trig:
    def __init__(self) :
        cc.caput('c2:Trig',0)
        self.num = 0
        self.QuitEvent  = cothread.Event(auto_reset = False)


    def Callback(self,value):

        #print 'Trigger first> ', cc.caget('c2:Trig')
        cothread.Sleep(0.1)
        
        d = cc.caput('c2:Trig',self.num)
        print 'Trigger >' , cc.caget('c2:Trig')
        self.num += 1
        cothread.Sleep(0.3)

    def Reset(self):
        self.QuitEvent.Signal()
        if hasattr(self,'s'):
            self.s.close()

    def TrigStart(self):
        self.QuitEvent.Reset()
        #try:
        self.s = cc.camonitor('c2:wfmsl2ch0',self.Callback)
        self.QuitEvent.Wait()
            #cothread.WaitForQuit() 
        #except KeyboardInterrupt:
        #    self.Reset()
            

def main():
    a = Trig()
    try:
        a.TrigStart()
    except KeyboardInterrupt:
        a.Reset()

if __name__ == "__main__":
    main()

   
    
    
