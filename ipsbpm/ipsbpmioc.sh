#!/bin/sh 
# start extlw ioc

case "$1" in
    start)
    echo "Starting $0"

    if [ -f /tmp/ipsbpmioc.lock ]; then
	echo "ipsbpmioc alread running"
	exit 0;
    fi

    cd /home/epics/ipsbpm/bpm/iocBoot/iocbpm/
    screen -d -m -S ipsbpmioc -h 1000 ./st.cmd
    touch /tmp/ipsbpmioc.lock
    echo $USER >> /tmp/ipsbpmioc.lock
    chmod ug+wr /tmp/ipsbpmioc.lock
    ;;

    stop)
    echo "Stopping $0"
    /usr/bin/killall -9 st.cmd
    rm -rf /tmp/ipsbpmioc.lock
    ;;

    restart)
    echo "Restarting $0"
    sh $0 stop
    sh $0 start
    ;;
esac