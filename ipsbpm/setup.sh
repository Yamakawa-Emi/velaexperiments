#EPICS
export EPICS_CA_MAX_ARRAY_BYTES=100000000

export EPICS_HOST_ARCH=linux-x86_64
export HOST_ARCH=linux-x86_64
export EPICS=/home/$USER/epics
export EPICS_BASE=$EPICS/base
export EPICS_EXTN=$EPICS/extensions
export PATH=$PATH:$EPICS_BASE/bin/$EPICS_HOST_ARCH
export PATH=$PATH:$EPICS_EXTN/bin/$EPICS_HOST_ARCH
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${EPICS_BASE}/lib/${EPICS_HOST_ARCH}:${EPICS_EXTN}/lib/$EPICS_HOST_ARCH"

export EPICS_CA_AUTO_ADDR_LIST=YES
export EPICS_CA_ADDR_LIST="localhost"
export EPICS_CAS_INTF_ADDR_LIST="localhost"


export EDMLIBS=${EPICS_EXTN}/lib/$EPICS_HOST_ARCH
export EDMOBJECTS=/home/$USER/ipsbpm/bpm/edm/EDM_setup/edmstuff
export EDMFILES=/home/$USER/ipsbpm/bpm/edm/EDM_setup/edmstuff/prefs
export EDMPVOBJECTS=/home/$USER/ipsbpm/bpm/edm/EDM_setup/edmstuff
export EDMHELPFILES=${EPICS_EXTN}/src/edm/helpFiles
export EDMDATAFILES=/home/$USER/ipsbpm/bpm/edm

# python bpm analysis
export BPMDATA=/home/$USER/ipsbpm/bpm/data
export BPMROOT=/home/$USER/ipsbpm/bpm
export BPMRUN=$BPMROOT/run/
export BPMDAT=$BPMROOT/dat
export BPMCONFIG=$BPMRUN/CBPM/bpmConfig.txt
export SYSCONFIG=$BPMRUN/CBPM/sysConfig.txt

alias vdct="java -jar /home/$USER/ipsbpm/software/VisualDCT/2.6.1274/VisualDCT.jar"


#
# TCL/Tk
#

TCL_INC=/usr/include
TK_INC=/usr/include
export TCL_INC TK_INC
